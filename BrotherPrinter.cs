﻿using bpac;
using System;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public class BrotherPrinter
    {
        private static readonly Lazy<BrotherPrinter> lazy = new Lazy<BrotherPrinter>(() => new BrotherPrinter());
        public static BrotherPrinter Instance { get { return lazy.Value; } }


        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public async Task PrintStickerAsync(string kioskFilePath, string printerTemplateFileName, string photoFileName, string title, string location, string name)
        {
            await Task.Run(() =>
            {
                try
                {
                    Document doc = new Document();
                    if (doc.Open(kioskFilePath + printerTemplateFileName) != false)
                    {
                        //objPhoto
                        //objTitle
                        //objSignInDate
                        //objLocation

                        //doc.GetObject("objPhoto").SetData(0, kioskFilePath + photoFileName, 4);

                        doc.GetObject("objType").Text = title;
                        doc.GetObject("objSignInDate").Text = DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt");
                        doc.GetObject("objLocation").Text = string.IsNullOrWhiteSpace(location) ? "" : $"L{location}";
                        doc.GetObject("objName").Text = string.IsNullOrWhiteSpace(name) ? "" : name;

                        doc.StartPrint("", PrintOptionConstants.bpoAutoCut);
                        doc.PrintOut(1, PrintOptionConstants.bpoColor);
                        doc.EndPrint();
                        doc.Close();
                    }
                    else
                        throw new Exception("Kiosk Printer Error : " + doc.ErrorCode);
                }
                catch (Exception ex)
                {
                    log.Error("Kiosk Printer Error", ex);
                }
            });
        }
    }
}
