﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model
{
    public class NavigationData
    {
        public string MainPage { get; set; }
        public List<NavigationGroup> NavigationGroups { get; set; }
        public Dictionary<string,string> GenericMessage { get; set; }
        public List<NavigationPage> SpecialPages { get; set; }


        public NavigationPage NavigateToNext(int selectedGroup, int currentPageCounter)
        {
            if (NavigationGroups.Count > selectedGroup)
                return NavigationGroups[selectedGroup].NavigateToNext(currentPageCounter);

            return null;
        }

        public NavigationPage NavigateToBack(int selectedGroup, int currentPageCounter)
        {
            if (NavigationGroups.Count > selectedGroup)
                return NavigationGroups[selectedGroup].NavigateToBack(currentPageCounter);

            return null;
        }

        public NavigationPage NavigateToMain(int selectedGroup)
        {
            if (NavigationGroups.Count > selectedGroup)
                return NavigationGroups[selectedGroup].NavigateToMain();

            return null;
        }
        public NavigationPage GetSpecialPage(string pageName)
        {
            NavigationPage returnPage = SpecialPages.FirstOrDefault(i => i.Page.Equals(pageName, StringComparison.OrdinalIgnoreCase));
            return returnPage == null ? new NavigationPage() : returnPage;
        }


        public string GetGenericMessage(string key)
        {
            string value = "";
            if (GenericMessage != null)
            {
                if (GenericMessage.TryGetValue(key, out value))
                    return value;
                else
                    return "";
            }
            return "";
        }
    }
}
