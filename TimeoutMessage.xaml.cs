﻿using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class TimeoutMessage : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;
        
        private int CloseScreenTimerCounter = 10;

        public TimeoutMessage(Utility.Log.Ilog ilog, NavigationPage navigationPage)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblContent.Text = navigationPage.GetMessages(0);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            CloseScreenTimerCounter = ConfigurationStore.Instance.CloseScreenTimer;

            StartCloseScreenTimer();

            NavigationHelper.Instance.HideLoadingPanel();
        }


        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopCloseScreenTimer();
        }

        private void StartCloseScreenTimer()
        {
            countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
            lblCountdown.Content = countDownCounter.ToString();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
            closeScreenTimer.Tick += closeScreenTimer_Tick;
            closeScreenTimer.Start();
        }
        private void StopCloseScreenTimer()
        {
            lock (_lock)
            {
                closeScreenTimer.Stop();
                closeScreenTimer.Tick -= closeScreenTimer_Tick;
            }
        }

        private int countDownCounter = 0;
        private object _lock = new object();
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            if (countDownCounter >= 0)
                countDownCounter--;

            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();

            if (countDownCounter < 0)
            {
                //ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigationHelper.Instance.NavigateToMain();
            }
        }

        #region Buttons

        private void OKStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        #endregion
    }
}
