﻿using System;

namespace Kiosk.Model.API.Response
{
    public class SignInHistoryResponse
    {
        public DateTime? SignInDateTime { get; set; }
        public DateTime? SignOutDateTime { get; set; }
        public string CardNumber { get; set; }
        public string Name { get; set; }
        public string IdentityNumberLast4 { get; set; }
        public string LocationName { get; set; }
        public string ContactNumber { get; set; }

        public string VisitorType { get; set; }
        public string SignInFrom { get; set; }
    }
}
