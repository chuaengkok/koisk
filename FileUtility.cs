﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Kiosk.Utility
{
    public static class FileUtility
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static async Task<bool> SaveImage(Utility.Log.Ilog ilog, string path, string fileName, BitmapImage image)
        {
            bool isSuccessful = true; ;
            await Task.Run(() =>
            {
                if (image != null)
                {
                    try
                    {
                        BitmapEncoder encoder = new PngBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(image));

                        using (var fileStream = new System.IO.FileStream($"{path}{fileName}", System.IO.FileMode.Create))
                        {
                            encoder.Save(fileStream);

                        }
                    }
                    catch (Exception ex)
                    {
                        ilog.LogError("Exception", ex);
                        isSuccessful = false;
                    }
                }
            });

            return isSuccessful;
        }

        public static async Task<bool> DeleteFile(Utility.Log.Ilog ilog, string path, string fileName)
        {

            bool isSuccessful = true; ;
            await Task.Run(() =>
            {
                try
                {
                    if (File.Exists($"{path}{fileName}"))
                    {
                        File.Delete($"{path}{fileName}");
                    }
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                    isSuccessful = false;
                }
            });

            return isSuccessful;
        }

        public static ImageSource ImageSourceFromFile(Utility.Log.Ilog ilog, string path, string fileName)
        {
            try
            {
                if (File.Exists($"{path}{fileName}"))
                {
                    ImageSource imageSource = new BitmapImage(new Uri($"{path}{fileName}"));
                    return imageSource;
                }
            }
            catch (Exception ex)
            {
                ilog.LogError("Exception", ex);
            }
            return null;

        }
    }
}
