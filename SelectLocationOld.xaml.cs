﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using Kiosk.Utility;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class SelectLocationOld : UserControl
    {
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;
        private List<Location> locationList;

        public SelectLocationOld(NavigationPage navigationPage, KioskVisitor kioskVisitor, List<Location> locationList)
        {
            InitializeComponent();

            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
            this.locationList = locationList;
        }
        
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // get location from DB
            lvLocations.ItemsSource = locationList;

            lblMessage.Content = "";

            NavigationHelper.Instance.HideLoadingPanel();
        }


        #region Buttons
        private void BackStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void NextStackPanel_Click(object sender, RoutedEventArgs e)
        {
            if (lvLocations.SelectedItem != null)
            {
                Location location = (Location)lvLocations.SelectedItem;
                if (location != null)
                {
                    kioskVisitor.LocationID = location.ID;
                    kioskVisitor.LocationName = location.Name;
                }
                NavigationHelper.Instance.NavigateToNext();
            }
            else
            {
                lblMessage.Content = navigationPage.GetMessages(0);
            }
        }
        #endregion
    }
}
