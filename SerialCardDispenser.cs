﻿using SerialPortLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public class SerialCardDispenser
    {
        private SerialPortInput serialPort;
        private string portName = "";
        private int baudRate = 0;

        // for 
        public int locationIDMin { get; set; }
        public int locationIDMax { get; set; }

        private SerialCardDispenserStatus currentStatus { get; set; }
        //public SerialCardDispenserStatus CurrentStatus { get { return currentStatus; } }

        private readonly static byte[] DispenseCardCommand = { 0x02, 0x44, 0x43, 0x03, 0x06, 0x05 };
        private readonly static byte[] RetrieveCardCommand = { 0x02, 0x43, 0x50, 0x03, 0x12, 0x05 };
        private readonly static byte[] RequestStatusCommand = { 0x02, 0x52, 0x46, 0x03, 0x15, 0x05 };
        private readonly static byte[] ResetConnectionCommand = { 0x02, 0x53, 0x54, 0x03, 0x06, 0x05 };
        private readonly static byte[] EjectCardCommand = { 0x02, 0x46, 0x55, 0x03, 0x12, 0x05 };
        private readonly static byte[] DispenseCardToReadAreaCommand = { 0x02, 0x44, 0x48, 0x03, 0x0D, 0x05 };
        private readonly static byte[] DispenseCardFromReadToFullCommand = { 0x02, 0x45, 0x53, 0x03, 0x17, 0x05 };

        //private readonly static Dictionary<string, string> DispenserStatusList = new Dictionary<string, string>()
        //{
        //    {"383030", "Currently dispensing card."},
        //    {"343030", "Currently retracting card."},
        //    {"323030", "An error has occured."},
        //    {"313030", "Retracted card holder is full."},
        //    {"303830", "Unknown."},
        //    {"303430", "Unknown."},
        //    {"303230", "Card Dispenser is jammed."},
        //    {"303130", "Low on cards."},
        //    {"303038", "No more cards."},
        //    {"303034", "Card is ready for retrieval."},
        //    {"303032", "Card is currently at the reader, waiting for further instructions."},
        //    {"303031", "Card is still being processed. Please wait."},
        //    {"303030", "Card Dispenser is ready on standby for instructions."},
        //    {"000000", "Failed. Please try again."}
        //};

        public SerialCardDispenser(string portName, int baudRate, int locationIDMin, int locationIDMax)
        {
            serialPort = new SerialPortInput();
            serialPort.ConnectionStatusChanged += ConnectionChangedEvent;
            serialPort.MessageReceived += MessageReceivedEvent;

            this.portName = portName;
            this.baudRate = baudRate;
            this.locationIDMin = locationIDMin;
            this.locationIDMax = locationIDMax;

            this.currentStatus = SerialCardDispenserStatus.Offline;
        }

        public bool Connect()
        {
            if (!serialPort.IsConnected)
            {
                // Set port options
                serialPort.SetPort(portName, baudRate);

                // Connect the serial port
                return serialPort.Connect();
            }
            return true;
            //else
            //    return ResetConnection();
        }

        public void Disconnect()
        {
           // if (serialPort.IsConnected)
            serialPort.Disconnect();
            this.currentStatus = SerialCardDispenserStatus.Offline;
        }

        public bool IsConnected()
        {
            return serialPort.IsConnected;
        }


        private void ConnectionChangedEvent(object sender, ConnectionStatusChangedEventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("Connected = {0}", args.Connected);
            if (!args.Connected)
                this.currentStatus = SerialCardDispenserStatus.Offline;

        }

        private void MessageReceivedEvent(object sender, MessageReceivedEventArgs args)
        {
            //Console.WriteLine("Received message: {0}", BitConverter.ToString(args.Data));
            //Console.WriteLine("Received message string: {0}", GetMessage(args.Data));
            GetMessage(args.Data);
        }

        private void GetMessage(byte[] arr)
        {
            if (arr.Length > 7)
            {
                string key = BitConverter.ToString(arr, 4, 3).Replace("-", string.Empty);

                System.Diagnostics.Debug.WriteLine("key = {0}", key);

                int keyInt = 0;
                int.TryParse(key, out keyInt);

                if (Enum.IsDefined(typeof(SerialCardDispenserStatus), keyInt))
                    this.currentStatus = (SerialCardDispenserStatus)keyInt;
            }
        }

        /// <summary>
        /// Call once to dispense the card to the card reader (half dispense), call again to dispense fully (full dispense)
        /// </summary>
        /// <returns></returns>
        public bool DispenseCard()
        {
            return SendMessageToSerial(DispenseCardCommand);
        }

        /// <summary>
        /// Retrieve half dispensed card and full dispensed card
        /// </summary>
        /// <returns></returns>
        public bool RetrieveCard()
        {
            return SendMessageToSerial(RetrieveCardCommand);
        }

        /// <summary>
        /// Request current status of card dispenser
        /// </summary>
        /// <returns></returns>
        public async Task<SerialCardDispenserStatus> RequestStatusAsync()
        {
            //return serialPort.SendMessage(RequestStatusCommand);  // do not use SendMessageToSerial(byte[] value)
            SendMessageToSerial(RequestStatusCommand);

            await Task.Delay(300);

            return this.currentStatus;
        }

        /// <summary>
        /// Reset card dispenser to initial state
        /// </summary>
        /// <returns></returns>
        public bool ResetConnection()
        {
            return SendMessageToSerial(ResetConnectionCommand);
        }

        /// <summary>
        /// Eject card out of the card dispenser (card will fly out)
        /// </summary>
        /// <returns></returns>
        public bool EjectCard()
        {
            return SendMessageToSerial(EjectCardCommand);
        }

        /// <summary>
        /// half dispense a card, similar to DispenseCardCommand, only difference is will do nothing during if card is already half dispensed
        /// </summary>
        /// <returns></returns>
        public bool DispenseCardToReadArea()
        {
            return SendMessageToSerial(DispenseCardToReadAreaCommand);
        }

        /// <summary>
        /// fully dispense a card from half dispensed position, will do nothing if no card is in half dispensed position
        /// </summary>
        /// <returns></returns>
        public bool DispenseCardFromReadToFull()
        {
            return SendMessageToSerial(DispenseCardFromReadToFullCommand);
        }

        private bool SendMessageToSerial(byte[] value)
        {
            //if (!serialPort.IsConnected)
            //    throw new Exception("Card Dispenser is not connected.");
            return serialPort.SendMessage(value);
        }
    }
}
