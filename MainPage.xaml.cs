﻿using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class MainPage : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        private System.Windows.Threading.DispatcherTimer disableScreenTimer;

        public MainPage(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle1.Text = navigationPage.GetTitle(0);
            lblTitle2.Text = navigationPage.GetTitle(1);

            lblMessage.Text = navigationPage.GetMessages(0);


            //for OGS (disable for now)
            //DisableEnableScreen();

            //disableScreenTimer = new System.Windows.Threading.DispatcherTimer();
            //disableScreenTimer.Interval = new TimeSpan(0, 0, 1);
            //disableScreenTimer.Tick += disableScreenTimer_Tick;
            //disableScreenTimer.Start();


            NavigationHelper.Instance.HideLoadingPanel();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (disableScreenTimer != null)
            {
                disableScreenTimer.Stop();
                disableScreenTimer.Tick -= disableScreenTimer_Tick;
            }
        }

        private void disableScreenTimer_Tick(object sender, EventArgs args)
        {
            DisableEnableScreen();
        }

        private void DisableEnableScreen()
        {
            if (ConfigurationStore.Instance.KioskEnabledBySchedule)
            {
                bool disabledByEmptyCard = false;
                // only do this checking if kiosk is enabled
                if (ConfigurationStore.Instance.KioskConfigDisableKioskCardDispenserEmpty) //disable/enable kiosk if card dispenser is empty
                {
                    if (ConfigurationStore.Instance.KioskEnabledHasCard)
                    {
                        if (!btnVisitor.IsEnabled)
                        {
                            btnVisitor.IsEnabled = true;
                            lblMessage.Text = navigationPage.GetMessages(0);
                            panelDisable.Visibility = Visibility.Collapsed;
                            
                        }
                    }
                    else
                    {
                        disabledByEmptyCard = true;
                        if (btnVisitor.IsEnabled)
                        {
                            btnVisitor.IsEnabled = false;
                            lblMessage.Text = navigationPage.GetMessages(2);
                            panelDisable.Visibility = Visibility.Visible;
                            System.Diagnostics.Debug.WriteLine("Disabled by no card in card dispenser");
                        }
                    }
                }

                if (!disabledByEmptyCard) // skip schedule checking
                {
                    if (!btnVisitor.IsEnabled)
                    {
                        btnVisitor.IsEnabled = true;
                        lblMessage.Text = navigationPage.GetMessages(0);
                        panelDisable.Visibility = Visibility.Collapsed;
                    }
                }
            }
            else
            {
                if (btnVisitor.IsEnabled)
                {
                    btnVisitor.IsEnabled = false;
                    lblMessage.Text = navigationPage.GetMessages(1);
                    panelDisable.Visibility = Visibility.Visible;
                    System.Diagnostics.Debug.WriteLine("Disabled by Schedule");
                }
            }
        }

        #region Buttons
        private void VisitorStackPanel_Click(object sender, RoutedEventArgs e)
        {
            kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Visitor;
            NavigationHelper.Instance.SelectedGroup = 0;
            NavigationHelper.Instance.NavigateToNext();
        }

        private void DeliveryStackPanel_Click(object sender, RoutedEventArgs e)
        {
            kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Delivery;
            NavigationHelper.Instance.SelectedGroup = 1;
            NavigationHelper.Instance.NavigateToNext();
        }
        #endregion
    }
}
