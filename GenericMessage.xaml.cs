﻿using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class GenericMessage : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;

        private System.Windows.Threading.DispatcherTimer initialTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;
        
        private int CloseScreenTimerCounter = 5;

        public GenericMessage(Utility.Log.Ilog ilog, NavigationPage navigationPage)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblContent.Text = navigationPage.GetMessages(0);

            initialTimer = new System.Windows.Threading.DispatcherTimer();
            initialTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            CloseScreenTimerCounter = ConfigurationStore.Instance.CloseScreenTimer;
            

            StartInitialTimer();

            NavigationHelper.Instance.HideLoadingPanel();
        }


        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopInitialTimer();
            StopCloseScreenTimer();
        }


        private void StartInitialTimer()
        {
            initialTimerCounter = 1;
            lblCountdown.Content = "";
            initialTimer.Tick -= initialTimer_Tick;
            initialTimer.Tick += initialTimer_Tick;
            initialTimer.Start();
        }
        private void StartCloseScreenTimer()
        {
            countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
            lblCountdown.Content = countDownCounter.ToString();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
            closeScreenTimer.Tick += closeScreenTimer_Tick;
            closeScreenTimer.Start();
        }
        private void StopInitialTimer()
        {
            lock (_lock)
            {
                initialTimer.Stop();
                initialTimer.Tick -= initialTimer_Tick;
            }
        }
        private void StopCloseScreenTimer()
        {
            lock (_lock)
            {
                closeScreenTimer.Stop();
                closeScreenTimer.Tick -= closeScreenTimer_Tick;
            }
        }

        private int initialTimerCounter = 1;
        private void initialTimer_Tick(object sender, EventArgs args)
        {
            if (initialTimerCounter > CloseScreenTimerCounter)
            {
                StopInitialTimer();
                StartCloseScreenTimer();
            }
            initialTimerCounter++;
            CommandManager.InvalidateRequerySuggested();
        }

        private int countDownCounter = 0;
        private object _lock = new object();
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();

            if (countDownCounter < 0)
            {
                //ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigationHelper.Instance.NavigateToMessageBeforeMain();
            }
        }

        #region Buttons

        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }
        private void NextStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToNext();
        }

        #endregion
    }
}
