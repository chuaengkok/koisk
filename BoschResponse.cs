﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model.API.Response
{
    public class BoschResponse
    {
        public bool IsSucceed { get; set; }
        public string Remarks { get; set; }
    }
}
