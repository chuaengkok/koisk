﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using Kiosk.Utility;

namespace Kiosk.Model
{
    public class KioskVisitor
    {
        public VisitType VistorVisitType { get; set; }
        public string VisitorName { get; set; }
        public string VisitorContactNumber { get; set; }
        public string CompanyName { get; set; }
        public bool? Blacklisted { get; set; }
        
        public DateTime? EntryStartDatetime { get; set; }
        public DateTime? EntryEndDatetime { get; set; }
        public string IdentityNumber { get; set; }
        public string IdentityNumberLast4
        {
            get
            {
                return string.IsNullOrWhiteSpace(IdentityNumber) || IdentityNumber.Length < 4 ? "" : IdentityNumber.Substring(IdentityNumber.Length - 4);
            }
        }
        public string IdentityNumberMasked
        {
            get
            {
                return string.IsNullOrWhiteSpace(IdentityNumber) || IdentityNumber.Length < 4 ? "" : new string('*', IdentityNumber.Length - 4) + IdentityNumber.Substring(IdentityNumber.Length - 4);
            }
        }

        public string StringFromInput { get; set; }
        public bool StringFromInputEnd { get; set; } //  carridge return
        public void ResetStringFromInput()
        {
            StringFromInput = string.Empty;
            StringFromInputEnd = false;

            // careful with this, but for now can reset
            //CardNumber = string.Empty;
        }

        public string CardNumber { get; set; }

        public string LocationID { get; set; }
        public string LocationName { get; set; } //meant for tblposition location name

        public string LocationLevel
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(LocationName))
                    return LocationName.Split(' ').Last();
                else
                    return "";
            }
        }

        public string UnitName { get; set; } //meant for location name from fraser portal 
        public string UnitLevel { get; set; }
        public string UnitNumber { get; set; }
        public string UnitNumberFinal
        {
            get
            {
                if (string.IsNullOrWhiteSpace(UnitLevel) && string.IsNullOrWhiteSpace(UnitNumber))
                    return "";
                else if (string.IsNullOrWhiteSpace(UnitNumber))
                    return UnitLevel;
                else if (string.IsNullOrWhiteSpace(UnitLevel))
                    return UnitNumber;
                else
                    return $"{UnitLevel}-{UnitNumber}";
            }
        }
        public string PTWNumber { get; set; }
        public bool? IncludeSaturday { get; set; } = true;
        public bool? IncludeSunday { get; set; } = true;
        public bool? IncludeHoliday { get; set; } = true;

        private BitmapImage _photo;

        public System.Windows.Media.Imaging.BitmapImage Photo
        {
            get { return _photo; }
            set
            {
                _photo = value;
                if (_photo != null)
                {
                    BitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(_photo));

                    using (var ms = new MemoryStream())
                    {
                        try
                        {
                            encoder.Save(ms);
                            PhotoBytes = ms.ToArray();
                        }
                        catch (Exception ex)
                        {
                            
                        }
                    }
                }
                
            }
        }

        public byte[] PhotoBytes { get; private set; }

        public enum VisitType
        {
            Default = 0,
            Visitor = 1 ,
            Contractor = 2 , 
            Delivery = 3,
            Adhoc = 4,
        }

        public byte[] IDPhoto { get; set; }
    }
    
    public static class VisitTypeExtensions
    {
        public static string ToString(this KioskVisitor.VisitType me)
        {
            switch (me)
            {
                case KioskVisitor.VisitType.Visitor: return "V";
                case KioskVisitor.VisitType.Delivery: return "D";
                case KioskVisitor.VisitType.Contractor: return "C";
                case KioskVisitor.VisitType.Adhoc: return "C";
                default: return "";
            }
        }
    }
}
