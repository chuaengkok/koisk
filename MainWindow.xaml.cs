﻿using Kiosk.Model;
using Kiosk.Utility;
using Kiosk.Utility.API;
using Kiosk.Utility.CardDispenserNew;
using Kiosk.Utility.CardDispenserOld;
using Kiosk.Utility.Log;
using Kiosk.Utility.Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Kiosk
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ////private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private KioskVisitor kioskVisitor = new KioskVisitor();

        private Stack<NavigationHelper.Navigate> navigateHistory = new Stack<NavigationHelper.Navigate>();
        private NavigationHelper.Navigate firstPage;

        private List<Model.API.Response.Location> locationList;
        private List<Model.Siemens.AccessGroup> siemensAccessGroups;
        //private List<Model.Siemens.WorkGroup> siemensWorkGroups;

        private Ilog ilog;

        public bool ExpectStringInput { get; set; }

        private string passportScannerError = "";

        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TestData();

            imgLogo.Source = FileUtility.ImageSourceFromFile(ilog, ConfigurationStore.Instance.KioskLogoFilePath, ConfigurationStore.Instance.LogoFileName);
            imgCertisLogo.Source = FileUtility.ImageSourceFromFile(ilog, ConfigurationStore.Instance.KioskLogoFilePath, "CertisLogo.png");


            //for debuggingif 
            //this.WindowState = WindowState.Normal; //maximized for production
            //this.WindowStyle = WindowStyle.ToolWindow; //maximized for production
            //this.ResizeMode = ResizeMode.CanResize;
            //this.Height = 780;// 1024;
            //this.Width = 1280;

            // for production
            this.WindowState = WindowState.Maximized;
            this.WindowStyle = WindowStyle.None;
            this.ResizeMode = ResizeMode.NoResize;

            //this.Width = 1024;
            //this.Height = 1280;



            ilog = new SerilogHelper("LogFiles\\", "DebugLog-.txt", "InfoLog-.txt", "WarningLog-.txt", "ErrorLog-.txt", ConfigurationStore.Instance.IsDebug);
            string error = "";

            //try catch this to handle invalid navigation.json
            try
            {
                string navigationConfigString = ConfigurationStore.Instance.ReadJsonConfigFromFile("Navigation.json");
                NavigationHelper.Instance.LoadPages(this, navigationConfigString);
            }
            catch (Exception ex)
            {
                ilog.LogError("Error loading navigation list", ex);
                error = "Unable to start kiosk (Navigation.Json not found)";
            }

            if (string.IsNullOrWhiteSpace(error))
            {
                ShowPanelLoading();
                firstPage = NavigationHelper.Instance.GetMainPageEnum();

                if (ConfigurationStore.Instance.HideCursor)
                    System.Windows.Input.Mouse.OverrideCursor = System.Windows.Input.Cursors.None;



                switch (ConfigurationStore.Instance.KioskLocation)
                {
                    case "OGS":
                        error = await DoStartupForOGS();
                        break;
                    case "CT":
                    case "SBR":
                        error = await DoStartupForCTSBR();
                        break;
                    case "Fraser":
                        error = await DoStartupForFraser();
                        break;
                    case "JTC SouthWest":
                        error = await DoStartupForJTCSouthWest();
                        break;
                    default:
                        error = await DoStartupForOthers();
                        break;
                }

            }




            //error = "Unable to start kiosk (error=Unable to retrieve SiPass Token)";
            //error = "Unable to start kiosk (error=Unable to retrieve SiPass AccessGroups)";

            //error = "Contact number is blacklisted, please proceed to concierge for assistance";
            //error = "Please proceed to Concierge for assistance as multiple entry detected";

            //ConfigurationStore.Instance.KioskConfigDisableKioskCardDispenserEmpty = false;

            // actual start 
            if (string.IsNullOrWhiteSpace(error))
            {
                NavigationHelper.Instance.NavigateToMain();
            }
            else
            {
                ilog.LogError(error);
                NavigationHelper.Instance.NavigateToError(error, NavigationHelper.Instance.GetGenericMessage("ErrorPageTitle"), true);
            }
        }

        // for fraser add worker
        NavigationPage workerListSummaryNavigationPage = null;
        private List<KioskVisitor> newKioskVisitorList;
        private KioskVisitor existingKioskVisitor;

        public void NavigateContentPage(NavigationHelper.Navigate navigate, NavigationPage navigationPage, bool hideButton = false)
        {
            ShowPanelLoading();

            ExpectStringInput = false;

            if (navigate == firstPage)
                kioskVisitor = new KioskVisitor();
            else
            {
                if (string.IsNullOrWhiteSpace(kioskVisitor.LocationName) && !string.IsNullOrWhiteSpace(kioskVisitor.LocationID))
                {
                    Model.API.Response.Location location = locationList.Find(e => e.ID == kioskVisitor.LocationID);
                    if (location != null)
                        kioskVisitor.LocationName = location.Name;
                }
            }

            switch (navigate)
            {
                case NavigationHelper.Navigate.MainPage:
                    MainContent.Content = new ContentPage.MainPage(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.MainPage2:
                    MainContent.Content = new ContentPage.MainPage2(ilog, navigationPage, kioskVisitor);
                    break;
                //case NavigationHelper.Navigate.MainPage3:
                //    MainContent.Content = new ContentPage.MainPage3(navigationPage, kioskVisitor);
                //    break;
                case NavigationHelper.Navigate.MainPage4:
                    MainContent.Content = new ContentPage.MainPage4(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.StaffAttendance:
                    MainContent.Content = new ContentPage.StaffAttendance(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.PDPAAcknowledgement:
                    MainContent.Content = new ContentPage.PDPAAcknowledgement(ilog, navigationPage);
                    break;
                case NavigationHelper.Navigate.EnterCompanyName:
                    if (navigationPage.SkipIfHasValue && !string.IsNullOrWhiteSpace(kioskVisitor.CompanyName))
                        NavigationHelper.Instance.NavigateToNext();
                    else
                        MainContent.Content = new ContentPage.EnterCompanyName(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.EnterUnitNumber:
                    MainContent.Content = new ContentPage.EnterUnitNumber(ilog, navigationPage, kioskVisitor);
                    break;

                case NavigationHelper.Navigate.EnterName:
                    if (navigationPage.SkipIfHasValue && !string.IsNullOrWhiteSpace(kioskVisitor.VisitorName))
                        NavigationHelper.Instance.NavigateToNext();
                    else
                        MainContent.Content = new ContentPage.EnterName(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.EnterIdentityManual:
                    MainContent.Content = new ContentPage.EnterIdentityManual(ilog, kioskVisitor, locationList);
                    break;
                case NavigationHelper.Navigate.EnterContactNumber:
                    // kioskVisitor.ContactNumber = "112233";
                    if (navigationPage.SkipIfHasValue && !string.IsNullOrWhiteSpace(kioskVisitor.VisitorContactNumber))
                        NavigationHelper.Instance.NavigateToNext();
                    else
                        MainContent.Content = new ContentPage.EnterContactNumber(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.SelectLocation:
                    MainContent.Content = new ContentPage.SelectLocation(ilog, navigationPage, kioskVisitor, locationList, siemensAccessGroups);
                    break;
                case NavigationHelper.Navigate.VisitSummary:
                    ExpectStringInput = true;
                    MainContent.Content = new ContentPage.VisitSummary(ilog, navigationPage, kioskVisitor, siemensAccessGroups);//, siemensWorkGroups);
                    break;
                case NavigationHelper.Navigate.ErrorMessage:
                    MainContent.Content = new ContentPage.ErrorMessage(ilog, navigationPage, hideButton);
                    break;
                case NavigationHelper.Navigate.ScanIdentityCard:
                    MainContent.Content = new ContentPage.ScanIdentityCard(ilog, navigationPage, kioskVisitor, locationList);
                    break;
                case NavigationHelper.Navigate.TakePhoto:
                    MainContent.Content = new ContentPage.TakePhoto(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.EnterPin:
                    MainContent.Content = new ContentPage.EnterPin(ilog, navigationPage);
                    break;

                case NavigationHelper.Navigate.AdminMain:
                    // reset all the adhoc/new list variables
                    newKioskVisitorList = null;
                    existingKioskVisitor = null;
                    MainContent.Content = new ContentPage.AdminMain(ilog, navigationPage, kioskVisitor);
                    break;
                //case NavigationHelper.Navigate.ScanIdentityCardBarcode:
                //    ExpectStringInput = true;
                //    MainContent.Content = new ContentPage.ScanIdentityCardBarcode(navigationPage, kioskVisitor, locationList);
                //    break;
                case NavigationHelper.Navigate.ScanIdentityCardBarcodeNoRescan:
                    ExpectStringInput = true;
                    MainContent.Content = new ContentPage.ScanIdentityCardBarcodeNoRescan(ilog, navigationPage, kioskVisitor);
                    break;


                case NavigationHelper.Navigate.EnterLevel:
                    MainContent.Content = new ContentPage.EnterLevel(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.EnterUnit:
                    MainContent.Content = new ContentPage.EnterUnit(ilog, navigationPage, kioskVisitor);
                    break;



                case NavigationHelper.Navigate.ScanPTWBarcodeNoRescan:
                    ExpectStringInput = true;
                    MainContent.Content = new ContentPage.ScanPTWBarcodeNoRescan(ilog, navigationPage, kioskVisitor);
                    break;

                case NavigationHelper.Navigate.ShowPTWInfo:
                    MainContent.Content = new ContentPage.ShowPTWInfo(ilog, navigationPage, kioskVisitor);
                    break;



                case NavigationHelper.Navigate.ExistingWorkerInfo:
                    MainContent.Content = new ContentPage.ExistingWorkerInfo(ilog, navigationPage, kioskVisitor);
                    break;


                case NavigationHelper.Navigate.WorkerListMain:

                    if (newKioskVisitorList == null)
                    {
                        newKioskVisitorList = new List<KioskVisitor>();
                        existingKioskVisitor = new KioskVisitor();
                        KioskVisitorUtility.CopyDataToKioskVisitor(existingKioskVisitor, kioskVisitor);
                        //existingKioskVisitor.CompanyName = kioskVisitor.CompanyName;
                        //existingKioskVisitor.LocationID = kioskVisitor.LocationID;
                        //existingKioskVisitor.LocationName = kioskVisitor.LocationName;
                        //existingKioskVisitor.VisitorName = kioskVisitor.VisitorName;
                        //existingKioskVisitor.IdentityNumber = kioskVisitor.IdentityNumber;
                        //existingKioskVisitor.EntryStartDatetime = kioskVisitor.EntryStartDatetime;
                        //existingKioskVisitor.EntryEndDatetime = kioskVisitor.EntryEndDatetime;

                        //existingKioskVisitor.UnitName = kioskVisitor.UnitName;

                        //existingKioskVisitor.PTWNumber = existingKioskVisitor.PTWNumber;
                    }
                    else
                    {
                        // not blank means not cancelled pressed
                        if (!string.IsNullOrWhiteSpace(kioskVisitor.VisitorName) &&
                            !string.IsNullOrWhiteSpace(kioskVisitor.VisitorContactNumber) &&
                            !string.IsNullOrWhiteSpace(kioskVisitor.IdentityNumber) &&
                            kioskVisitor.Photo != null)
                            newKioskVisitorList.Add(kioskVisitor);
                    }


                    if (workerListSummaryNavigationPage == null)
                    {
                        workerListSummaryNavigationPage = navigationPage;
                    }

                    MainContent.Content = new ContentPage.WorkerListMain(ilog, workerListSummaryNavigationPage, newKioskVisitorList);
                    break;
                case NavigationHelper.Navigate.AddScanIdentityCardBarcodeNoRescan:
                    ExpectStringInput = true;
                    kioskVisitor = new KioskVisitor();
                    navigationPage = NavigationHelper.Instance.GetSpecialNavigationPage(navigate.ToString());
                    MainContent.Content = new ContentPage.AddScanIdentityCardBarcodeNoRescan(ilog, navigationPage, kioskVisitor, newKioskVisitorList);
                    break;
                case NavigationHelper.Navigate.AddEnterName:
                    navigationPage = NavigationHelper.Instance.GetSpecialNavigationPage(navigate.ToString());
                    MainContent.Content = new ContentPage.AddEnterName(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.AddEnterContactNumber:
                    navigationPage = NavigationHelper.Instance.GetSpecialNavigationPage(navigate.ToString());
                    MainContent.Content = new ContentPage.AddEnterContactNumber(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.AddTakePhoto:
                    navigationPage = NavigationHelper.Instance.GetSpecialNavigationPage(navigate.ToString());
                    MainContent.Content = new ContentPage.AddTakePhoto(ilog, navigationPage, kioskVisitor);
                    break;

                case NavigationHelper.Navigate.WorkerListVisitSummary:
                    MainContent.Content = new ContentPage.WorkerListVisitSummary(ilog, navigationPage, existingKioskVisitor, newKioskVisitorList);
                    break;

                case NavigationHelper.Navigate.WorkerListMainAdhoc:
                    newKioskVisitorList = new List<KioskVisitor>();
                    MainContent.Content = new ContentPage.WorkerListMainAdhoc(ilog, navigationPage, newKioskVisitorList, kioskVisitor);
                    break;

                case NavigationHelper.Navigate.FraserVisitSummary:
                    MainContent.Content = new ContentPage.FraserVisitSummary(ilog, navigationPage, kioskVisitor);
                    break;

                case NavigationHelper.Navigate.ScanIdentityCardJTC:
                    MainContent.Content = new ContentPage.ScanIdentityCardJTC(ilog, navigationPage, kioskVisitor);
                    break;

                case NavigationHelper.Navigate.VisitSummaryJTC:
                    MainContent.Content = new ContentPage.VisitSummaryJTC(ilog, navigationPage, kioskVisitor);
                    break;

                case NavigationHelper.Navigate.GenericMessage:
                    MainContent.Content = new ContentPage.GenericMessage(ilog, navigationPage);
                    break;

                case NavigationHelper.Navigate.TimeoutMessage:
                    //navigationPage = NavigationHelper.Instance.GetSpecialNavigationPage(navigate.ToString()); // handled in navigation helper
                    MainContent.Content = new ContentPage.TimeoutMessage(ilog, navigationPage);
                    break;

                case NavigationHelper.Navigate.WorkerListVisitSummaryJTC:
                    MainContent.Content = new ContentPage.WorkerListVisitSummaryJTC(ilog, navigationPage, existingKioskVisitor, newKioskVisitorList);
                    break;

                // concierge
                case NavigationHelper.Navigate.MainPageConcierge:
                    MainContent.Content = new ContentPage.MainPageConcierge(ilog, navigationPage, kioskVisitor);
                    break;
                case NavigationHelper.Navigate.SignInConcierge:
                    MainContent.Content = new ContentPage.SignInConcierge(ilog, navigationPage, kioskVisitor, locationList);
                    break;
                case NavigationHelper.Navigate.SignOutConcierge:
                    MainContent.Content = new ContentPage.SignOutConcierge(ilog, navigationPage);
                    break;
                case NavigationHelper.Navigate.SignInHistory:
                    MainContent.Content = new ContentPage.SignInHistory(ilog, navigationPage);
                    break;


            }
        }

        public void ShowPanelLoading(string loadingPanelText = "")
        {
            if (string.IsNullOrWhiteSpace(loadingPanelText))
                lblLoadingPanelText.Text = NavigationHelper.Instance.GetGenericMessage("LoadingScreenLoading");
            else
                lblLoadingPanelText.Text = loadingPanelText;

            this.Dispatcher.Invoke(() =>
            {
                panelLoading.Visibility = Visibility.Visible;
                CommandManager.InvalidateRequerySuggested();
            });
        }

        public void HidePanelLoading()
        {
            this.Dispatcher.Invoke(() =>
            {
                panelLoading.Visibility = Visibility.Collapsed;
                CommandManager.InvalidateRequerySuggested();
            });
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (panelLoading.Visibility == Visibility.Collapsed)
                panelLoading.Visibility = Visibility.Visible;
            else
                panelLoading.Visibility = Visibility.Collapsed;
        }

        private void Button_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            NavigationHelper.Instance.NavigateToSelectedMain(ConfigurationStore.Instance.AdminGroupStartIndex);

            //this.Close();
        }

        public void CloseApplication()
        {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Window closing");
            try
            {
                //ilog.LogInfo("window closing");
                //PassportScanner.Instance.StopScanner();
                //WebcamHelper.Instance.StopCamera();
                PassportScanner.Instance.UnloadScanner();

                SerialCardDispenserManager.Instance.CloseAllSerialCardDispenser();
                CardDispenserManager.Instance.CloseAllCardDispenser();


            }
            catch (Exception ex)
            {
                ilog.LogError("Error in window closing", ex);
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }



        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (ExpectStringInput) // only needed for visit summary for the card number
            {
                if (kioskVisitor != null)
                {
                    string temp = KeypressHelper.GetCharFromKey(e.Key).ToString().Trim();


                    if (e.Key == Key.Enter)
                    {
                        kioskVisitor.StringFromInputEnd = true;
                        ExpectStringInput = false;  // prevent double scan so end scanning here 
                        if (ConfigurationStore.Instance.IsDebug) Console.WriteLine("Input Ended");
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(temp))
                        {
                            if (kioskVisitor.StringFromInput == null)
                                kioskVisitor.StringFromInput = "";
                            kioskVisitor.StringFromInput = kioskVisitor.StringFromInput + temp;
                        }

                        if (ConfigurationStore.Instance.IsDebug) Console.WriteLine($"Input {temp}");
                    }

                }
            }
        }


        private async Task<string> DoStartupForOGS()
        {
            string error = "";
            //imgLogo.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"/Image/OneGeorgeStreetSmall.png", UriKind.Relative));

            string cardDispenserConfigString = ConfigurationStore.Instance.ReadJsonConfigFromFile("SerialCardDispenser.json");
            SerialCardDispenserManager.Instance.AddSerialCardDispenser(cardDispenserConfigString);
            SerialCardDispenserManager.Instance.StartSendStatus(ilog, 1000);

            SiemensAPIHelper siemensAPIHelper = new SiemensAPIHelper(ilog, ConfigurationStore.Instance.KioskID, ConfigurationStore.Instance.SiemenAPIUrl);
            string token = await siemensAPIHelper.GetToken(ConfigurationStore.Instance.SiemenAPILogin, ConfigurationStore.Instance.SiemenAPIPassword);
            if (!string.IsNullOrEmpty(token))
            {
                siemensAccessGroups = await siemensAPIHelper.GetAccessGroups(token);

                //siemensWorkGroups = await siemensAPIHelper.GetWorkGroups(token);

                await siemensAPIHelper.Logout(token);

                if (siemensAccessGroups == null || siemensAccessGroups.Count == 0)
                {
                    ilog.LogError("SiPassTokenError"); // log here just in case navigation json not configured properly
                    error = NavigationHelper.Instance.GetGenericMessage("SiPassTokenError");
                }
                else
                {
                    // need to call API to get schedule (for OGS only)

                    GenericAPIHelper genericAPIHelper = new GenericAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout); ;
                    KioskConfig kioskConfig = await genericAPIHelper.GetKioskConfig(ConfigurationStore.Instance.KioskID);

                    if (kioskConfig != null)
                    {
                        ConfigurationStore.Instance.KioskConfigDisableKioskCardDispenserEmpty = kioskConfig.DisableKioskOnCardDispenserEmpty;
                        ConfigurationStore.Instance.VisitorReentryLimitInSeconds = kioskConfig.VisitorReentryLimitInSeconds;

                        ScheduleEnableDisable scheduleEnableDisable = new ScheduleEnableDisable(kioskConfig.KioskMainScheduleCron);
                        await scheduleEnableDisable.StartSchedule(ilog);

                        if (!string.IsNullOrWhiteSpace(kioskConfig.KioskDisableTimingForLevel))
                        {
                            try
                            {
                                KioskDisableLevelTiming[] kioskDisableLevelTimings = Newtonsoft.Json.JsonConvert.DeserializeObject<KioskDisableLevelTiming[]>(kioskConfig.KioskDisableTimingForLevel);
                                if (kioskDisableLevelTimings != null && kioskDisableLevelTimings.Length > 0)
                                    ConfigurationStore.Instance.KioskDisableLevelTimingList.AddRange(kioskDisableLevelTimings);

                                foreach (KioskDisableLevelTiming kioskDisableLevelTiming in ConfigurationStore.Instance.KioskDisableLevelTimingList)
                                {
                                    Console.WriteLine($"Start - {kioskDisableLevelTiming.DisableStartDatetime.TimeOfDay}");
                                    Console.WriteLine($"End - {kioskDisableLevelTiming.DisableEndDatetime.TimeOfDay}");
                                }
                            }
                            catch (Exception ex)
                            {
                                ilog.LogError("Invalid kioskConfig.KioskDisableTimingForLevel", ex);
                            }
                        }
                    }
                    else
                        ilog.LogDebug("Kiosk config NULL");
                }

            }
            else
            {
                ilog.LogError("SiPassAccessGroupError"); // log here just in case navigation json not configured properly
                error = NavigationHelper.Instance.GetGenericMessage("SiPassAccessGroupError");

            }
            return error;
        }
        private async Task<string> DoStartupForCTSBR()
        {
            string error = "";
            //imgLogo.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"/Image/Logo/CapitaLandLogoSmall.png", UriKind.Relative));

            string cardDispenserConfigString = ConfigurationStore.Instance.ReadJsonConfigFromFile("SerialCardDispenser.json");
            SerialCardDispenserManager.Instance.AddSerialCardDispenser(cardDispenserConfigString);
            SerialCardDispenserManager.Instance.StartSendStatus(ilog, 10000);

            try
            {
                PassportScanner.Instance.LoadScanner(ConfigurationStore.Instance.PassportScannerUserID,
                                       ConfigurationStore.Instance.KioskFilePath, ConfigurationStore.Instance.TempVisitorIdentityFileName);
            }
            catch (Exception ex)
            {
                ilog.LogError("PassportScanner error", ex);
                passportScannerError = NavigationHelper.Instance.GetGenericMessage("PassportScannerError");
            }


            int counter = 0;
            do
            {
                GenericAPIHelper genericAPIHelper = new GenericAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                locationList = await genericAPIHelper.GetLocationList(ConfigurationStore.Instance.LocationIndex);

                await Task.Delay(1000);
                counter++;

            } while (counter < 10 && (locationList == null || locationList.Count == 0));

            if (locationList == null || locationList.Count == 0)
            {
                ilog.LogError("LocationListError"); // log here just in case navigation json not configured properly
                error = NavigationHelper.Instance.GetGenericMessage("LocationListError");
            }

            return error;
        }
        private async Task<string> DoStartupForFraser()
        {

            //if (ConfigurationStore.Instance.KioskSubLocation == "NPC-NW")
            //    imgLogo.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"/Image/Logo/NorthpointLogoSmall.png", UriKind.Relative));
            //else if (ConfigurationStore.Instance.KioskSubLocation == "CCP")
            //    imgLogo.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"/Image/Logo/ChangiCityPointLogo.png", UriKind.Relative));



            string error = await GetCurrentKioskLocationID();



            return error;
        }
        private async Task<string> DoStartupForJTCSouthWest()
        {
            string error = await GetLocationListByKioskSubLocation();

            if (string.IsNullOrWhiteSpace(error))
            {
                // no need to do this anymore
                //int counter = 0;
                //do
                //{
                //    GenericAPIHelper genericAPIHelper = new GenericAPIHelper(ilog, ConfigurationStore.Instance.GMSApiURL, ConfigurationStore.Instance.APICallTimeout);
                //    locationList = await genericAPIHelper.GetLocationListByParentID(ConfigurationStore.Instance.LocationID);

                //    await Task.Delay(1000);

                //    counter++;

                //} while (counter < 10 && (locationList == null || locationList.Count == 0));

                //if (locationList == null || locationList.Count == 0)
                //{
                //    ilog.LogError("LocationListError"); // log here just in case navigation json not configured properly
                //    error = NavigationHelper.Instance.GetGenericMessage("LocationListError");
                //}


                try
                {

                    string cardDispenserConfigString = ConfigurationStore.Instance.ReadJsonConfigFromFile("SerialCardDispenser.json");

                    if (!string.IsNullOrWhiteSpace(cardDispenserConfigString)) // if empty means no card dispenser configured
                    {
                        CardDispenserManager.Instance.AddSerialCardDispenser(cardDispenserConfigString);
                        CardDispenserManager.Instance.StartSendStatus(ilog, 10000);
                    }
                }
                catch (Exception ex)
                {
                    ilog.LogError("Card Dispenser error", ex);
                    error = NavigationHelper.Instance.GetGenericMessage("CardDispenserError");
                }


                // not empty then do start up for passport scanner (concierge no passport scanner)
                if (!string.IsNullOrWhiteSpace(ConfigurationStore.Instance.TempVisitorIdentityFileName))
                {
                    try
                    {
                        PassportScanner.Instance.LoadScanner(ConfigurationStore.Instance.PassportScannerUserID,
                                               ConfigurationStore.Instance.KioskFilePath, ConfigurationStore.Instance.TempVisitorIdentityFileName);
                    }
                    catch (Exception ex)
                    {
                        ilog.LogError("PassportScanner error", ex);
                        error = NavigationHelper.Instance.GetGenericMessage("PassportScannerError");
                    }
                }
            }

            return error;
        }

        private async Task<string> DoStartupForOthers()
        {
            string error = await GetLocationListByKioskSubLocation();

            if (string.IsNullOrWhiteSpace(error))
            {
                // no need to do this anymore
                //int counter = 0;
                //do
                //{
                //    GenericAPIHelper genericAPIHelper = new GenericAPIHelper(ilog, ConfigurationStore.Instance.GMSApiURL, ConfigurationStore.Instance.APICallTimeout);
                //    locationList = await genericAPIHelper.GetLocationListByParentID(ConfigurationStore.Instance.LocationID);

                //    await Task.Delay(1000);

                //    counter++;

                //} while (counter < 10 && (locationList == null || locationList.Count == 0));

                //if (locationList == null || locationList.Count == 0)
                //{
                //    ilog.LogError("LocationListError"); // log here just in case navigation json not configured properly
                //    error = NavigationHelper.Instance.GetGenericMessage("LocationListError");
                //}


                try
                {

                    string cardDispenserConfigString = ConfigurationStore.Instance.ReadJsonConfigFromFile("SerialCardDispenser.json");

                    if (!string.IsNullOrWhiteSpace(cardDispenserConfigString)) // if empty means no card dispenser configured
                    {
                        CardDispenserManager.Instance.AddSerialCardDispenser(cardDispenserConfigString);
                        CardDispenserManager.Instance.StartSendStatus(ilog, 10000);
                    }
                }
                catch (Exception ex)
                {
                    ilog.LogError("Card Dispenser error", ex);
                    error = NavigationHelper.Instance.GetGenericMessage("CardDispenserError");
                }


                // not empty then do start up for passport scanner (concierge no passport scanner)
                if (!string.IsNullOrWhiteSpace(ConfigurationStore.Instance.TempVisitorIdentityFileName))
                {
                    try
                    {
                        PassportScanner.Instance.LoadScanner(ConfigurationStore.Instance.PassportScannerUserID,
                                               ConfigurationStore.Instance.KioskFilePath, ConfigurationStore.Instance.TempVisitorIdentityFileName);
                    }
                    catch (Exception ex)
                    {
                        ilog.LogError("PassportScanner error", ex);
                        error = NavigationHelper.Instance.GetGenericMessage("PassportScannerError");
                    }
                }
            }

            return error;
        }


        private async Task<string> GetCurrentKioskLocationID()
        {
            string error = "";
            int counter = 0;
            do
            {
                GenericAPIHelper genericAPIHelper = new GenericAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                locationList = await genericAPIHelper.GetLocationList(ConfigurationStore.Instance.LocationIndex);

                await Task.Delay(1000);
                counter++;


            } while (counter < 10 && (locationList == null || locationList.Count == 0));

            if (locationList == null || locationList.Count == 0)
            {
                ilog.LogError("LocationListError"); // log here just in case navigation json not configured properly
                error = NavigationHelper.Instance.GetGenericMessage("LocationListError");
            }
            else
            {
                Model.API.Response.Location location = locationList.FirstOrDefault(i => i.Name.Equals(ConfigurationStore.Instance.KioskSubLocation, StringComparison.OrdinalIgnoreCase));
                if (location != null)
                    ConfigurationStore.Instance.LocationID = Convert.ToInt32(location.ID);
                else
                    error = NavigationHelper.Instance.GetGenericMessage("LocationConfigError");
            }

            return error;
        }


        private async Task<string> GetLocationListByKioskSubLocation()
        {
            string error = "";
            int counter = 0;
            do
            {
                GenericAPIHelper genericAPIHelper = new GenericAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                locationList = await genericAPIHelper.GetLocationListByKioskSubLocation(ConfigurationStore.Instance.KioskSubLocation);

                await Task.Delay(1000);
                counter++;


            } while (counter < 10 && (locationList == null || locationList.Count == 0));

            if (locationList == null || locationList.Count == 0)
            {
                ilog.LogError("LocationListError"); // log here just in case navigation json not configured properly
                error = NavigationHelper.Instance.GetGenericMessage("LocationListError");
            }
            else
            {
                Model.API.Response.Location location = locationList.FirstOrDefault(i => i.Name.Equals(ConfigurationStore.Instance.KioskSubLocation, StringComparison.OrdinalIgnoreCase));
                if (location != null)
                    ConfigurationStore.Instance.LocationID = Convert.ToInt32(location.ID);
                else
                    error = NavigationHelper.Instance.GetGenericMessage("LocationConfigError");
            }

            return error;
        }

        private async void TestData()
        {
            //GallagherAPIHelper gallagherAPIHelper = new GallagherAPIHelper("", 5);
            //gallagherAPIHelper.CreateCardHolder("Peter", "Contractor", "10101010", DateTime.Now, DateTime.Now.AddHours(2),
            //    "http://divisionURL", "http://accessgroupURL", "http://cardtypeURL");


            //KioskVisitor kioskVisitor = new KioskVisitor();
            //kioskVisitor.IdentityNumber = "S8033985B";

            //Console.WriteLine(kioskVisitor.IdentityNumber);
            //Console.WriteLine(kioskVisitor.IdentityNumberLast4);
            //Console.WriteLine(kioskVisitor.IdentityNumberMasked);

            //kioskVisitor.IdentityNumber = "985B";
            //Console.WriteLine(kioskVisitor.IdentityNumber);
            //Console.WriteLine(kioskVisitor.IdentityNumberLast4);
            //Console.WriteLine(kioskVisitor.IdentityNumberMasked);

            //kioskVisitor.IdentityNumber = "98";
            //Console.WriteLine(kioskVisitor.IdentityNumber);
            //Console.WriteLine(kioskVisitor.IdentityNumberLast4);
            //Console.WriteLine(kioskVisitor.IdentityNumberMasked);


            //kioskVisitor.IdentityNumber = null;
            //Console.WriteLine(kioskVisitor.IdentityNumber);
            //Console.WriteLine(kioskVisitor.IdentityNumberLast4);
            //Console.WriteLine(kioskVisitor.IdentityNumberMasked);



            //kioskVisitor.IdentityNumber = "done";
            //Console.WriteLine(kioskVisitor.IdentityNumber);
            //Console.WriteLine(kioskVisitor.IdentityNumberLast4);
            //Console.WriteLine(kioskVisitor.IdentityNumberMasked);

            // test sqlite VisitorHistory

            //KioskSQLiteHelper.Instance.InsertOrUpdateRecordVisitHistory("99112233");
            //KioskSQLiteHelper.Instance.InsertOrUpdateRecordVisitHistory("99112234");
            //KioskSQLiteHelper.Instance.InsertOrUpdateRecordVisitHistory("99112235");
            //KioskSQLiteHelper.Instance.InsertOrUpdateRecordVisitHistory("99112236");
            //KioskSQLiteHelper.Instance.InsertOrUpdateRecordVisitHistory("99112237");
            //string temp1 = KioskSQLiteHelper.Instance.GetDatetimeVisitHistory("99112235");
            //DateTime datetime = DateTime.Parse(temp1);


            //temp1 = KioskSQLiteHelper.Instance.GetDatetimeVisitHistory("99112236");
            //datetime = DateTime.Parse(temp1);


            //temp1 = KioskSQLiteHelper.Instance.GetDatetimeVisitHistory("99112238");
            //datetime = DateTime.Parse(temp1);

            //System.Threading.Thread.Sleep(10000);

            //ConfigurationStore.Instance.VisitorReentryLimitInSeconds = 15;

            //string errorMessage = "";
            //if (ConfigurationStore.Instance.VisitorReentryLimitInSeconds > 0)
            //{
            //    string lastLoginDatetimeString = Utility.Sqlite.KioskSQLiteHelper.Instance.GetDatetimeVisitHistory("99112236");
            //    DateTime lastLoginDatetime = DateTime.MinValue;
            //    if (DateTime.TryParse(lastLoginDatetimeString, out lastLoginDatetime))
            //    {
            //        if (lastLoginDatetime > DateTime.MinValue)
            //        {
            //            if (lastLoginDatetime.AddSeconds(ConfigurationStore.Instance.VisitorReentryLimitInSeconds) > DateTime.Now)
            //                errorMessage = "Error";
            //        }
            //    }
            //}
            //if (string.IsNullOrWhiteSpace(errorMessage))
            //    NavigationHelper.Instance.NavigateToNext();
            //else
            //    NavigationHelper.Instance.NavigateToError(errorMessage);
            // end test sqlite


            // test sqlite CompanyData
            //System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();

            //CompanyCardSQLiteHelper.Instance.SaveCompanyCollectedCardData("Company AAA");
            //CompanyCardSQLiteHelper.Instance.SaveCompanyCollectedCardData("Company AAA");

            //CompanyCardSQLiteHelper.Instance.SaveCompanyCollectedCardData("Company BBB");
            //CompanyCardSQLiteHelper.Instance.SaveCompanyCollectedCardData("Company BBB");
            //CompanyCardSQLiteHelper.Instance.SaveCompanyCollectedCardData("Company BBB");

            //KioskSQLiteHelper.Instance.InsertOrUpdateRecord("CardDispenser", "Normal");

            //KioskSQLiteHelper.Instance.InsertOrUpdateRecord("CardDispenser", "Low");

            //string carddispenserstatus1 = KioskSQLiteHelper.Instance.GetData("CardDispenser");

            //KioskSQLiteHelper.Instance.InsertOrUpdateRecord("CardDispenser", "Empty");

            //string carddispenserstatus2 = KioskSQLiteHelper.Instance.GetData("CardDispenser");


            //KioskSQLiteHelper.Instance.InsertOrUpdateRecord("Printer", "Ok");

            //string Printer1 = KioskSQLiteHelper.Instance.GetData("Printer");

            //KioskSQLiteHelper.Instance.InsertOrUpdateRecord("Printer", "Empty");

            //string Printer2 = KioskSQLiteHelper.Instance.GetData("Printer");

            //long mili = stopwatch.ElapsedMilliseconds;
            //end test sqlite



            // test dispenser
            //SerialCardDispenser serialCardDispenser = SerialCardDispenserManager.Instance.GetSerialCardDispenserByLevel(kioskVisitor.LocationID);

            //IsInVisitSummary = true;

            //kioskVisitor = new KioskVisitor();


            //await Task.Delay(500);
            //serialCardDispenser.DispenseCardToReadArea();
            //await Task.Delay(1000);

            //ilog.LogInfo($"Card Number = {kioskVisitor.CardNumber}");

            //serialCardDispenser.DispenseCardFromReadToFull();

            //await Task.Delay(1000);

            //this.Close();
            //return;
            // test dispenser end



            //kioskVisitor = new KioskVisitor();
            //NavigationPage navigationPage = new NavigationPage();

            ////kioskVisitor.CompanyName = "ABC PTE LTD";
            //kioskVisitor.Name = "Johnny Tan";
            //kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Visitor;
            //kioskVisitor.LocationName = "A Visitor 15";
            //kioskVisitor.CardNumber = "1111111";


            //navigationPage.Titles = new List<string>();
            //navigationPage.Titles.Add("");
            //navigationPage.Messages = new List<string>();
            //navigationPage.Messages.Add("Your pass is being processed now\nPlease collect it at the bottom slot");
            //navigationPage.Messages.Add("Please collect your card\nCard not collected will be retrieved by kiosk in ...");
            //navigationPage.Messages.Add("Screen closing in ...");
            ////navigationPage.Messages.Add("Unable to dispense card due to wrong name entered, please proceed to concierge for assistance");



            //NavigateContentPage(NavigationHelper.Navigate.VisitSummary, navigationPage);



            //kioskVisitor = new KioskVisitor();
            //NavigateContentPage(NavigationHelper.Navigate.VisitSummary, new NavigationPage());



            // test Siemens integration

            //kioskVisitor = new KioskVisitor();
            //kioskVisitor.Name = "Test111";
            //kioskVisitor.LocationName = "A Visitor 15";
            //kioskVisitor.CardNumber = "22112211";

            //Model.Siemens.AccessGroup accessGroup = siemensAccessGroups.Find(i => i.Name.Equals(kioskVisitor.LocationName, StringComparison.InvariantCultureIgnoreCase));
            //if (accessGroup != null)
            //{
            //    //Model.Siemens.WorkGroup workGroup = siemensWorkGroups.Find(i => i.Name.Equals(kioskVisitor.LocationName, StringComparison.InvariantCultureIgnoreCase));
            //    //if (workGroup != null)
            //    //{

            //    SiemensAPIHelper siemensAPIHelper = new SiemensAPIHelper(ConfigurationStore.Instance.KioskID, ConfigurationStore.Instance.SiemenAPIUrl);
            //    string token = await siemensAPIHelper.GetToken(ConfigurationStore.Instance.SiemenAPILogin, ConfigurationStore.Instance.SiemenAPIPassword);
            //    if (!string.IsNullOrWhiteSpace(token))
            //    {

            //        Model.Siemens.CardHolder cardHolder = await siemensAPIHelper.GetCardHolderByCardNumber(token, kioskVisitor.CardNumber);
            //        if (cardHolder != null)
            //            await siemensAPIHelper.DeleteCardHolderbyID(token, cardHolder.Token);

            //        await siemensAPIHelper.CreateCardHolder(token, kioskVisitor.Name, kioskVisitor.CardNumber,
            //                            accessGroup.Token, accessGroup.Name);//, workGroup.Token.ToString(), workGroup.Name);

            //        await siemensAPIHelper.Logout(token);


            //    }
            //    else
            //    {
            //        error = "Unable to dispense card (error=SiPass empty token)";
            //        ilog.LogError(error);
            //        NavigationHelper.Instance.NavigateToError(error);
            //    }
            //    //}
            //    //else
            //    //{
            //    //    error = "Unable to dispense card for the location selected (error=Invalid SiPass WorkGroup)";
            //    //    ilog.LogError(error);
            //    //    NavigationHelper.Instance.NavigateToError(error);
            //    //}
            //}
            //else
            //{
            //    error = "Unable to dispense card for the location selected (error=Invalid SiPass AccessGroup)";
            //    ilog.LogError(error);
            //    NavigationHelper.Instance.NavigateToError(error);
            //}

            //end test siemens integration



            //BrotherPrinter.Instance.PrintStickerAsync(@"C:\Certis\KioskPrinterFiles\", "Fraser.lbx",
            //                        ConfigurationStore.Instance.TempVisitorPhotoFileName, "Test", "Johnny","TTTTT");


            //SerialCardDispenser serialCardDispenser = SerialCardDispenserManager.Instance.GetSerialCardDispenser(0);
            //serialCardDispenser.Connect();

            //if (serialCardDispenser.IsConnected())
            //{
            //    await Task.Delay(1000);
            //    serialCardDispenser.DispenseCard();
            //    await Task.Delay(1000);
            //    serialCardDispenser.DispenseCard();


            //    System.Threading.Thread.Sleep(30000);

            //    serialCardDispenser.RetrieveCard();
            //}


            //APIHelper aPIHelper = new APIHelper(ConfigurationStore.Instance.GMSApiURL, 5000);
            //int count = await aPIHelper.GetNumberofCardSignOutByCompany("C22 Pte Ltd");

            //HidePanelLoading();
        }

    }
}
