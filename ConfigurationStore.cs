﻿using Kiosk.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public class ConfigurationStore
    {
        public string CameraImageRotateDegree { get; set; }
        public int CountdownTimer { get; set; }
        public int CloseScreenTimer { get; set; }
        public int APICallTimeout { get; set; }

        public string KioskFilePath { get; set; }
        public string KioskLogoFilePath { get; set; }
        public string PrinterTemplateFileName { get; set; }
        public string PrinterBarcodeTemplateFileName { get; set; }

        public string LogoFileName { get; set; }
        public string TempVisitorPhotoFileName { get; set; }
        public string TempVisitorBarcodeFileName { get; set; }
        
        public bool HideCursor { get; set; }
        public bool IsDebug { get; set; }

        
        public string PassportScannerUserID { get; set; }
        public bool AllowsPassportScanning { get; set; }

        public string KioskApiURL { get; set; }
        public string TempVisitorIdentityFileName { get; set; }
        

        public string ButtonTextColour { get; set; }
        public string ButtonTextColourOnEnter { get; set; }

        public string KioskID { get; set; }
        
        public int CardNumberLength { get; set; }

        public string InternalMomAPIUrl { get; set; }
        public bool CheckMOMWorkPermit { get; set; }
        
        public string KioskLocation { get; set; }
        public string KioskSubLocation { get; set; }
        public int LocationID { get; set; }
        public int LocationIndex { get; set; }


        //not from config file (from server) 
        public bool KioskEnabledBySchedule { get; set; } = true;
        public bool KioskEnabledHasCard { get; set; } = true;
        public bool KioskConfigDisableKioskCardDispenserEmpty { get; set; } = false;
        public int VisitorReentryLimitInSeconds { get; set; }

        public string Pincode { get; set; }
        public int AdminGroupStartIndex { get; set; }


        // external API
        public string SiemenAPIUrl { get; set; }
        public string SiemenAPILogin { get; set; }
        public string SiemenAPIPassword { get; set; }
        public string BoschAPIUrl { get; set; }


        //gallagher apis
        public string GallagherAPIUrl { get; set; }
        public string GallagherAPIKey { get; set; }
        public string GallagherAPIDivisionUrl { get; set; }
        public string GallagherAPIAccessGroupUrl { get; set; }
        public string GallagherAPICardTypeUrl { get; set; }


        public List<KioskDisableLevelTiming> KioskDisableLevelTimingList { get; set; }

        //public string CardDispenserPortName { get; set; }
        //public int CardDispenserBaudRate { get; set; }


        private static readonly Lazy<ConfigurationStore> lazy = new Lazy<ConfigurationStore>(() => new ConfigurationStore());
        public static ConfigurationStore Instance { get { return lazy.Value; } }

        public bool FR_CheckQuality
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["FR_CheckQuality_Enable"]))
                {
                    return bool.Parse(ConfigurationManager.AppSettings["FR_CheckQuality_Enable"]);
                }

                return false;
            }
        }
        public bool FR_OneToOneEnabled
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["FR_OneToOne_Enable"]))
                {
                    return bool.Parse(ConfigurationManager.AppSettings["FR_OneToOne_Enable"]);
                }

                return false;
            }
        }
        public decimal FR_OneToOneThredshold
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["FR_OneToOne_Thredshold"]))
                {
                    return decimal.Parse(ConfigurationManager.AppSettings["FR_OneToOne_Thredshold"]);
                }

                return -1;
            }
        }

        public string GMSUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["GMS_Url"];
            }
        }

        private ConfigurationStore()
        {
            if (ConfigurationManager.AppSettings["CameraImageRotateDegree"] != null)
                CameraImageRotateDegree = ConfigurationManager.AppSettings["CameraImageRotateDegree"];
            else
                CameraImageRotateDegree = "0";
            
            if (ConfigurationManager.AppSettings["CountdownTimer"] != null)
                CountdownTimer = Convert.ToInt32(ConfigurationManager.AppSettings["CountdownTimer"]);
            else
                CountdownTimer = 5;

            if (ConfigurationManager.AppSettings["CloseScreenTimer"] != null)
                CloseScreenTimer = Convert.ToInt32(ConfigurationManager.AppSettings["CloseScreenTimer"]);
            else
                CloseScreenTimer = 5;

            if (ConfigurationManager.AppSettings["APICallTimeout"] != null)
                APICallTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["APICallTimeout"]);
            else
                APICallTimeout = 3;

            if (ConfigurationManager.AppSettings["KioskFilePath"] != null)
                KioskFilePath = ConfigurationManager.AppSettings["KioskFilePath"];
            else
                KioskFilePath = "";

            if (ConfigurationManager.AppSettings["KioskLogoFilePath"] != null)
                KioskLogoFilePath = ConfigurationManager.AppSettings["KioskLogoFilePath"];
            else
                KioskLogoFilePath = "";
            

            if (ConfigurationManager.AppSettings["PrinterTemplateFileName"] != null)
                PrinterTemplateFileName = ConfigurationManager.AppSettings["PrinterTemplateFileName"];
            else
                PrinterTemplateFileName = "";

            if (ConfigurationManager.AppSettings["PrinterBarcodeTemplateFileName"] != null)
                PrinterBarcodeTemplateFileName = ConfigurationManager.AppSettings["PrinterBarcodeTemplateFileName"];
            else
                PrinterBarcodeTemplateFileName = "";


            
            if (ConfigurationManager.AppSettings["LogoFileName"] != null)
                LogoFileName = ConfigurationManager.AppSettings["LogoFileName"];
            else
                LogoFileName = "Logo.png";

            if (ConfigurationManager.AppSettings["TempVisitorPhotoFileName"] != null)
                TempVisitorPhotoFileName = ConfigurationManager.AppSettings["TempVisitorPhotoFileName"];
            else
                TempVisitorPhotoFileName = "Temp0001.png";
            
            if (ConfigurationManager.AppSettings["TempVisitorBarcodeFileName"] != null)
                TempVisitorBarcodeFileName = ConfigurationManager.AppSettings["TempVisitorBarcodeFileName"];
            else
                TempVisitorBarcodeFileName = "TempBarcode0001.png";

            

            if (ConfigurationManager.AppSettings["HideCursor"] != null)
                HideCursor = Convert.ToBoolean(ConfigurationManager.AppSettings["HideCursor"]);
            else
                HideCursor = false;

            if (ConfigurationManager.AppSettings["IsDebug"] != null)
                IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["IsDebug"]);
            else
                IsDebug = false;
            

            if (ConfigurationManager.AppSettings["PassportScannerUserID"] != null)
                PassportScannerUserID = ConfigurationManager.AppSettings["PassportScannerUserID"];
            else
                PassportScannerUserID = "";


            if (ConfigurationManager.AppSettings["AllowsPassportScanning"] != null)
                AllowsPassportScanning = Convert.ToBoolean(ConfigurationManager.AppSettings["AllowsPassportScanning"]);
            else
                AllowsPassportScanning = false;
            

            if (ConfigurationManager.AppSettings["GMSApiURL"] != null)
                KioskApiURL = ConfigurationManager.AppSettings["GMSApiURL"];
            else
                KioskApiURL = "";

            if (ConfigurationManager.AppSettings["TempVisitorIdentityFileName"] != null)
                TempVisitorIdentityFileName = ConfigurationManager.AppSettings["TempVisitorIdentityFileName"];
            else
                TempVisitorIdentityFileName = "";

            if (ConfigurationManager.AppSettings["ButtonTextColour"] != null)
                ButtonTextColour = ConfigurationManager.AppSettings["ButtonTextColour"];
            else
                ButtonTextColour = "004289";

            if (ConfigurationManager.AppSettings["ButtonTextColourOnEnter"] != null)
                ButtonTextColourOnEnter = ConfigurationManager.AppSettings["ButtonTextColourOnEnter"];
            else
                ButtonTextColourOnEnter = "52789F";


            if (ConfigurationManager.AppSettings["KioskID"] != null)
                KioskID = ConfigurationManager.AppSettings["KioskID"];
            else
                KioskID = "";

            if (ConfigurationManager.AppSettings["SiemenAPIUrl"] != null)
                SiemenAPIUrl = ConfigurationManager.AppSettings["SiemenAPIUrl"];
            else
                SiemenAPIUrl = "";

            if (ConfigurationManager.AppSettings["SiemenAPILogin"] != null)
                SiemenAPILogin = ConfigurationManager.AppSettings["SiemenAPILogin"];
            else
                SiemenAPILogin = "";

            if (ConfigurationManager.AppSettings["SiemenAPIPassword"] != null)
                SiemenAPIPassword = ConfigurationManager.AppSettings["SiemenAPIPassword"];
            else
                SiemenAPIPassword = "";

            if (ConfigurationManager.AppSettings["BoschAPIUrl"] != null)
                BoschAPIUrl = ConfigurationManager.AppSettings["BoschAPIUrl"];
            else
                BoschAPIUrl = "";

            if (ConfigurationManager.AppSettings["CheckMOMWorkPermit"] != null)
                CheckMOMWorkPermit = Convert.ToBoolean(ConfigurationManager.AppSettings["CheckMOMWorkPermit"]);
            else
                CheckMOMWorkPermit = false;


            if (ConfigurationManager.AppSettings["CardNumberLength"] != null)
                CardNumberLength = Convert.ToInt32(ConfigurationManager.AppSettings["CardNumberLength"]);
            else
                CardNumberLength = 8;

            if (ConfigurationManager.AppSettings["InternalMomAPIUrl"] != null)
                InternalMomAPIUrl = ConfigurationManager.AppSettings["InternalMomAPIUrl"];
            else
                InternalMomAPIUrl = "";

            
            if (ConfigurationManager.AppSettings["KioskLocation"] != null)
                KioskLocation = ConfigurationManager.AppSettings["KioskLocation"];
            else
                KioskLocation = "";

            if (ConfigurationManager.AppSettings["KioskSubLocation"] != null)
                KioskSubLocation = ConfigurationManager.AppSettings["KioskSubLocation"];
            else
                KioskSubLocation = "";

            if (ConfigurationManager.AppSettings["Pincode"] != null)
                Pincode = ConfigurationManager.AppSettings["Pincode"];
            else
                Pincode = "111112";

            if (ConfigurationManager.AppSettings["AdminGroupStartIndex"] != null)
                AdminGroupStartIndex = Convert.ToInt32(ConfigurationManager.AppSettings["AdminGroupStartIndex"]);
            else
                AdminGroupStartIndex = 2;


            if (ConfigurationManager.AppSettings["LocationIndex"] != null)
                LocationIndex = Convert.ToInt32(ConfigurationManager.AppSettings["LocationIndex"]);
            else
                LocationIndex = 1;



            if (ConfigurationManager.AppSettings["GallagherAPIUrl"] != null)
                GallagherAPIUrl = ConfigurationManager.AppSettings["GallagherAPIUrl"];
            else
                GallagherAPIUrl = "";

            if (ConfigurationManager.AppSettings["GallagherAPIKey"] != null)
                GallagherAPIKey = ConfigurationManager.AppSettings["GallagherAPIKey"];
            else
                GallagherAPIKey = "";

            if (ConfigurationManager.AppSettings["GallagherAPIDivisionUrl"] != null)
                GallagherAPIDivisionUrl = ConfigurationManager.AppSettings["GallagherAPIDivisionUrl"];
            else
                GallagherAPIDivisionUrl = "";

            if (ConfigurationManager.AppSettings["GallagherAPIAccessGroupUrl"] != null)
                GallagherAPIAccessGroupUrl = ConfigurationManager.AppSettings["GallagherAPIAccessGroupUrl"];
            else
                GallagherAPIAccessGroupUrl = "";

            if (ConfigurationManager.AppSettings["GallagherAPICardTypeUrl"] != null)
                GallagherAPICardTypeUrl = ConfigurationManager.AppSettings["GallagherAPICardTypeUrl"];
            else
                GallagherAPICardTypeUrl = "";


            //KioskEnabledBySchedule = true;
            //KioskEnabledHasCard = true;
            //KioskConfigDisableKioskCardDispenserEmpty = false; // if true means will disable


            VisitorReentryLimitInSeconds = -1;

            KioskDisableLevelTimingList = new List<KioskDisableLevelTiming>();


            //foreach (NameValueConfigurationCollection temp in ConfigurationManager.AppSettings)
            //{

            //}


            




        //if (ConfigurationManager.AppSettings["CardDispenserPortName"] != null)
        //    CardDispenserPortName = ConfigurationManager.AppSettings["CardDispenserPortName"];
        //else
        //    CardDispenserPortName = "";

            //if (ConfigurationManager.AppSettings["CardDispenserBaudRate"] != null)
            //    CardDispenserBaudRate = Convert.ToInt32(ConfigurationManager.AppSettings["CardDispenserBaudRate"]);
            //else
            //    CardDispenserBaudRate = 9600;

        }

        public string ReadJsonConfigFromFile(string fileName)
        {
            string returnString = "";
            using (System.IO.StreamReader file = System.IO.File.OpenText(fileName))
            {
                returnString = file.ReadToEnd();
            }

            return returnString;
        }

        private class AuthToken
        {
            public string Token { get; set; }
            public DateTime ExpiryDate { get; set; }
        }
    }
}
