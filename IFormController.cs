﻿
namespace Kiosk.Controller
{
    public interface IFormController
    {
        string Load();
        string Submit(); 
    }
}
