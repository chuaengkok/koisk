﻿using MessagingToolkit.QRCode.Codec;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public static class QRCodeHelper
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void GenerateQRCode(Utility.Log.Ilog ilog, string path, string fileName, string content)
        {
            if (!string.IsNullOrWhiteSpace(content))
            {
                try
                {
                    QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
                    Bitmap qrcode = qrCodeEncoder.Encode(content);

                    qrcode.Save(path + fileName, ImageFormat.Png);

                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception in generating QRCode", ex);
                }
            }
        }
    }
}
