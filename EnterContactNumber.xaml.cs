﻿using Kiosk.Model;
using Kiosk.Utility;
using Kiosk.Utility.API;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Kiosk.ContentPage
{
    public partial class EnterContactNumber : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;
        
        private System.Windows.Threading.DispatcherTimer initialTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;

        private bool isLocalNumber;

        private int CloseScreenTimerCounter = 5;
        public EnterContactNumber(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblMessage.Text = "";
            
            initialTimer = new System.Windows.Threading.DispatcherTimer();
            initialTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            CloseScreenTimerCounter = ConfigurationStore.Instance.CloseScreenTimer;

            //if (!string.IsNullOrWhiteSpace(kioskVisitor.ContactNumber))
            //{
            //    NavigationHelper.Instance.NavigateToNext();
            //}
            //else

            //{
                StartInitialTimer();

                NavigationHelper.Instance.HideLoadingPanel();

                isLocalNumber = true;
                txtPlusSign.Visibility = Visibility.Hidden;
           // }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopInitialTimer();
            StopCloseScreenTimer();
        }

        private int initialTimerCounter = 1;
        private void initialTimer_Tick(object sender, EventArgs args)
        {
            if (initialTimerCounter > CloseScreenTimerCounter)
            {
                StopInitialTimer();
                StartCloseScreenTimer();
            }
            initialTimerCounter++;
            CommandManager.InvalidateRequerySuggested();
        }

        private int countDownCounter = 0;
        private object _lock = new object();
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();
            
            if (countDownCounter < 0)
            {
                //ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigationHelper.Instance.NavigateToMessageBeforeMain();
            }
        }

        private void StartInitialTimer()
        {
            initialTimerCounter = 1;
            lblMessage.Text = "";
            lblCountdown.Content = "";
            initialTimer.Tick -= initialTimer_Tick;
            initialTimer.Tick += initialTimer_Tick;
            initialTimer.Start();
        }
        private void StartCloseScreenTimer()
        {
            countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
            lblMessage.Text = navigationPage.GetMessages(1);
            lblCountdown.Content = countDownCounter.ToString();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
            closeScreenTimer.Tick += closeScreenTimer_Tick;
            closeScreenTimer.Start();
        }
        private void StopInitialTimer()
        {
            lock (_lock)
            {
                initialTimer.Stop();
                initialTimer.Tick -= initialTimer_Tick;
            }
        }
        private void StopCloseScreenTimer()
        {
            lock (_lock)
            {
                closeScreenTimer.Stop();
                closeScreenTimer.Tick -= closeScreenTimer_Tick;
            }
        }

        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void RefreshNoActivityTimer()
        {
            lock (_lock)
            {
                if (initialTimer.IsEnabled)
                    initialTimerCounter = 1;
                else
                {
                    if (closeScreenTimer.IsEnabled)
                    {
                        StopCloseScreenTimer();
                        if (!initialTimer.IsEnabled)
                            StartInitialTimer();
                    }
                }
            }
        }

        #region Buttons
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();

            Button button = (Button)sender;
            string value = button.CommandParameter.ToString();
            switch (value.ToUpper())
            {
                case "BACK":
                    if (!string.IsNullOrWhiteSpace(txtInput.Text))
                        txtInput.Text = txtInput.Text.Remove(txtInput.Text.Length - 1);
                    break;
                case "C":
                    txtInput.Text = "";
                    lblMessage.Text = "";
                    break;
                default:
                    if (isLocalNumber)
                    {
                        if (txtInput.Text.Length < 8)
                            txtInput.Text = txtInput.Text + value;
                    }
                    else
                    {
                        if (txtInput.Text.Length < 16)
                            txtInput.Text = txtInput.Text + value;
                    }
                    break;
            }
        }

        private void BtnLocalForeign_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();

            txtInput.Text = "";
            if (sender == btnLocalNumber)
            {
                isLocalNumber = true;
                txtPlusSign.Visibility = Visibility.Hidden;

                btnLocalNumber.Foreground = Brushes.Black;
                btnLocalNumber.Background = Brushes.LightGray;

                btnForeignNumber.Foreground = Brushes.White;
                btnForeignNumber.Background = (Brush)new BrushConverter().ConvertFrom("#002856");
            }
            else
            {
                isLocalNumber = false;
                txtPlusSign.Visibility = Visibility.Visible;

                btnForeignNumber.Foreground = Brushes.Black;
                btnForeignNumber.Background = Brushes.LightGray;

                btnLocalNumber.Foreground = Brushes.White;
                btnLocalNumber.Background = (Brush)new BrushConverter().ConvertFrom("#002856");
            }
            //#002856
        }

        private void BackStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private async void NextStackPanel_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();

            if (string.IsNullOrWhiteSpace(txtInput.Text))
            {
                lblMessage.Text = navigationPage.GetMessages(0);
                return;
            }
            else if (isLocalNumber)
            {
                Match match = Regex.Match(txtInput.Text, @"[6|8|9]\d{7}");
                if (!match.Success)
                {
                    lblMessage.Text = navigationPage.GetMessages(0);
                    return;
                }
            }

            kioskVisitor.VisitorContactNumber = txtInput.Text;
            
            if (ConfigurationStore.Instance.KioskLocation == "OGS")
            {
                NavigationHelper.Instance.ShowLoadingPanel(NavigationHelper.Instance.GetGenericMessage("LoadingScreenVerifying"));


                // infinite 
                InfiniteAPIHelper infiniteAPIHelper = new InfiniteAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                Model.API.Response.InfiniteVisitor infiniteVisitor = await infiniteAPIHelper.GetInfiniteVisitor(kioskVisitor.VisitorContactNumber);
                
                NavigationHelper.Instance.HideLoadingPanel();

                if (infiniteVisitor == null)
                    NavigationHelper.Instance.NavigateToError(navigationPage.GetMessages(2));
                else if (infiniteVisitor.Blacklist == true)
                    NavigationHelper.Instance.NavigateToError(navigationPage.GetMessages(3));
                else
                {
                    kioskVisitor.VisitorName = infiniteVisitor.Name;

                    // check last visit datetime for multiple entry
                    if (ConfigurationStore.Instance.VisitorReentryLimitInSeconds > 0 &&
                        infiniteVisitor.Check_in.HasValue && 
                        infiniteVisitor.Check_in.Value.AddSeconds(ConfigurationStore.Instance.VisitorReentryLimitInSeconds) > DateTime.Now)
                    {
                        //string lastLoginDatetimeString = Utility.Sqlite.KioskSQLiteHelper.Instance.GetDatetimeVisitHistory(kioskVisitor.ContactNumber);
                        //DateTime lastLoginDatetime = DateTime.MinValue;
                        //if (DateTime.TryParse(lastLoginDatetimeString, out lastLoginDatetime))
                        //{
                        //    if (lastLoginDatetime > DateTime.MinValue)
                        //    {
                        //        if (lastLoginDatetime.AddSeconds(ConfigurationStore.Instance.VisitorReentryLimitInSeconds) > DateTime.Now)
                        //            errorMessage = navigationPage.GetMessages(4);
                        //    }
                        //}

                        NavigationHelper.Instance.NavigateToError(navigationPage.GetMessages(4));
                    }
                    else
                        NavigationHelper.Instance.NavigateToNext();
                }
            }
            else
                NavigationHelper.Instance.NavigateToNext();
        }
        #endregion
    }
}
