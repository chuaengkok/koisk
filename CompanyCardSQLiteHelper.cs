﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Utility.Sqlite
{
    public class CompanyCardSQLiteHelper
    {
        private static readonly Lazy<CompanyCardSQLiteHelper> lazy = new Lazy<CompanyCardSQLiteHelper>(() => new CompanyCardSQLiteHelper());
        public static CompanyCardSQLiteHelper Instance { get { return lazy.Value; } }
        
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly string ConnectionString = "Data Source=CompanyCard.db; Version=3; New=True;";
        private static readonly string CompanyCardRecordTableName = "CompanyCardRecord";

        
        private CompanyCardSQLiteHelper()
        {
            CreateTable();
        }

        private SQLiteConnection CreateConnection()
        {
            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(ConnectionString);
           // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                throw new SQLiteException("SQLite error in CreateConnection", ex);
            }
            return sqlite_conn;
        }

        private void CloseConnection(SQLiteConnection sqlite_conn)
        {
            try
            {
                sqlite_conn.Close();
                sqlite_conn.Dispose();
            }
            catch (Exception ex)
            {
                throw new SQLiteException("SQLite error in CloseConnection", ex);
            }
        }

        private void CreateTable()
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"SELECT name FROM sqlite_master WHERE name='{CompanyCardRecordTableName}'";
            var name = sqlite_cmd.ExecuteScalar();

            // check account table exist or not 
            if (name == null || name.ToString() != CompanyCardRecordTableName)
            {
                string Createsql = $"CREATE TABLE {CompanyCardRecordTableName} (CompanyName VARCHAR(100), CreatedDatetime DATETIME)";
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = Createsql;
                sqlite_cmd.ExecuteNonQuery();
            }

            CloseConnection(conn);
        }

        public void SaveCompanyCollectedCardData(string companyName)
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"INSERT INTO {CompanyCardRecordTableName} VALUES(@companyname, @datetime); ";
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@companyname", companyName));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));

            sqlite_cmd.ExecuteNonQuery();

            CloseConnection(conn);
        }

        public int GetCountByCompanyNameToday(string companyName)
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"SELECT COUNT(1) FROM {CompanyCardRecordTableName} WHERE CompanyName = '{companyName}' AND CreatedDatetime BETWEEN @startDate AND @endDate";
            //sqlite_cmd.Parameters.AddWithValue("@startDate", DateTime.Now.Date);
            //sqlite_cmd.Parameters.AddWithValue("@endDate", DateTime.Now.AddDays(1).Date);

            sqlite_cmd.Parameters.Add(new SQLiteParameter("@startDate", DateTime.Now.Date));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@endDate", DateTime.Now.AddDays(1).Date));

            var count = sqlite_cmd.ExecuteScalar();
            int countInt = 0;
            
            if (count != null)
                int.TryParse(count.ToString(), out countInt);

            CloseConnection(conn);

            return countInt;
        }
    }
}