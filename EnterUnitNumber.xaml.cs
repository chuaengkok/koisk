﻿using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class EnterUnitNumber : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        private System.Windows.Threading.DispatcherTimer initialTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;

        private int CloseScreenTimerCounter = 5;
        public EnterUnitNumber(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblMessage.Text = navigationPage.GetMessages(0);
            
            initialTimer = new System.Windows.Threading.DispatcherTimer();
            initialTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            CloseScreenTimerCounter = ConfigurationStore.Instance.CloseScreenTimer;

            txtInput.Text = "";

            StartInitialTimer();

            NavigationHelper.Instance.HideLoadingPanel();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopInitialTimer();
            StopCloseScreenTimer();
        }

        private int initialTimerCounter = 1;
        private void initialTimer_Tick(object sender, EventArgs args)
        {
            if (initialTimerCounter > CloseScreenTimerCounter)
            {
                StopInitialTimer();
                StartCloseScreenTimer();
            }
            initialTimerCounter++;
            CommandManager.InvalidateRequerySuggested();
        }

        private int countDownCounter = 0;
        private object _lock = new object();
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();
            
            if (countDownCounter < 0)
            {
                //ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigationHelper.Instance.NavigateToMessageBeforeMain();
            }
        }

        private void StartInitialTimer()
        {
            initialTimerCounter = 1;
            lblMessage.Text = navigationPage.GetMessages(0);
            lblCountdown.Content = "";
            initialTimer.Tick -= initialTimer_Tick;
            initialTimer.Tick += initialTimer_Tick;
            initialTimer.Start();
        }
        private void StartCloseScreenTimer()
        {
            countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
            lblMessage.Text = navigationPage.GetMessages(1);
            lblCountdown.Content = countDownCounter.ToString();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
            closeScreenTimer.Tick += closeScreenTimer_Tick;
            closeScreenTimer.Start();
        }
        private void StopInitialTimer()
        {
            lock (_lock)
            {
                initialTimer.Stop();
                initialTimer.Tick -= initialTimer_Tick;
            }
        }
        private void StopCloseScreenTimer()
        {
            lock (_lock)
            {
                closeScreenTimer.Stop();
                closeScreenTimer.Tick -= closeScreenTimer_Tick;
            }
        }

        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void RefreshNoActivityTimer()
        {
            lock (_lock)
            {
                if (initialTimer.IsEnabled)
                    initialTimerCounter = 1;
                else
                {
                    if (closeScreenTimer.IsEnabled)
                    {
                        StopCloseScreenTimer();
                        if (!initialTimer.IsEnabled)
                            StartInitialTimer();
                    }
                }
            }
        }

        #region Buttons
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();
            lblMessage.Text = "";

            Button button = (Button)sender;
            string value = button.CommandParameter.ToString();
            switch (value.ToUpper())
            {
                case "BACK":
                    if (!string.IsNullOrWhiteSpace(txtInput.Text))
                        txtInput.Text = txtInput.Text.Remove(txtInput.Text.Length - 1);
                    break;
                case "C":
                    txtInput.Text = "";
                    lblMessage.Text = "";
                    break;
                case "B" :
                    if (string.IsNullOrWhiteSpace(txtInput.Text))
                        txtInput.Text = "B";
                    break;
                case "-":
                    if (!string.IsNullOrWhiteSpace(txtInput.Text) && !txtInput.Text.Contains('-') && txtInput.Text != "B")
                        txtInput.Text = txtInput.Text + value;
                    break;
                default:
                    if (txtInput.Text.Length < 7)
                    {
                        txtInput.Text = txtInput.Text + value;
                    }
                    break;
            }
        }
        
        private void BackStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void NextStackPanel_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();
            if (!string.IsNullOrWhiteSpace(txtInput.Text))
            {
                kioskVisitor.UnitNumber = txtInput.Text.Trim();
                NavigationHelper.Instance.NavigateToNext();
            }
            else
                lblMessage.Text = navigationPage.GetMessages(0);
        }
        #endregion
    }
}
