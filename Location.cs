﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model.API.Response
{
    public class Location
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public override string ToString() { return Name; }
        
        public int GetLevel()
        {
            int levelInt = 0;
            string[] splitName = Name.Split(' ','-');
            if (splitName != null && splitName.Length > 1)
                int.TryParse(splitName[0], out levelInt);
            return levelInt;
        }
    }
}
