﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model
{
    public class KioskDisableLevelTiming
    {
        public int Level { get; set; }
        public int DisableStartHour { get; set; }
        public int DisableStartMinute { get; set; }
        public int DisableDurationInMinute { get; set; }

        private DateTime? _DisableStartDatetime = null;
        private DateTime? _DisableEndDatetime = null;

        public DateTime DisableStartDatetime
        {
            get
            {
                if (_DisableStartDatetime.HasValue)
                    return _DisableStartDatetime.Value;
                else
                {
                    _DisableStartDatetime = DateTime.MinValue.Date.AddHours(DisableStartHour).AddMinutes(DisableStartMinute);
                    return _DisableStartDatetime.Value;
                }
            }
        }
        public DateTime DisableEndDatetime
        {
            get
            {
                if (_DisableEndDatetime.HasValue)
                    return _DisableEndDatetime.Value;
                else
                {
                    _DisableEndDatetime = DateTime.MinValue.Date.AddHours(DisableStartHour).AddMinutes(DisableStartMinute + DisableDurationInMinute);
                    return _DisableEndDatetime.Value;
                }
            }
        }

    }
}
