﻿
using System.Collections.Generic;

namespace Kiosk.Model.API.Response
{
    public class APIResultSignInHistory
    {
        public bool IsSuccess { get; set; }
        public string ServerErrorMessage { get; set; }
        public string ServerErrorCode { get; set; }
        public List<SignInHistoryResponse> Data { get; set; }
    }
}
