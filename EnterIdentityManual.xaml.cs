﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using Kiosk.Model.Mom;
using Kiosk.Utility;
using Kiosk.Utility.API;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class EnterIdentityManual : UserControl
    {
        //private NavigationPage navigationPage;
        private Utility.Log.Ilog ilog;
        private KioskVisitor kioskVisitor;

        private System.Windows.Threading.DispatcherTimer initialTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;

        private int CloseScreenTimerCounter = 5;
        private List<Location> locationList;
        
        public EnterIdentityManual(Utility.Log.Ilog ilog, KioskVisitor kioskVisitor, List<Location> locationList)
        {
            InitializeComponent();

            this.ilog = ilog;
            //this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
            this.locationList = locationList;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblMessage.Text = "";

            initialTimer = new System.Windows.Threading.DispatcherTimer();
            initialTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            CloseScreenTimerCounter = ConfigurationStore.Instance.CloseScreenTimer;

            StartInitialTimer();

            NavigationHelper.Instance.HideLoadingPanel();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopInitialTimer();
            StopCloseScreenTimer();
        }

        private int initialTimerCounter = 1;
        private void initialTimer_Tick(object sender, EventArgs args)
        {
            if (initialTimerCounter > CloseScreenTimerCounter)
            {
                StopInitialTimer();
                StartCloseScreenTimer();
            }
            initialTimerCounter++;
            CommandManager.InvalidateRequerySuggested();
        }

        private int countDownCounter = 0;
        private object _lock = new object();
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();

            if (countDownCounter < 0)
            {
                //ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigationHelper.Instance.NavigateToMessageBeforeMain();
            }
        }

        private void StartInitialTimer()
        {
            initialTimerCounter = 1;
            lblMessage.Text = "";
            lblCountdown.Content = "";
            initialTimer.Tick -= initialTimer_Tick;
            initialTimer.Tick += initialTimer_Tick;
            initialTimer.Start();
        }
        private void StartCloseScreenTimer()
        {
            countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
            lblMessage.Text = "No activity detected, closing screen in..";
            lblCountdown.Content = countDownCounter.ToString();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
            closeScreenTimer.Tick += closeScreenTimer_Tick;
            closeScreenTimer.Start();
        }
        private void StopInitialTimer()
        {
            lock (_lock)
            {
                initialTimer.Stop();
                initialTimer.Tick -= initialTimer_Tick;
            }
        }
        private void StopCloseScreenTimer()
        {
            lock (_lock)
            {
                closeScreenTimer.Stop();
                closeScreenTimer.Tick -= closeScreenTimer_Tick;
            }
        }

        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void RefreshNoActivityTimer()
        {
            lock (_lock)
            {
                if (initialTimer.IsEnabled)
                    initialTimerCounter = 1;
                else
                {
                    if (closeScreenTimer.IsEnabled)
                    {
                        StopCloseScreenTimer();
                        if (!initialTimer.IsEnabled)
                            StartInitialTimer();
                    }
                }
            }
        }

        #region Buttons
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();

            Button button = (Button)sender;
            string value = button.CommandParameter.ToString();
            if (value.ToUpper() == "BACK")
            {
                if (!string.IsNullOrWhiteSpace(txtInput.Text))
                    txtInput.Text = txtInput.Text.Remove(txtInput.Text.Length - 1);

                //if (!string.IsNullOrWhiteSpace(identityNumberFull))
                //    identityNumberFull = identityNumberFull.Remove(identityNumberFull.Length - 1);
            }
            else
            {
                if (txtInput.Text.Length < 9)
                {
                    //identityNumberFull = identityNumberFull + value;
                    txtInput.Text = txtInput.Text + value;
                }
            }
        }



        private void BackStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private async void NextStackPanel_Click(object sender, RoutedEventArgs e)
        {
            StopCloseScreenTimer();
            StopInitialTimer();

            if (!string.IsNullOrWhiteSpace(txtInput.Text))
            {

                string identityNumberLastFour = "";
                if (txtInput.Text.Length > 3)
                    identityNumberLastFour = txtInput.Text.Substring(txtInput.Text.Length - 4);

                kioskVisitor.IdentityNumber = identityNumberLastFour;

                //if (kioskVisitor.VistorVisitType == KioskVisitor.VisitType.Contractor)
                //{
                //    NavigationHelper.Instance.ShowLoadingPanel("Verifying...");

                //    CapitalandAPIHelper capitalandAPIHelper = new CapitalandAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                //    string errorMessage = await capitalandAPIHelper.CheckValidSignIn(kioskVisitor, (int)this.kioskVisitor.VistorVisitType, kioskVisitor.IdentityNumber);
                //    if (string.IsNullOrWhiteSpace(errorMessage))
                //    {
                //        // only foreign contractor need to verify MOM
                //        if (ConfigurationStore.Instance.CheckMOMWorkPermit && !txtInput.Text.ToUpper().StartsWith("T") && !txtInput.Text.ToUpper().StartsWith("S"))
                //        {

                //            MomAPIHelper momAPIHelper = new MomAPIHelper(ilog, ConfigurationStore.Instance.InternalMomAPIUrl, ConfigurationStore.Instance.APICallTimeout);
                //            WorkerStatus workerStatus = await momAPIHelper.GetWorkerStatus(txtInput.Text);

                //            NavigationHelper.Instance.HideLoadingPanel();
                //            if (workerStatus != null && workerStatus.valid)
                //            {
                //                Location location = locationList.Find(l => l.ID == kioskVisitor.LocationID);
                //                if (location != null)
                //                    kioskVisitor.LocationName = location.Name;

                //                NavigationHelper.Instance.NavigateToNext();
                //            }
                //            else
                //            {
                //                if (workerStatus == null)
                //                    NavigationHelper.Instance.NavigateToError("Unable to verify worker status");
                //                else
                //                    NavigationHelper.Instance.NavigateToError(workerStatus.message);
                //            }
                //        }
                //        else
                //            NavigationHelper.Instance.NavigateToNext();
                //    }
                //    else
                //        NavigationHelper.Instance.NavigateToError(errorMessage);
                //}
                //else
                    NavigationHelper.Instance.NavigateToNext();
            }
            else
            {
                lblMessage.Text = "Please enter a valid Identity/Passport Number";
                StartInitialTimer();
            }
        }
        #endregion
    }
}
