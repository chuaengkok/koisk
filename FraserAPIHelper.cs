﻿using Kiosk.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kiosk.Model.API.Response;
using Kiosk.Model.API;

namespace Kiosk.Utility.API
{
    public class FraserAPIHelper : APIHelper
    {
        public FraserAPIHelper(Utility.Log.Ilog ilog, string url, int apiCallTimeout) : base (ilog, url, apiCallTimeout)
        {
        }
        public async Task<APIResultFraser> GetPTWDetails(string ptwNumber, int locationID)
        {
            APIResultFraser apiResult = new APIResultFraser();
            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        PTWNumber = ptwNumber,
                        LocationID = locationID
                    };

                    IRestResponse restResponse = GenericRestAPICall("/api/GetPTWDetails", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                    {
                        apiResult = JsonConvert.DeserializeObject<APIResultFraser>(restResponse.Content);

                        if (!string.IsNullOrWhiteSpace(apiResult.ServerErrorCode))
                            ilog.LogDebug($"API Result error message {apiResult.ServerErrorCode}");
                    }
                    else
                    {
                        if (!restResponse.IsSuccessful)
                            ilog.LogDebug($"restResponse error message : {restResponse.ErrorMessage}");

                        if (string.IsNullOrWhiteSpace(apiResult.ServerErrorCode))
                        {
                            APIMessage apiMessage = ErrorMessageHelper.Instance.GetLocalServerError();
                            apiResult.ServerErrorCode = apiMessage.Code;
                            apiResult.ServerErrorMessage = apiMessage.Message;
                        }
                    }
                }
                catch (Exception ex)
                {
                    APIMessage apiMessage = ErrorMessageHelper.Instance.GetLocalServerError();
                    apiResult.ServerErrorCode = apiMessage.Code;
                    apiResult.ServerErrorMessage = apiMessage.Message;
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult;
        }

        public async Task<APIResultFraser> CheckValidSignIn(int visitorTypeID, string identityNumber, string identityNumberLast4, int locationID)
        {
            APIResultFraser apiResult = new APIResultFraser();
            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        VisitorTypeID = visitorTypeID,
                        IdentityNumber = identityNumber,
                        IdentityNumberLast4 = identityNumberLast4,
                        LocationID = locationID
                    };

                    IRestResponse restResponse = GenericRestAPICall("/api/CheckValidSignInFraser", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                    {
                        apiResult = JsonConvert.DeserializeObject<APIResultFraser>(restResponse.Content);

                        if (!string.IsNullOrWhiteSpace(apiResult.ServerErrorCode))
                            ilog.LogDebug($"API Result error message {apiResult.ServerErrorCode}");
                    }
                    else
                    {
                        if (!restResponse.IsSuccessful)
                            ilog.LogDebug($"restResponse error message : {restResponse.ErrorMessage}");

                        if (string.IsNullOrWhiteSpace(apiResult.ServerErrorCode))
                        {
                            APIMessage apiMessage = ErrorMessageHelper.Instance.GetLocalServerError();
                            apiResult.ServerErrorCode = apiMessage.Code;
                            apiResult.ServerErrorMessage = apiMessage.Message;
                        }
                    }
                }
                catch (Exception ex)
                {
                    APIMessage apiMessage = ErrorMessageHelper.Instance.GetLocalServerError();
                    apiResult.ServerErrorCode = apiMessage.Code;
                    apiResult.ServerErrorMessage = apiMessage.Message;
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult;
        }

        public async Task<APIResultFraser> SaveVisitInfo(int visitorTypeID, string identityNumberLast4, string visitorName, 
                    string visitorContactNumber, string locationID, string cardNumber, string companyName,
                    string unitName, string unitNumber, string ptwNumber,
                    bool? includeSaturday, bool? includeSunday, bool? includeHoliday,
                    DateTime? entryStartDatetime, DateTime? entryEndDatetime,
                    string deviceName, byte[] photo)
        {
            
            APIResultFraser apiResult = new APIResultFraser() ;
            await Task.Run(() =>
            {
                try
                {
                    var photo64 = "";
                    if (photo != null && photo.Length > 0)
                    {
                        photo64 = Convert.ToBase64String(photo);
                    }

                    object requestBody = new
                    {
                        VisitorTypeID = visitorTypeID,
                        //IdentityNumber = identityNumber,
                        IdentityNumberLast4 = identityNumberLast4,
                        VisitorName = visitorName,
                        VisitorContactNumber = visitorContactNumber,
                        LocationID = locationID,
                        CardNumber = cardNumber,
                        CompanyName = companyName,
                        UnitName = unitName,
                        UnitNumber = unitNumber,
                        PTWNumber = ptwNumber,
                        IncludeSaturday = includeSaturday,
                        IncludeSunday = includeSunday,
                        IncludeHoliday = includeHoliday,
                        EntryStartDatetime = entryStartDatetime,
                        EntryEndDatetime = entryEndDatetime,
                        DeviceName = deviceName,
                        Photo64 = photo64
                    };

                    IRestResponse restResponse = GenericRestAPICall("/api/SaveVisitRecordFraser", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                        apiResult = JsonConvert.DeserializeObject<APIResultFraser>(restResponse.Content);
                }
                catch (Exception ex)
                {
                    APIMessage apiMessage = ErrorMessageHelper.Instance.GetLocalServerError();
                    apiResult.ServerErrorCode = apiMessage.Code;
                    apiResult.ServerErrorMessage = apiMessage.Message;
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult;
        }

        public async Task<APIResultFraser> SaveVisitInfoAdhoc(int visitorTypeID, string identityNumberLast4, string visitorName,
                    string visitorContactNumber, string locationID, string cardNumber, string companyName,
                    string unitName, string unitNumber, string ptwNumber,
                    bool? includeSaturday, bool? includeSunday, bool? includeHoliday,
                    DateTime? entryStartDatetime, DateTime? entryEndDatetime,
                    string deviceName,byte[] photo)
        {
            APIResultFraser apiResult = new APIResultFraser();
            await Task.Run(() =>
            {
                try
                {
                    var photo64 = "";
                    if (photo != null && photo.Length > 0)
                    {
                        photo64 = Convert.ToBase64String(photo);
                    }

                    object requestBody = new
                    {
                        VisitorTypeID = visitorTypeID,
                        //IdentityNumber = identityNumber,
                        IdentityNumberLast4 = identityNumberLast4,
                        VisitorName = visitorName,
                        VisitorContactNumber = visitorContactNumber,
                        LocationID = locationID,
                        CardNumber = cardNumber,
                        CompanyName = companyName,
                        UnitName = unitName,
                        UnitNumber = unitNumber,
                        PTWNumber = ptwNumber,
                        IncludeSaturday = includeSaturday,
                        IncludeSunday = includeSunday,
                        IncludeHoliday = includeHoliday,
                        EntryStartDatetime = entryStartDatetime,
                        EntryEndDatetime = entryEndDatetime,
                        DeviceName = deviceName,
                        Photo64 = photo64
                    };

                    IRestResponse restResponse = GenericRestAPICall("/api/SaveVisitRecordFraserAdhoc", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                        apiResult = JsonConvert.DeserializeObject<APIResultFraser>(restResponse.Content);
                }
                catch (Exception ex)
                {
                    APIMessage apiMessage = ErrorMessageHelper.Instance.GetLocalServerError();
                    apiResult.ServerErrorCode = apiMessage.Code;
                    apiResult.ServerErrorMessage = apiMessage.Message;
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult;
        }
    }
}
