﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public static class KioskVisitorUtility
    {
        public static void CopyDataToKioskVisitor(KioskVisitor newKioskVisitor, KioskVisitor oldKioskVisitor, bool replaceIfHasValue = false)
        {
            if (replaceIfHasValue)
            {
                newKioskVisitor.IdentityNumber = oldKioskVisitor.IdentityNumber;
                newKioskVisitor.VisitorName = oldKioskVisitor.VisitorName;
                newKioskVisitor.VisitorContactNumber = oldKioskVisitor.VisitorContactNumber;
                newKioskVisitor.CompanyName = oldKioskVisitor.CompanyName;
                newKioskVisitor.Blacklisted = oldKioskVisitor.Blacklisted;
                newKioskVisitor.LocationID = oldKioskVisitor.LocationID;
                newKioskVisitor.LocationName = oldKioskVisitor.LocationName;
                newKioskVisitor.CardNumber = oldKioskVisitor.CardNumber;
                // VisitorTypeID not need to copy
                newKioskVisitor.UnitName = newKioskVisitor.UnitName;
                newKioskVisitor.UnitNumber =oldKioskVisitor.UnitNumber;
                newKioskVisitor.PTWNumber = oldKioskVisitor.PTWNumber;
                newKioskVisitor.IncludeSaturday = oldKioskVisitor.IncludeSaturday;
                newKioskVisitor.IncludeSunday = oldKioskVisitor.IncludeSunday;
                newKioskVisitor.IncludeHoliday = oldKioskVisitor.IncludeHoliday;
                newKioskVisitor.EntryStartDatetime = oldKioskVisitor.EntryStartDatetime;
                newKioskVisitor.EntryEndDatetime = oldKioskVisitor.EntryEndDatetime;
                newKioskVisitor.VistorVisitType = oldKioskVisitor.VistorVisitType;
            }
            else
            {
                newKioskVisitor.IdentityNumber = string.IsNullOrWhiteSpace(newKioskVisitor.IdentityNumber) ? oldKioskVisitor.IdentityNumber : newKioskVisitor.IdentityNumber;
                newKioskVisitor.VisitorName = string.IsNullOrWhiteSpace(newKioskVisitor.VisitorName) ? oldKioskVisitor.VisitorName : newKioskVisitor.VisitorName;
                newKioskVisitor.VisitorContactNumber = string.IsNullOrWhiteSpace(newKioskVisitor.VisitorContactNumber) ? oldKioskVisitor.VisitorContactNumber : newKioskVisitor.VisitorContactNumber;
                newKioskVisitor.CompanyName = string.IsNullOrWhiteSpace(newKioskVisitor.CompanyName) ? oldKioskVisitor.CompanyName : newKioskVisitor.CompanyName;
                newKioskVisitor.Blacklisted =  !newKioskVisitor.Blacklisted.HasValue ? oldKioskVisitor.Blacklisted : newKioskVisitor.Blacklisted;
                newKioskVisitor.LocationID = string.IsNullOrWhiteSpace(newKioskVisitor.LocationID) ? oldKioskVisitor.LocationID : newKioskVisitor.LocationID;
                newKioskVisitor.LocationName = string.IsNullOrWhiteSpace(newKioskVisitor.LocationName) ? oldKioskVisitor.LocationName : newKioskVisitor.LocationName;
                newKioskVisitor.CardNumber = string.IsNullOrWhiteSpace(newKioskVisitor.CardNumber) ? oldKioskVisitor.CardNumber : newKioskVisitor.CardNumber;
                newKioskVisitor.UnitName = string.IsNullOrWhiteSpace(newKioskVisitor.UnitName) ? oldKioskVisitor.UnitName : newKioskVisitor.UnitName;
                newKioskVisitor.UnitNumber = string.IsNullOrWhiteSpace(newKioskVisitor.UnitNumber) ? oldKioskVisitor.UnitNumber : newKioskVisitor.UnitNumber;
                newKioskVisitor.PTWNumber = string.IsNullOrWhiteSpace(newKioskVisitor.PTWNumber) ? oldKioskVisitor.PTWNumber : newKioskVisitor.PTWNumber;
                newKioskVisitor.IncludeSaturday = oldKioskVisitor.IncludeSaturday;
                newKioskVisitor.IncludeSunday = oldKioskVisitor.IncludeSunday;
                newKioskVisitor.IncludeHoliday = oldKioskVisitor.IncludeHoliday;
                newKioskVisitor.EntryStartDatetime = !newKioskVisitor.EntryStartDatetime.HasValue ? oldKioskVisitor.EntryStartDatetime : newKioskVisitor.EntryStartDatetime;
                newKioskVisitor.EntryEndDatetime = !newKioskVisitor.EntryEndDatetime.HasValue ? oldKioskVisitor.EntryEndDatetime : newKioskVisitor.EntryEndDatetime;

                newKioskVisitor.VistorVisitType = oldKioskVisitor.VistorVisitType;
            }

            //                "IdentityNumber": "333C",
            //"VisitorName": "Peter 3C",
            //"VisitorContactNumber": null,
            //"CompanyName": "333 Pte Ltd",
            //"Blacklisted": false,
            //"LocationID": 4,
            //"LocationName": "NPC-NW",
            //"CardNumber": null,
            //"VisitorTypeID": 0,
            //"UnitName": null,
            //"UnitNumber": null,
            //"PTWNumber": null,
            //"IncludeSaturday": true,
            //"IncludeSunday": true,
            //"IncludeHoliday": false,
            //"EntryStartDatetime": "2019-11-27T08:00:00",
            //"EntryEndDatetime": "2019-12-31T08:00:00"
        }

        public static void CopyDataToKioskVisitor(KioskVisitor kioskVisitor, APIResultVisitor apiResultVisitor, bool replaceIfHasValue = false)
        {
            if (apiResultVisitor != null)
            {
                if (replaceIfHasValue)
                {
                    kioskVisitor.IdentityNumber = apiResultVisitor.IdentityNumber;
                    kioskVisitor.VisitorName = apiResultVisitor.VisitorName;
                    kioskVisitor.VisitorContactNumber = apiResultVisitor.VisitorContactNumber;
                    kioskVisitor.CompanyName = apiResultVisitor.CompanyName;
                    kioskVisitor.Blacklisted = apiResultVisitor.Blacklisted;
                    kioskVisitor.LocationID = apiResultVisitor.LocationID.ToString();
                    kioskVisitor.LocationName = apiResultVisitor.LocationName;
                    kioskVisitor.CardNumber = apiResultVisitor.CardNumber;
                    // VisitorTypeID no need to copy
                    kioskVisitor.UnitName = apiResultVisitor.UnitName;
                    kioskVisitor.UnitNumber = apiResultVisitor.UnitNumber;
                    kioskVisitor.PTWNumber = apiResultVisitor.PTWNumber;
                    kioskVisitor.IncludeSaturday = apiResultVisitor.IncludeSaturday;
                    kioskVisitor.IncludeSunday = apiResultVisitor.IncludeSunday;
                    kioskVisitor.IncludeHoliday = apiResultVisitor.IncludeHoliday;
                    kioskVisitor.EntryStartDatetime = apiResultVisitor.EntryStartDatetime;
                    kioskVisitor.EntryEndDatetime = apiResultVisitor.EntryEndDatetime;
                }
                else
                {
                    kioskVisitor.IdentityNumber = string.IsNullOrWhiteSpace(kioskVisitor.IdentityNumber) ? apiResultVisitor.IdentityNumber : kioskVisitor.IdentityNumber;
                    kioskVisitor.VisitorName = string.IsNullOrWhiteSpace(kioskVisitor.VisitorName) ? apiResultVisitor.VisitorName : kioskVisitor.VisitorName;
                    kioskVisitor.VisitorContactNumber = string.IsNullOrWhiteSpace(kioskVisitor.VisitorContactNumber) ? apiResultVisitor.VisitorContactNumber : kioskVisitor.VisitorContactNumber;
                    kioskVisitor.CompanyName = string.IsNullOrWhiteSpace(kioskVisitor.CompanyName) ? apiResultVisitor.CompanyName : kioskVisitor.CompanyName;
                    kioskVisitor.Blacklisted = !kioskVisitor.Blacklisted.HasValue ? apiResultVisitor.Blacklisted : kioskVisitor.Blacklisted;
                    kioskVisitor.LocationID = string.IsNullOrWhiteSpace(kioskVisitor.LocationID) ? (apiResultVisitor.LocationID.HasValue ? apiResultVisitor.LocationID.Value.ToString() : "") : kioskVisitor.LocationID;
                    kioskVisitor.LocationName = string.IsNullOrWhiteSpace(kioskVisitor.LocationName) ? apiResultVisitor.LocationName : kioskVisitor.LocationName;
                    kioskVisitor.CardNumber = string.IsNullOrWhiteSpace(kioskVisitor.CardNumber) ? apiResultVisitor.CardNumber : kioskVisitor.CardNumber;
                    kioskVisitor.UnitName = string.IsNullOrWhiteSpace(kioskVisitor.UnitName) ? apiResultVisitor.UnitName : kioskVisitor.UnitName;
                    kioskVisitor.UnitNumber = string.IsNullOrWhiteSpace(kioskVisitor.UnitNumber) ? apiResultVisitor.UnitNumber : kioskVisitor.UnitNumber;
                    kioskVisitor.PTWNumber = string.IsNullOrWhiteSpace(kioskVisitor.PTWNumber) ? apiResultVisitor.PTWNumber : kioskVisitor.PTWNumber;
                    kioskVisitor.IncludeSaturday = apiResultVisitor.IncludeSaturday;
                    kioskVisitor.IncludeSunday = apiResultVisitor.IncludeSunday;
                    kioskVisitor.IncludeHoliday = apiResultVisitor.IncludeHoliday;
                    kioskVisitor.EntryStartDatetime = !kioskVisitor.EntryStartDatetime.HasValue ? apiResultVisitor.EntryStartDatetime : kioskVisitor.EntryStartDatetime;
                    kioskVisitor.EntryEndDatetime = !kioskVisitor.EntryEndDatetime.HasValue ? apiResultVisitor.EntryEndDatetime : kioskVisitor.EntryEndDatetime;

                }
            }
        }
    }
}
