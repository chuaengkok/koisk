﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using Kiosk.Model.Mom;
using Kiosk.Utility;
using Kiosk.Utility.API;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Kiosk.ContentPage
{
    public partial class ScanIdentityCardBarcode : UserControl
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;
        private List<Location> locationList;

        private System.Windows.Threading.DispatcherTimer identityScannerTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;

        private string scanningText = "Scanning..";
        private int countDownCounter;

        private char[] charToTrim = { '*', '\n', '\r' };

        public ScanIdentityCardBarcode(NavigationPage navigationPage, KioskVisitor kioskVisitor, List<Location> locationList)
        {
            InitializeComponent();

            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
            this.locationList = locationList;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblMessage.Text = navigationPage.GetMessages(0);

            identityScannerTimer = new System.Windows.Threading.DispatcherTimer();
            identityScannerTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            kioskVisitor.ResetStringFromInput();
            kioskVisitor.IdentityNumber = string.Empty;

            StartScanning();
            
            NavigationHelper.Instance.HideLoadingPanel();
        }

        private void StartScanning()
        {
            identityScannerTimer.Tick += new EventHandler(identityScannerTimer_Tick);
            closeScreenTimer.Tick += new EventHandler(closeScreenTimer_Tick);

            countDownCounter = ConfigurationStore.Instance.CountdownTimer;
            lblCountdown.Content = scanningText;

            string errorMessage = "";
            lblMessage2.Text = "";
            

            if (string.IsNullOrWhiteSpace(errorMessage))
                identityScannerTimer.Start();
            else
                StartCloseScreenCountdown(errorMessage);
            //IdentityNumber = await PassportScanner.Instance.StartScanningFake(); // use fake, no passport scanner now 
        }

        private void StartCloseScreenCountdown(string message)
        {
            StopIdentityScannerTimer();
            countDownCounter = ConfigurationStore.Instance.CountdownTimer;
            lblCountdown.Content = countDownCounter;

            ShowMessage(message);
            closeScreenTimer.Start();
        }

        private void identityScannerTimer_Tick(object sender, EventArgs args)
        {
            try
            {
                int secondPassed = ConfigurationStore.Instance.CountdownTimer - countDownCounter + 1;
                countDownCounter--;
                lblCountdown.Content = scanningText + new string('.', secondPassed >= 0 ? secondPassed : 0);

                if (kioskVisitor.StringFromInputEnd)
                {

                    kioskVisitor.IdentityNumber = kioskVisitor.StringFromInput.Trim(charToTrim);
                    if (!string.IsNullOrWhiteSpace(kioskVisitor.IdentityNumber))
                    {
                        StopIdentityScannerTimer();

                        //if (System.IO.File.Exists(ConfigurationStore.Instance.KioskFilePath + ConfigurationStore.Instance.TempVisitorIdentityFileName))
                        //    lblImageIdentity.Source = new BitmapImage(new Uri(ConfigurationStore.Instance.KioskFilePath + ConfigurationStore.Instance.TempVisitorIdentityFileName));

                        ShowMessage($"ID No. : {kioskVisitor.IdentityNumberMasked}");

                        //kioskVisitor.Name = PassportScanner.Instance.Name.Replace(";", "");

                        if (!string.IsNullOrWhiteSpace(kioskVisitor.VisitorName))
                            lblMessage2.Text = $"Name : {kioskVisitor.VisitorName}";

                        countDownCounter = ConfigurationStore.Instance.CountdownTimer;
                        lblCountdown.Content = countDownCounter.ToString();

                        CancelStackPanel.Visibility = Visibility.Collapsed;
                        //ManualRescanStackPanel.Visibility = Visibility.;
                        OKStackPanel.Visibility = Visibility.Visible;
                        //RescanStackPanel.Visibility = Visibility.Collapsed;

                        closeScreenTimer.Start();

                    }
                    else
                        NavigationHelper.Instance.NavigateToError(NavigationHelper.Instance.GetGenericMessage("EmptyBarcodeError"));

                }
                else
                {
                    if (countDownCounter <= 0)
                        StartCloseScreenCountdown(navigationPage.GetMessages(1));
                }
            }
            catch (Exception ex)
            {
                log.Error("identityScannerTimer_Tick ", ex);
            }

            // Forcing the CommandManager to raise the RequerySuggested event
            CommandManager.InvalidateRequerySuggested();
        }
        
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();
            
            if (countDownCounter <= 0)
            {
                StopCloseScreenTimer();
                if (!string.IsNullOrWhiteSpace(kioskVisitor.IdentityNumber))
                    NavigateToNextScreen();
                else
                    NavigateToMainScreen();
            }
        }
        
        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private async void NavigateToNextScreen()
        {
            NavigationHelper.Instance.ShowLoadingPanel("Verifying...");

            FraserAPIHelper fraserAPIHelper = new FraserAPIHelper(ConfigurationStore.Instance.GMSApiURL, ConfigurationStore.Instance.APICallTimeout);
            APIResultFraser apiResult = await fraserAPIHelper.CheckValidSignIn((int)this.kioskVisitor.VistorVisitType, kioskVisitor.IdentityNumber, kioskVisitor.IdentityNumberLast4, ConfigurationStore.Instance.LocationID);
            if (apiResult.IsSuccess)
            {

                NavigationHelper.Instance.NavigateToNext();
            }
            else
                NavigationHelper.Instance.NavigateToError(apiResult.ServerErrorMessage);
        }
        
        private void ShowMessage(string message)
        {
            this.Dispatcher.Invoke(() =>
            {
                lblMessage.Text = message;

                grdCancel.Visibility = Visibility.Collapsed;
                grdRescan.Visibility = Visibility.Visible;
            });
        }

        private void StopIdentityScannerTimer()
        {
            PassportScanner.Instance.StopTimer();
            identityScannerTimer.Stop();
            identityScannerTimer.Tick -= identityScannerTimer_Tick;
        }
        private void StopCloseScreenTimer()
        {
            closeScreenTimer.Stop();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopIdentityScannerTimer();
            StopCloseScreenTimer();
        }

        #region Buttons
        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigateToMainScreen();
        }

        private void ManualEntryStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToPage(NavigationHelper.Navigate.EnterIdentityManual);
        }

        private void OKStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigateToNextScreen();
        }

        private void RescanStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.ShowLoadingPanel();

            StopCloseScreenTimer();

            grdCancel.Visibility = Visibility.Visible;
            grdRescan.Visibility = Visibility.Collapsed;

            CancelStackPanel.Visibility = Visibility.Visible;
            //ManualRescanStackPanel.Visibility = Visibility.Visible;
            OKStackPanel.Visibility = Visibility.Collapsed;
           // RescanStackPanel.Visibility = Visibility.Visible;

            lblMessage.Text = navigationPage.GetMessages(0);

            lblImageIdentity.Visibility = Visibility.Collapsed;

            kioskVisitor.ResetStringFromInput();
            kioskVisitor.IdentityNumber = string.Empty;

            StartScanning();
            
            NavigationHelper.Instance.HideLoadingPanel();
        }
        #endregion
    }
}
