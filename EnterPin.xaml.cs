﻿using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class EnterPin : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;

        private System.Windows.Threading.DispatcherTimer initialTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;

        private int CloseScreenTimerCounter = 5;
        public EnterPin(Utility.Log.Ilog ilog, NavigationPage navigationPage)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblMessage.Text = navigationPage.GetMessages(0);
            
            initialTimer = new System.Windows.Threading.DispatcherTimer();
            initialTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            CloseScreenTimerCounter = ConfigurationStore.Instance.CloseScreenTimer;

            txtPassword.Password = "";

            StartInitialTimer();

            NavigationHelper.Instance.HideLoadingPanel();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopInitialTimer();
            StopCloseScreenTimer();
        }

        private int initialTimerCounter = 1;
        private void initialTimer_Tick(object sender, EventArgs args)
        {
            if (initialTimerCounter > CloseScreenTimerCounter)
            {
                StopInitialTimer();
                StartCloseScreenTimer();
            }
            initialTimerCounter++;
            CommandManager.InvalidateRequerySuggested();
        }

        private int countDownCounter = 0;
        private object _lock = new object();
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();
            
            if (countDownCounter < 0)
            {
                //ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigationHelper.Instance.NavigateToMessageBeforeMain();
            }
        }

        private void StartInitialTimer()
        {
            initialTimerCounter = 1;
            lblMessage.Text = navigationPage.GetMessages(0);
            lblCountdown.Content = "";
            initialTimer.Tick -= initialTimer_Tick;
            initialTimer.Tick += initialTimer_Tick;
            initialTimer.Start();
        }
        private void StartCloseScreenTimer()
        {
            countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
            lblMessage.Text = navigationPage.GetMessages(2);
            lblCountdown.Content = countDownCounter.ToString();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
            closeScreenTimer.Tick += closeScreenTimer_Tick;
            closeScreenTimer.Start();
        }
        private void StopInitialTimer()
        {
            lock (_lock)
            {
                initialTimer.Stop();
                initialTimer.Tick -= initialTimer_Tick;
            }
        }
        private void StopCloseScreenTimer()
        {
            lock (_lock)
            {
                closeScreenTimer.Stop();
                closeScreenTimer.Tick -= closeScreenTimer_Tick;
            }
        }

        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void RefreshNoActivityTimer()
        {
            lock (_lock)
            {
                if (initialTimer.IsEnabled)
                    initialTimerCounter = 1;
                else
                {
                    if (closeScreenTimer.IsEnabled)
                    {
                        StopCloseScreenTimer();
                        if (!initialTimer.IsEnabled)
                            StartInitialTimer();
                    }
                }
            }
        }

        #region Buttons
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();
            lblMessage.Text = "";

            Button button = (Button)sender;
            string value = button.CommandParameter.ToString();
            switch (value.ToUpper())
            {
                case "BACK":
                    if (!string.IsNullOrWhiteSpace(txtPassword.Password))
                        txtPassword.Password = txtPassword.Password.Remove(txtPassword.Password.Length - 1);
                    break;
                case "C":
                    txtPassword.Password = "";
                    lblMessage.Text = "";
                    break;
                default:
                    if (txtPassword.Password.Length < 6)
                        txtPassword.Password = txtPassword.Password + value;
                    break;
            }
        }
        
        private void BackStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        int pinCounter = 0;
        private void NextStackPanel_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();
            if (txtPassword.Password == ConfigurationStore.Instance.Pincode)
            {
                NavigationHelper.Instance.NavigateToNext();
            }
            else
            {
                pinCounter++;

                txtPassword.Password = "";
                if (pinCounter >= 3)
                    NavigationHelper.Instance.NavigateToError(navigationPage.GetMessages(3));
                else
                    lblMessage.Text = navigationPage.GetMessages(1);
            }

        }
        #endregion
    }
}
