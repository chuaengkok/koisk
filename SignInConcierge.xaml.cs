﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using Kiosk.Utility;
using Kiosk.Utility.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class SignInConcierge : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;
        private List<Location> locationList;

        private KioskVisitor.VisitType visitType;

        public SignInConcierge(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor, List<Location> locationList)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
            this.locationList = locationList;

            visitType = kioskVisitor.VistorVisitType;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            cbbLevel.ItemsSource = this.locationList;
            ResetPage();
            cbbLevel.SelectedIndex = -1;

            NavigationHelper.Instance.HideLoadingPanel();
        }
        private void txtIDNumberSearch_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                SearchVisitor();
            }
        }
        private async void txtCardNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                await SignIn();
            }
        }

        private async void SearchVisitor()
        {
            //trim for extra characters at end for FIN
            if (txtIDNumberSearch.Text.Trim().Length > 9)
                kioskVisitor.IdentityNumber = txtIDNumberSearch.Text.Trim().Substring(0, 9);
            else
                kioskVisitor.IdentityNumber = txtIDNumberSearch.Text.Trim();

            NavigationHelper.Instance.ShowLoadingPanel();

            if (!string.IsNullOrWhiteSpace(kioskVisitor.IdentityNumber))
            {

                JTCAPIHelper jtcAPIHelper = new JTCAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                APIResultFraser apiResult = await jtcAPIHelper.CheckValidSignIn((int)this.kioskVisitor.VistorVisitType, kioskVisitor.IdentityNumber, kioskVisitor.IdentityNumberLast4, ConfigurationStore.Instance.LocationID);

                KioskVisitorUtility.CopyDataToKioskVisitor(kioskVisitor, apiResult.Data);
                ShowResult();


                if (kioskVisitor.Blacklisted.HasValue && kioskVisitor.Blacklisted.Value)
                {
                    btnSignIn.Visibility = Visibility.Collapsed;
                    lblMessage.Text = navigationPage.GetMessages(3);
                }
                else if (apiResult.IsSuccess)
                {
                    if (string.IsNullOrWhiteSpace(kioskVisitor.VisitorName) && string.IsNullOrWhiteSpace(kioskVisitor.VisitorContactNumber))
                        lblMessage.Text = navigationPage.GetMessages(2);
                    else
                        lblMessage.Text = navigationPage.GetMessages(1);
                }
                else
                {
                    ErrorMessageHelper.ServerErrorCode errorCode;
                    if (Enum.TryParse<ErrorMessageHelper.ServerErrorCode>(apiResult.ServerErrorCode, out errorCode))
                    {
                        string localErrorMessage = NavigationHelper.Instance.GetGenericMessage("MOMValidationError");
                        if (!string.IsNullOrWhiteSpace(localErrorMessage))
                            NavigationHelper.Instance.NavigateToError(localErrorMessage.Replace("[[errormessage]]", apiResult.ServerErrorMessage));
                        else
                            NavigationHelper.Instance.NavigateToError(apiResult.ServerErrorMessage);
                    }
                    else
                        NavigationHelper.Instance.NavigateToError(apiResult.ServerErrorMessage);
                }

            }
            else
                lblMessage.Text = navigationPage.GetMessages(0);


            NavigationHelper.Instance.HideLoadingPanel();
        }
        private void ResetPage()
        {
            lblMessage.Text = navigationPage.GetMessages(0);

            vbxFields.Visibility = Visibility.Hidden;
            vbxSearch.Visibility = Visibility.Visible;

            btnSignIn.Visibility = Visibility.Collapsed;

            txtIDNumberSearch.Text = "";
            txtContactNumber.Text = "";
            txtIDNumber.Text = "";
            txtName.Text = "";
            txtCardNumber.Text = "";
            ResetContactNumber(true);
            rbnLocal.IsChecked = true;

            kioskVisitor = new KioskVisitor();
            kioskVisitor.VistorVisitType = this.visitType;

            cbbLevel.Text = "";
            cbbLevel.SelectedIndex = -1;

            txtIDNumberSearch.Focus();
        }

        private void ShowResult()
        {

            vbxFields.Visibility = Visibility.Visible;
            vbxSearch.Visibility = Visibility.Hidden;

            txtIDNumber.Text = kioskVisitor.IdentityNumberMasked;
            txtName.Text = kioskVisitor.VisitorName;

            if (!string.IsNullOrWhiteSpace(kioskVisitor.VisitorContactNumber))
            {
                if (!kioskVisitor.VisitorContactNumber.All(char.IsDigit))
                    kioskVisitor.VisitorContactNumber = "";

                if (kioskVisitor.VisitorContactNumber.Length != 8 || !(kioskVisitor.VisitorContactNumber.StartsWith("6") || kioskVisitor.VisitorContactNumber.StartsWith("8") || kioskVisitor.VisitorContactNumber.StartsWith("9")))
                    rbnForeign.IsChecked = true;
            }

            txtContactNumber.Text = kioskVisitor.VisitorContactNumber;

            btnSignIn.Visibility = Visibility.Visible;

            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                txtName.Focus();
                txtName.SelectAll();
            }
            else if (string.IsNullOrWhiteSpace(txtContactNumber.Text))
            {
                txtContactNumber.Focus();
                txtContactNumber.SelectAll();
            }
            else
                cbbLevel.Focus();
        }

        private void ResetContactNumber(bool isLocal)
        {
            txtContactNumber.Text = "";
            if (isLocal)
            {
                txtContactNumber.MaxLength = 8;
                lblPlusSign.Visibility = Visibility.Collapsed;
            }
            else
            {
                txtContactNumber.MaxLength = 20;
                lblPlusSign.Visibility = Visibility.Visible;
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (e.Source == rbnLocal)
            {
                ResetContactNumber(true);
                txtContactNumber.Focus();
            }
            else
            {
                ResetContactNumber(false);
                txtContactNumber.Focus();
            }
        }   

        private void txtContactNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }


        private async System.Threading.Tasks.Task<bool> SignIn()
        {
            NavigationHelper.Instance.ShowLoadingPanel();

            bool success = false;

            // do sign In;
            if (!string.IsNullOrWhiteSpace(txtName.Text) && !string.IsNullOrWhiteSpace(txtContactNumber.Text))
            {
                string contactNumberError = ValidateContactNumber();
                if (string.IsNullOrWhiteSpace(contactNumberError))
                {
                    if (cbbLevel.SelectedItem != null && cbbLevel.SelectedIndex > -1)
                    {
                        kioskVisitor.LocationID = ((Location)cbbLevel.SelectedItem).ID;
                        kioskVisitor.VisitorName = txtName.Text.Trim();
                        kioskVisitor.VisitorContactNumber = txtContactNumber.Text.Trim();
                        kioskVisitor.CardNumber = txtCardNumber.Text.Trim();


                        FraserAPIHelper fraserAPIHelper = new FraserAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                        APIResultFraser apiResult = await fraserAPIHelper.SaveVisitInfo((int)kioskVisitor.VistorVisitType, kioskVisitor.IdentityNumber, kioskVisitor.VisitorName,
                                                            kioskVisitor.VisitorContactNumber, kioskVisitor.LocationID, kioskVisitor.CardNumber, kioskVisitor.CompanyName,
                                                            kioskVisitor.UnitName, kioskVisitor.UnitNumberFinal, kioskVisitor.PTWNumber,
                                                            kioskVisitor.IncludeSaturday, kioskVisitor.IncludeSunday, kioskVisitor.IncludeHoliday,
                                                            kioskVisitor.EntryStartDatetime.Value, kioskVisitor.EntryEndDatetime.Value, ConfigurationStore.Instance.KioskID,
                                                            kioskVisitor.PhotoBytes);

                        if (apiResult.IsSuccess)
                        {
                            // clear all data;
                            ResetPage();
                            lblMessage.Text = navigationPage.GetMessages(5);
                            success = true;
                        }
                        else
                        {
                            NavigationHelper.Instance.NavigateToError(apiResult.ServerErrorMessage);
                        }


                    }
                    else
                    {
                        lblMessage.Text = navigationPage.GetMessages(4);
                        cbbLevel.Focus();
                    }
                }
                else
                {
                    lblMessage.Text = contactNumberError;
                    txtContactNumber.Focus();
                }
            }
            else
            {
                lblMessage.Text = navigationPage.GetMessages(6);
                if (string.IsNullOrWhiteSpace(txtName.Text))
                    txtName.Focus();
                else if (string.IsNullOrWhiteSpace(txtContactNumber.Text))
                    txtContactNumber.Focus();
            }

            NavigationHelper.Instance.HideLoadingPanel();
            return success;
        }





        private void BackToMain()
        {
            NavigationHelper.Instance.NavigateToMain();
        }


        #region Buttons
        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            BackToMain();
        }
        private async void SignInStackPanel_Click(object sender, RoutedEventArgs e)
        {
            await SignIn();
        }

        private string ValidateContactNumber()
        {
            if (!string.IsNullOrWhiteSpace(txtContactNumber.Text))
            {
                 if (!txtContactNumber.Text.All(char.IsDigit))
                    return navigationPage.GetMessages(7);

                 if (rbnLocal.IsChecked.Value && (txtContactNumber.Text.Length != 8 || !(txtContactNumber.Text.StartsWith("6") || txtContactNumber.Text.StartsWith("8") || txtContactNumber.Text.StartsWith("9"))))
                    return navigationPage.GetMessages(8);
            }
            return "";
        }


        private void SearchStackPanel_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtIDNumberSearch.Text) && txtIDNumberSearch.Text.Length >= 4)
            {
                SearchVisitor();
            }
            else
                lblMessage.Text = navigationPage.GetMessages(0);
        }
        private void ResetStackPanel_Click(object sender, RoutedEventArgs e)
        {
            ResetPage();
        }


        #endregion

    }
}
