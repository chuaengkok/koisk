﻿using Kiosk.Model.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public class ErrorMessageHelper
    {
        private static readonly Lazy<ErrorMessageHelper> lazy = new Lazy<ErrorMessageHelper>(() => new ErrorMessageHelper());
        public static ErrorMessageHelper Instance { get { return lazy.Value; } }

        private List<APIMessage> ErrorMessages;

        public enum ServerErrorCode
        {            
            MOMLive,
            MOMNotFound,
            MOMExpired,
            MOMError
        }


        public enum LocalErrorCode
        {
            //Generic errors
            LocalServerError
        }

        private ErrorMessageHelper()
        {
            ErrorMessages = new List<APIMessage>();

            // visitor related 
            ErrorMessages.Add(new APIMessage() { Code = LocalErrorCode.LocalServerError.ToString(), Message = "Unable to communicate with server, please contact security officer for assistance" });
        }

        public APIMessage GetLocalServerError()
        {
            return ErrorMessages.FirstOrDefault(i => i.Code.Equals(LocalErrorCode.LocalServerError.ToString(), StringComparison.OrdinalIgnoreCase));
        }
    }
}
