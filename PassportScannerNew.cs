﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace Kiosk.Utility
{
    public class PassportScannerNew
    {
        private static readonly Lazy<PassportScannerNew> lazy = new Lazy<PassportScannerNew>(() => new PassportScannerNew());
        public static PassportScannerNew Instance { get { return lazy.Value; } }


        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //indicates the idcard.dll is loaded or not
        bool m_bIsIDCardLoaded = false;
        int ncount = 0;
        private static class MyDll
        {
            [DllImport("kernel32")]
            public static extern int LoadLibrary(string strDllName);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int InitIDCard(char[] cArrUserID, int nType, char[] cArrDirectory);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetRecogResult(int nIndex, char[] cArrBuffer, ref int nBufferLen);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogIDCard();

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetFieldName(int nIndex, char[] cArrBuffer, ref int nBufferLen);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int AcquireImage(int nCardType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SaveImage(char[] cArrFileName);
            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SaveHeadImage(char[] cArrFileName);


            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetCurrentDevice(char[] cArrDeviceName, int nLength);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern void GetVersionInfo(char[] cArrVersion, int nLength);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern bool CheckDeviceOnline();

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern bool SetAcquireImageType(int nLightType, int nImageType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern bool SetUserDefinedImageSize(int nWidth, int nHeight);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern bool SetAcquireImageResolution(int nResolutionX, int nResolutionY);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SetIDCardID(int nMainID, int[] nSubID, int nSubIdCount);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int AddIDCardID(int nMainID, int[] nSubID, int nSubIdCount);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogIDCardEX(int nMainID, int nSubID);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetButtonDownType();

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetGrabSignalType();

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SetSpecialAttribute(int nType, int nSet);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern void FreeIDCard();
            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetDeviceSN(char[] cArrSn, int nLength);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetBusinessCardResult(int nID, int nIndex, char[] cArrBuffer, ref int nBufferLen);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogBusinessCard(int nType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetBusinessCardFieldName(int nID, char[] cArrBuffer, ref int nBufferLen);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetBusinessCardResultCount(int nID);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int LoadImageToMemory(string path, int nType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int ClassifyIDCard(ref int nCardType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogChipCard(int nDGGroup, bool bRecogVIZ, int nSaveImageType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogGeneralMRZCard(bool bRecogVIZ, int nSaveImageType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogCommonCard(int nSaveImageType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SaveImageEx(char[] lpFileName, int nType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetDataGroupContent(int nDGIndex, bool bRawData, byte[] lpBuffer, ref int len);
        }

        public bool IsIDCardLoaded
        {
            get { return m_bIsIDCardLoaded; }
        }

        // passport scanner related
        public bool LoadScanner()
        {
            log.Info("Start Load Scanner");

            //check if the kernel is loaded or not
            if (m_bIsIDCardLoaded)
            {
                //textBoxDisplayResult.Text = "The recognition engine is loaded successfully.";
                return true;
            }

            int nRet;
            nRet = MyDll.LoadLibrary("IDCard");
            if (nRet == 0)
            {
                throw new Exception("Failed to load IDCard.dll");
            }

            //Load engine

            char[] arr = ConfigurationStore.Instance.PassportScannerUserID.ToCharArray();

            nRet = MyDll.InitIDCard(arr, 1, null);
            if (nRet != 0)
            {
                throw new Exception("Failed to initialize the recognition engine.\r\n" + nRet.ToString());
            }
            MyDll.SetSpecialAttribute(1, 1);
            m_bIsIDCardLoaded = true;

            return true;
        }

        public void StopScanner()
        {
            log.Info("Stop Load Scanner");
            //timer.Enabled = false;
            dispatcherTimer.Tick -= TimerEventProcessor;
            dispatcherTimer.Stop();

            if (m_bIsIDCardLoaded)
            {
                MyDll.FreeIDCard();
                m_bIsIDCardLoaded = false;
            }
        }

        /*
        passport scanner information card type
        2005 - Singapore NRIC 2005 - 新加坡身份证2005版
        2004 - Singapore NRIC - 新加坡身份证
                IDENTITY CARD NO.
        2031 - Singapore Driving License - 新加坡驾驶证
                Driver's license number
        13 - Passport
        */

        private static readonly int[] CardTypeIDList = { 2004, 13, 2031, 2005, 2023, 2025 };
        private static readonly string[] WantedFieldNameList = { "IDENTITY CARD NO.", "FIN", "S Pass No.", "Work Permit No.", "Driver's license number", "Passport number" };

        public string Identity { get; set; }
        private System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        private static readonly object _locker = new object();

        public void StartScanning()
        {
            log.Info("StartScanning");


            if (!m_bIsIDCardLoaded)
                throw new Exception("Recognition engine not loaded");

            Identity = "";

            dispatcherTimer.Tick += new EventHandler(TimerEventProcessor);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 1);
            dispatcherTimer.Start();

        }

        private void TimerEventProcessor(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Identity))
            {
                if (!Monitor.TryEnter(_locker)) { return; }  // Don't let  multiple threads in here at the same time.

                try
                {
                    //for (int i = 0; i < CardTypeIDList.Length; i++)
                    //{
                        //Identity = ScanCardbyCardtype(CardTypeIDList[i], WantedFieldNameList[i]);
                        Identity = AutoClassAndRecognize();
                        if (!string.IsNullOrWhiteSpace(Identity))
                            return;
                    //}
                }
                finally
                {
                    Monitor.Exit(_locker);
                }
            }
        }

        private string GetStringWithoutNull(char[] charList)
        {
            if (charList != null && charList.Length > 0)
            {
                string temp = new string(charList);
                string[] tempResultList = temp.Split(new[] { '\0' }, 2);
                if (tempResultList.Length > 0)
                    return tempResultList[0];
            }
            return "";
        }


        public bool CheckScannerOnline()
        {
            if (!m_bIsIDCardLoaded)
                throw new Exception("Recognition engine not loaded");

            bool bRet = MyDll.CheckDeviceOnline();
            if (bRet)
                return true;
            else
                return false;
        }




        private string AutoClassAndRecognize()
        {
            string identityValue = "";
            int nRet = MyDll.GetGrabSignalType();
            if (nRet == 1)
            {
                int[] nSubID = new int[1];
                nSubID[0] = 0;

                for (int i = 0; i < CardTypeIDList.Length; i++)
                {
                    if (i == 0)
                        MyDll.SetIDCardID(CardTypeIDList[i], nSubID, 1);
                    else
                        MyDll.AddIDCardID(CardTypeIDList[i], nSubID, 1);
                }


                int nCardType = 0;
                nRet = MyDll.ClassifyIDCard(ref nCardType);
                if (nRet <= 0)
                {
                    return "";
                }



                int nDG = 15;
                int nSaveImage = 3;
                bool bVIZ = true;

                if (nCardType == 1)
                {
                    nRet = MyDll.RecogChipCard(nDG, bVIZ, nSaveImage);
                }

                if (nCardType == 2)
                {
                    nRet = MyDll.RecogGeneralMRZCard(bVIZ, nSaveImage);
                }

                if (nCardType == 3)
                {
                    nRet = MyDll.RecogCommonCard(nSaveImage);
                }

                if (nRet < 0)
                {
                    return "";
                }

                int MAX_CH_NUM = 128;
                char[] cArrFieldValue = new char[MAX_CH_NUM];
                char[] cArrFieldName = new char[MAX_CH_NUM];

                for (int i = 1; ; i++)
                {

                    nRet = MyDll.GetRecogResult(i, cArrFieldValue, ref MAX_CH_NUM);
                    if (nRet == 3)
                    {
                        break;
                    }
                    MyDll.GetFieldName(i, cArrFieldName, ref MAX_CH_NUM);

                    //textBoxDisplayResult.Text += new String(cArrFieldName);
                    //textBoxDisplayResult.Text += ":";
                    //textBoxDisplayResult.Text += new String(cArrFieldValue);
                    //textBoxDisplayResult.Text += "\r\n";


                    string tempFieldName = GetStringWithoutNull(cArrFieldName);

                    int pos = Array.IndexOf(WantedFieldNameList, tempFieldName);
                    if (pos > -1)
                    {
                        identityValue = GetStringWithoutNull(cArrFieldValue);
                        break;
                    }
                }


                if (!string.IsNullOrWhiteSpace(identityValue))
                {
                    //SaveImage
                    char[] carrImgPath = (ConfigurationStore.Instance.KioskFilePath +
                                    ConfigurationStore.Instance.TempVisitorIdentityFileName).ToCharArray();
                    nRet = MyDll.SaveImageEx(carrImgPath, nSaveImage);
                    if (nRet != 0)
                    {
                    }
                }
            }
            return identityValue;
        }

    }
}
