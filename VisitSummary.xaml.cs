﻿using Kiosk.Model;
using Kiosk.Model.Siemens;
using Kiosk.Utility;
using Kiosk.Utility.API;
using Kiosk.Utility.CardDispenserOld;
using Kiosk.Utility.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class VisitSummary : UserControl
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        private System.Windows.Threading.DispatcherTimer initialTimer;
        private System.Windows.Threading.DispatcherTimer collectCardTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;
        private System.Windows.Threading.DispatcherTimer cardDispenserStatusTimer;

        private SerialCardDispenserStatus cardDispenserStatus;

        private bool CardDispenserWorking;
        private bool CardRetrievedBackByKiosk;
        private bool CardReadyForRetrieval;

        private List<AccessGroup> siemensAccessGroups;

        private AccessGroup selectedAccessGroup;
        private string cardNumberFinal;
        //private List<WorkGroup> siemensWorkGroups;

        public VisitSummary(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor, 
                        List<AccessGroup> siemensAccessGroups) //, List<WorkGroup> siemensWorkGroups)
        {
            InitializeComponent();
            
            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
            this.siemensAccessGroups = siemensAccessGroups;
            //this.siemensWorkGroups = siemensWorkGroups;
        }
        
        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            CardDispenserWorking = true;
            CardRetrievedBackByKiosk = false;

            lblTitle1.Text = $"Hello {kioskVisitor.VisitorName}";
            lblTitle2.Text = navigationPage.GetTitle(0);
            lblMessage.Text = navigationPage.GetMessages(0);


            // test Siemens integration

            ////kioskVisitor = new KioskVisitor();
            ////kioskVisitor.Name = $"Test111 {DateTime.Now.ToString("2019MMdd-HHmmss")}";
            ////kioskVisitor.LocationName = "A Visitor 15";
            //kioskVisitor.CardNumber = "22112211";

            //Model.Siemens.AccessGroup accessGroup = siemensAccessGroups.Find(i => i.Name.Equals(kioskVisitor.LocationName, StringComparison.InvariantCultureIgnoreCase));
            //if (accessGroup != null)
            //{
            //    //Model.Siemens.WorkGroup workGroup = siemensWorkGroups.Find(i => i.Name.Equals(kioskVisitor.LocationName, StringComparison.InvariantCultureIgnoreCase));
            //    //if (workGroup != null)
            //    //{

            //    SiemensAPIHelper siemensAPIHelper = new SiemensAPIHelper(ConfigurationStore.Instance.KioskID, ConfigurationStore.Instance.SiemenAPIUrl);
            //    string token = await siemensAPIHelper.GetToken(ConfigurationStore.Instance.SiemenAPILogin, ConfigurationStore.Instance.SiemenAPIPassword);
            //    if (!string.IsNullOrWhiteSpace(token))
            //    {

            //        Model.Siemens.CardHolder cardHolder = await siemensAPIHelper.GetCardHolderByCardNumber(token, kioskVisitor.CardNumber);
            //        if (cardHolder != null)
            //            await siemensAPIHelper.DeleteCardHolderbyID(token, cardHolder.Token);

            //        await siemensAPIHelper.CreateCardHolder(token, kioskVisitor.Name, kioskVisitor.CardNumber,
            //                            accessGroup.Token, accessGroup.Name);//, workGroup.Token.ToString(), workGroup.Name);

            //        await siemensAPIHelper.Logout(token);


            //    }
            //    else
            //    {
            //        //error = "Unable to dispense card (error=SiPass empty token)";
            //        //ilog.LogError(error);
            //        //NavigationHelper.Instance.NavigateToError(error);
            //    }
            //    //}
            //    //else
            //    //{
            //    //    error = "Unable to dispense card for the location selected (error=Invalid SiPass WorkGroup)";
            //    //    ilog.LogError(error);
            //    //    NavigationHelper.Instance.NavigateToError(error);
            //    //}
            //}
            //else
            //{
            //    //error = "Unable to dispense card for the location selected (error=Invalid SiPass AccessGroup)";
            //    //ilog.LogError(error);
            //    //NavigationHelper.Instance.NavigateToError(error);
            //}

            //end test siemens integration


            

            initialTimer = new System.Windows.Threading.DispatcherTimer();
            initialTimer.Tick += new EventHandler(initialTimer_Tick);
            initialTimer.Interval = new TimeSpan(0, 0, 1);

            collectCardTimer = new System.Windows.Threading.DispatcherTimer();
            collectCardTimer.Tick += new EventHandler(collectCardTimer_Tick);
            collectCardTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Tick += new EventHandler(closeScreenTimer_Tick);
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            cardDispenserStatusTimer = new System.Windows.Threading.DispatcherTimer();
            cardDispenserStatusTimer.Tick += new EventHandler(cardDispenserStatusTimer_Tick);
            cardDispenserStatusTimer.Interval = new TimeSpan(0, 0, 1);


            CardReadyForRetrieval = false;
            cardDispenserStatus = SerialCardDispenserStatus.NoStatus;

            // reset before calling card dispenser
            kioskVisitor.ResetStringFromInput();
            kioskVisitor.CardNumber = string.Empty;

            cardDispenserStatusTimer.Start();

            try
            {

                //ilog.LogInfo("after UserControl_Loaded BrotherPrinter.Instance.PrintStickerAsync");

                SerialCardDispenser serialCardDispenser = SerialCardDispenserManager.Instance.GetSerialCardDispenserByLevel(kioskVisitor.LocationID);
                if (serialCardDispenser.IsConnected())
                {
                    // check if already dispense 2 cards
                    if (!string.IsNullOrWhiteSpace(kioskVisitor.CompanyName) && ConfigurationStore.Instance.KioskLocation != "OGS")
                    {
                        //int count = CompanyCardSQLiteHelper.Instance.GetCountByCompanyNameToday(kioskVisitor.CompanyName);

                        CapitalandAPIHelper capitalandAPIHelper = new CapitalandAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                        int count = await capitalandAPIHelper.GetNumberofCardSignOutByCompany(kioskVisitor.CompanyName);
                        if (count < 2)
                            CardDispenserWorking = await DispenseCard();
                        else
                        {
                            string tempMessage = navigationPage.GetMessages(3);
                            tempMessage = tempMessage.Replace("[CompanyName]", kioskVisitor.CompanyName);
                            lblMessage.Text = tempMessage;
                        }
                    }
                    else
                        CardDispenserWorking = await DispenseCard();
                }
                else
                    CardDispenserWorking = false;
            }
            catch (Exception ex)
            {
                ilog.LogError("Unable to connect to card dispenser ", ex);
                lblMessage.Text = ex.Message;
                CardDispenserWorking = false;
            }

            if (CardDispenserWorking)
            {

                // ogs no sticker

                if (ConfigurationStore.Instance.KioskLocation != "OGS")
                { 
                    bool saveImageSuccess = await FileUtility.SaveImage(ilog, ConfigurationStore.Instance.KioskFilePath, ConfigurationStore.Instance.TempVisitorPhotoFileName, kioskVisitor.Photo);


                    string stickerTitle = kioskVisitor.VistorVisitType.ToString().Length > 0 ? kioskVisitor.VistorVisitType.ToString()[0].ToString() : "";

                    //ilog.LogInfo("before UserControl_Loaded BrotherPrinter.Instance.PrintStickerAsync");
                    await Utility.BrotherPrinter.CapitalandPrint.Instance.PrintStickerAsync(ilog, ConfigurationStore.Instance.KioskFilePath, ConfigurationStore.Instance.PrinterTemplateFileName,
                                            ConfigurationStore.Instance.TempVisitorPhotoFileName, stickerTitle, kioskVisitor.LocationLevel, kioskVisitor.VisitorName);
                }

                initialTimer.Start();

                NavigationHelper.Instance.HideLoadingPanel();
            }
            else
            {
                string error = navigationPage.GetMessages(4);
                NavigationHelper.Instance.NavigateToError(error);
            }
        }

        private async Task<bool> DispenseCard()
        {
            bool successful = false;
            SerialCardDispenser serialCardDispenser = SerialCardDispenserManager.Instance.GetSerialCardDispenserByLevel(kioskVisitor.LocationID);

            await Task.Delay(500);

            int counter = 0;
            do
            {
                serialCardDispenser.DispenseCardToReadArea();

                await Task.Delay(1000);
                counter++;
            }
            while (cardDispenserStatus != SerialCardDispenserStatus.CardAtReaderLocation && counter < 5);

            if (cardDispenserStatus == SerialCardDispenserStatus.CardAtReaderLocation)
            {
                for (int i = 0; i < 10; i++)
                {
                    await Task.Delay(500);

                    if (ConfigurationStore.Instance.KioskLocation != "SBR")
                    {
                        if (!string.IsNullOrWhiteSpace(kioskVisitor.StringFromInput) && kioskVisitor.StringFromInput.Length >= ConfigurationStore.Instance.CardNumberLength)
                            break;
                    }
                    else
                    {
                        if (kioskVisitor.StringFromInputEnd)
                            break;
                    }
                }

                if (ConfigurationStore.Instance.KioskLocation == "SBR")
                    kioskVisitor.StringFromInput = kioskVisitor.StringFromInput.PadLeft(ConfigurationStore.Instance.CardNumberLength, '0');

                kioskVisitor.CardNumber = kioskVisitor.StringFromInput;

                ilog.LogInfo($"From Reader Card Number = {kioskVisitor.CardNumber}");

                if (!string.IsNullOrWhiteSpace(kioskVisitor.CardNumber) && kioskVisitor.CardNumber.Length >= ConfigurationStore.Instance.CardNumberLength)
                {
                    try
                    {
                        int cardNumberDecimal = 0;

                        if (ConfigurationStore.Instance.KioskLocation != "SBR")
                        {
                            string swapCardNumber = kioskVisitor.CardNumber.Substring(6, 2) +
                                                kioskVisitor.CardNumber.Substring(4, 2) +
                                                kioskVisitor.CardNumber.Substring(2, 2) +
                                                kioskVisitor.CardNumber.Substring(0, 2);

                            cardNumberDecimal = Convert.ToInt32(swapCardNumber, 16);

                            ilog.LogInfo($"Swap Card Number = {swapCardNumber}");
                            ilog.LogInfo($"Final Card Number Decimal = {cardNumberDecimal}");

                            this.cardNumberFinal = cardNumberDecimal.ToString();
                        }
                        else
                            this.cardNumberFinal = kioskVisitor.CardNumber;
                        
                        if (ConfigurationStore.Instance.KioskLocation == "OGS")
                        {
                            AccessGroup accessGroup = siemensAccessGroups.FirstOrDefault(e => e.Name.Equals(kioskVisitor.LocationName, StringComparison.InvariantCultureIgnoreCase));
                            if (accessGroup != null)
                            {
                                this.selectedAccessGroup = accessGroup;
                                //WorkGroup workGroup = siemensWorkGroups.FirstOrDefault(e => e.Name.Equals(kioskVisitor.LocationName, StringComparison.InvariantCultureIgnoreCase));
                                //if (workGroup != null)
                                //{


                                SiemensAPIHelper siemensAPIHelper = new SiemensAPIHelper(ilog, ConfigurationStore.Instance.KioskID, ConfigurationStore.Instance.SiemenAPIUrl);
                                string token = await siemensAPIHelper.GetToken(ConfigurationStore.Instance.SiemenAPILogin, ConfigurationStore.Instance.SiemenAPIPassword);
                                if (!string.IsNullOrWhiteSpace(token))
                                {
                                    CardHolder cardHolder = await siemensAPIHelper.GetCardHolderByCardNumber(token, cardNumberDecimal.ToString());
                                    if (cardHolder != null)
                                        await siemensAPIHelper.DeleteCardHolderbyID(token, cardHolder.Token);

                                    await siemensAPIHelper.CreateCardHolder(token, kioskVisitor.VisitorName, cardNumberDecimal.ToString(),
                                                    accessGroup.Token, accessGroup.Name);//, workGroup.Token.ToString(), workGroup.Name);

                                    await siemensAPIHelper.Logout(token);

                                    await Task.Delay(500);
                                    serialCardDispenser.DispenseCardFromReadToFull();
                                    successful = true;
                                }
                                else
                                {
                                    string error = navigationPage.GetMessages(4);
                                    ilog.LogError("Unable to dispense card (error=SiPass empty token)");
                                    NavigationHelper.Instance.NavigateToError(error);
                                }


                                //}
                                //else
                                //{
                                //    string error = "Unable to dispense card for the location selected (error=Invalid SiPass WorkGroup)";
                                //    ilog.LogError(error);
                                //    NavigationHelper.Instance.NavigateToError(error);
                                //}
                            }
                            else
                            {
                                string error = navigationPage.GetMessages(6);
                                ilog.LogError("Unable to dispense card for the location selected (error=Invalid SiPass AccessGroup)");
                                NavigationHelper.Instance.NavigateToError(error);
                            }
                        }
                        else
                        {
                            await Task.Delay(500);

                            serialCardDispenser.DispenseCardFromReadToFull();
                            successful = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ilog.LogError("Error in decoding card number from reader", ex);

                        string error = navigationPage.GetMessages(4);
                        NavigationHelper.Instance.NavigateToError(error);
                    }
                }
                else
                {
                    serialCardDispenser.RetrieveCard();
                    string error = navigationPage.GetMessages(5);
                    ilog.LogError("Unable to dispense card (error=Card Number empty or invalid)");
                    NavigationHelper.Instance.NavigateToError(error);
                }
            }
            else
            {
                serialCardDispenser.RetrieveCard();
                string error = navigationPage.GetMessages(5);
                ilog.LogError("Unable to dispense card (error=Unable to dispense card to reader area");
                NavigationHelper.Instance.NavigateToError(error);
            }
            return successful;
        }


        private int initialTimerCounter = 1;
        private void initialTimer_Tick(object sender, EventArgs args)
        {
            if (CardReadyForRetrieval && cardDispenserStatus != SerialCardDispenserStatus.CardReadyForRetrieval)
            {
                // card already dispense and then no longer at dispenser (card took by visitor)
                StopInitialTimer();
                StartCloseScreenCountdown();
            }
            else
            {
                if (initialTimerCounter > ConfigurationStore.Instance.CloseScreenTimer)
                {
                    StopInitialTimer();

                    if (CardDispenserWorking)
                    {
                        if (cardDispenserStatus == SerialCardDispenserStatus.CardReadyForRetrieval)
                        {
                            // card still on reader  
                            lblMessage.Text = navigationPage.GetMessages(1);
                            countDownCounter = ConfigurationStore.Instance.CountdownTimer;
                            lblCountdown.Content = countDownCounter;
                            lblCountdown.Visibility = Visibility.Visible;
                            collectCardTimer.Start();
                        }
                        else
                            StartCloseScreenCountdown();
                    }
                    else
                    {
                        ilog.LogInfo("inside !CardDispenserWorking");
                        // do the invalid close screen 
                        OKStackPanel.Visibility = Visibility.Visible;
                        // card no longer on reader
                        countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
                        lblCountdown.Content = ConfigurationStore.Instance.CountdownTimer;
                        lblCountdown.Visibility = Visibility.Visible;
                        closeScreenTimer.Start();
                    }

                }
            }
            initialTimerCounter++;
            CommandManager.InvalidateRequerySuggested();
        }
        
        private void collectCardTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();
            
            if (cardDispenserStatus != SerialCardDispenserStatus.CardReadyForRetrieval)
            {
                StopCollectCardTimer();

                StartCloseScreenCountdown();

            }
            else
            {
                if (countDownCounter < 0)
                {
                    StopCollectCardTimer();
                    SerialCardDispenser serialCardDispenser = SerialCardDispenserManager.Instance.GetSerialCardDispenserByLevel(kioskVisitor.LocationID);
                    if (serialCardDispenser.IsConnected())
                    {
                        bool result = serialCardDispenser.RetrieveCard();
                        CardRetrievedBackByKiosk = true;
                        Thread.Sleep(1000);
                        NavigateToMainScreen();
                    }
                }
            }
        }

        private int countDownCounter = 0;
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();


            if (countDownCounter < 0)
            {
                ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigationHelper.Instance.NavigateToMessageBeforeMain();
            }
        }

        private async void cardDispenserStatusTimer_Tick(object sender, EventArgs args)
        {
            if (CardDispenserWorking)
            {
                SerialCardDispenser serialCardDispenser = SerialCardDispenserManager.Instance.GetSerialCardDispenserByLevel(kioskVisitor.LocationID);
                if (serialCardDispenser.IsConnected())
                {
                    cardDispenserStatus = await serialCardDispenser.RequestStatusAsync();

                    if (cardDispenserStatus == SerialCardDispenserStatus.CardReadyForRetrieval)
                        CardReadyForRetrieval = true;
                }
            }
        }

        // card successfully collected
        private async void StartCloseScreenCountdown()
        {
            if (ConfigurationStore.Instance.KioskLocation == "OGS")
            {
                await Task.Delay(1000);
                //ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigateToMainScreen();
            }
            else
            {
                lblMessage.Text = navigationPage.GetMessages(2);
                OKStackPanel.Visibility = Visibility.Visible;
                // card no longer on reader
                countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
                lblCountdown.Content = countDownCounter;
                lblCountdown.Visibility = Visibility.Visible;
                closeScreenTimer.Start();
            }
        }

        private async void SaveSignInInformation()
        {
            ilog.LogInfo($"ContactNumber={kioskVisitor.VisitorContactNumber}, Name={kioskVisitor.VisitorName}, AccessGroup={(selectedAccessGroup == null ? "" : selectedAccessGroup.Name)}, CardNumberFinal={cardNumberFinal}");

            if (ConfigurationStore.Instance.KioskLocation == "OGS")
            {
                InfiniteAPIHelper infiniteAPIHelper = new InfiniteAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                await infiniteAPIHelper.SaveInfiniteVisitorLog(kioskVisitor.VisitorContactNumber, kioskVisitor.VisitorName, selectedAccessGroup == null ? "" : selectedAccessGroup.Name, cardNumberFinal);
            }
            else
            {
                CapitalandAPIHelper capitalandAPIHelper = new CapitalandAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                string errorMessage = await capitalandAPIHelper.SaveVisitInfo((int)kioskVisitor.VistorVisitType, kioskVisitor.IdentityNumber, kioskVisitor.VisitorName,
                                kioskVisitor.VisitorContactNumber, kioskVisitor.LocationID, cardNumberFinal);
                if (!string.IsNullOrWhiteSpace(errorMessage))
                    ilog.LogError($"Error in apiHelper.SaveVisitInfo : {errorMessage}");
            }
        }

        private void StopInitialTimer()
        {
            initialTimer.Stop();
            initialTimer.Tick -= initialTimer_Tick;
        }
        private void StopCollectCardTimer()
        {
            collectCardTimer.Stop();
            collectCardTimer.Tick -= collectCardTimer_Tick;
        }
        private void StopCloseScreenTimer()
        {
            closeScreenTimer.Stop();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
        }
        private void StopCardDispenserStatusTimer()
        {
            cardDispenserStatusTimer.Stop();
            cardDispenserStatusTimer.Tick -= cardDispenserStatusTimer_Tick;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {

            StopInitialTimer();
            StopCollectCardTimer();
            StopCloseScreenTimer();
            StopCardDispenserStatusTimer();

            //SerialCardDispenserManager.Instance.CloseAllSerialCardDispenser();
        }


        private void NavigateToMainScreen()
        {
            if (!CardRetrievedBackByKiosk)
            {
                // not using this since got card drop
                //if (!string.IsNullOrWhiteSpace(kioskVisitor.CompanyName))
                //    CompanyCardSQLiteHelper.Instance.SaveCompanyCollectedCardData(kioskVisitor.CompanyName);

                SaveSignInInformation();
            }

            //kioskVisitor.CardNumber = ??;
            NavigationHelper.Instance.NavigateToMain();
        }

        #region Buttons
        private void OKStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigateToMainScreen();
        }
        #endregion

    }
}
