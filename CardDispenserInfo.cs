﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model
{
    public class CardDispenserInfo
    {
        public string ComPortNumber { get; set; }
        public int BaudRate { get; set; }
        public int LocationIDMin { get; set; }
        public int LocationIDMax { get; set; }
    }
}
