﻿using JewelVMS.KioskWPF.Utility;
using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Kiosk.ContentPage
{
    public partial class ScanIdentityCardOld : UserControl
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        private System.Windows.Threading.DispatcherTimer identityScannerTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;

        private string identityNumber;
        private string scanningText = "Scanning..";
        private int countDownCounter;

        public ScanIdentityCardOld(NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Content = navigationPage.GetTitle(0);
            lblMessage.Content = navigationPage.GetMessages(0);

            identityScannerTimer = new System.Windows.Threading.DispatcherTimer();
            identityScannerTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            await StartScanning();
            
            NavigationHelper.Instance.HideLoadingPanel();
        }

        private async Task StartScanning()
        {
            identityScannerTimer.Tick += new EventHandler(identityScannerTimer_Tick);
            closeScreenTimer.Tick += new EventHandler(closeScreenTimer_Tick);

            identityNumber = "";
            countDownCounter = ConfigurationStore.Instance.CountdownTimer;
            lblCountdown.Content = scanningText;


            await Task.Run(() =>
            {
                try
                {
                    PassportScanner.Instance.LoadScanner(ConfigurationStore.Instance.PassportScannerUserID,
                                ConfigurationStore.Instance.KioskFilePath, ConfigurationStore.Instance.TempVisitorIdentityFileName);

                    Task.Delay(1000); // put a delay here, 

                    PassportScanner.Instance.StartScanning();
                }
                catch (Exception ex)
                {
                    log.Error("PassportScanner error", ex);
                    ShowMessage(ex.Message);
                }
            });


            identityScannerTimer.Start();
            //IdentityNumber = await PassportScanner.Instance.StartScanningFake(); // use fake, no passport scanner now 
        }

        private void identityScannerTimer_Tick(object sender, EventArgs args)
        {
            int secondPassed = ConfigurationStore.Instance.CountdownTimer - countDownCounter + 1;
            countDownCounter--;
            lblCountdown.Content = scanningText + new string('.', secondPassed >= 0 ? secondPassed : 0);
            
            if (!string.IsNullOrWhiteSpace(PassportScanner.Instance.Identity))
            {
                StopIdentityScannerTimer();
                identityNumber = PassportScanner.Instance.Identity;
                lblMessage.Content = identityNumber;
                
                lblImageTemplate.Visibility = Visibility.Collapsed;
                lblImageIdentity.Visibility = Visibility.Visible;

                if (System.IO.File.Exists(ConfigurationStore.Instance.KioskFilePath + ConfigurationStore.Instance.TempVisitorIdentityFileName))
                    lblImageIdentity.Source = new BitmapImage(new Uri(ConfigurationStore.Instance.KioskFilePath + ConfigurationStore.Instance.TempVisitorIdentityFileName));

                ShowMessage("Confirm Identity [" + identityNumber + "]");

                countDownCounter = ConfigurationStore.Instance.CountdownTimer;
                lblCountdown.Content = countDownCounter.ToString();

                CancelStackPanel.Visibility = Visibility.Collapsed;
                ManualRescanStackPanel.Visibility = Visibility.Collapsed;
                OKStackPanel.Visibility = Visibility.Visible;

                closeScreenTimer.Start();
            }
            else
            {
                if (countDownCounter < 0)
                {
                    StopIdentityScannerTimer();
                    countDownCounter = ConfigurationStore.Instance.CountdownTimer;
                    lblCountdown.Content = countDownCounter;

                    ShowMessage(navigationPage.GetMessages(1));

                    closeScreenTimer.Start();
                }
            }

            // Forcing the CommandManager to raise the RequerySuggested event
            CommandManager.InvalidateRequerySuggested();
        }
        
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();


            if (countDownCounter < 0)
            {
                StopCloseScreenTimer();
                if (!string.IsNullOrWhiteSpace(identityNumber))
                    NavigateToNextScreen();
                else
                    NavigateToMainScreen();
            }
        }
        
        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }
        private async void NavigateToNextScreen()
        {
            NavigationHelper.Instance.ShowLoadingPanel("Verifying...");

            APIHelper apiHelper = new APIHelper(ConfigurationStore.Instance.GMSApiURL, ConfigurationStore.Instance.APICallTimeout);
            string errorMessage = await apiHelper.CheckValidSignIn(kioskVisitor, (int)this.kioskVisitor.VistorVisitType, identityNumber);
            if (string.IsNullOrWhiteSpace(errorMessage))
            {

                NavigationHelper.Instance.HideLoadingPanel();

                kioskVisitor.IdentityNumber = identityNumber;
                NavigationHelper.Instance.NavigateToNext();
            }
            else
                NavigationHelper.Instance.NavigateToError(errorMessage);
        }
        
        private void ShowMessage(string message)
        {
            this.Dispatcher.Invoke(() =>
            {
                lblMessage.Content = message;

                grdCancel.Visibility = Visibility.Collapsed;
                grdRescan.Visibility = Visibility.Visible;
            });
        }

        private void StopIdentityScannerTimer()
        {
            PassportScanner.Instance.StopScanner();
            identityScannerTimer.Stop();
            identityScannerTimer.Tick -= identityScannerTimer_Tick;
        }
        private void StopCloseScreenTimer()
        {
            closeScreenTimer.Stop();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopIdentityScannerTimer();
            StopCloseScreenTimer();
            PassportScanner.Instance.StopScanner();
        }

        #region Buttons
        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigateToMainScreen();
        }

        private void ManualEntryStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToPage(NavigationHelper.Navigate.EnterIdentityManual);
        }

        private void OKStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigateToNextScreen();
        }

        private async void RescanStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.ShowLoadingPanel();

            StopCloseScreenTimer();

            grdCancel.Visibility = Visibility.Visible;
            grdRescan.Visibility = Visibility.Collapsed;

            CancelStackPanel.Visibility = Visibility.Visible;
            ManualRescanStackPanel.Visibility = Visibility.Visible;
            OKStackPanel.Visibility = Visibility.Collapsed;

            lblMessage.Content = navigationPage.GetMessages(0);

            lblImageTemplate.Visibility = Visibility.Visible;
            lblImageIdentity.Visibility = Visibility.Collapsed;

            await StartScanning();
            
            NavigationHelper.Instance.HideLoadingPanel();
        }
        #endregion
    }
}
