﻿using Kiosk.Model.Mom;
using Kiosk.Model.Siemens;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kiosk.Utility.API
{
    public class MomAPIHelperOld
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private string baseURL = "";
        public MomAPIHelperOld(Utility.Log.Ilog ilog, string baseURL)
        {
            this.baseURL = baseURL;
        }
        
        public async Task<WorkerStatus> GetWorkerStatus(string FIN)
        {
            WorkerStatus workerStatus = null;
            await Task.Run(() =>
            {
                IRestResponse restResponse = GenericRestAPICall($"/api/mom/workerstatus/{FIN}", Method.GET);

                workerStatus = JsonConvert.DeserializeObject<WorkerStatus>(restResponse.Content);

            });

            return workerStatus;
        }
        
        //public async Task<bool> UpdateWorker(string FIN, string name, string department)
        //{
        //    object requestBody = new
        //    {
        //        name = name,
        //        department = department
        //    };

        //    bool result = false;

        //    await Task.Run(() =>
        //    {
        //        IRestResponse restResponse = GenericRestAPICall($"/api/mom/workerstatus/{FIN}", Method.PUT, requestBody);

        //        if (restResponse.IsSuccessful)
        //            result = true;
        //        else
        //        {
        //            //log ?
        //        }

        //    });

        //    return result;
        //}
        
        private IRestResponse GenericRestAPICall(string method, Method postOrGet, object requestBody = null)
        {
            var client = new RestClient(baseURL);
            var request = new RestRequest(method, postOrGet);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-Type", "application/json");

            if (requestBody != null)
                request.AddJsonBody(requestBody);

            IRestResponse response = client.Execute(request);

            if (!response.IsSuccessful)
            {
                // log?
                ilog.LogInfo($"Response (failed) - {response.Content}");
            }
            else
            {
                ilog.LogInfo($"Response (success) - {response.Content}");
            }

            return response;
        }

    }
}
