﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model
{
    public class KioskConfig
    {
        public string KioskID { get; set; }
        public int VisitorReentryLimitInSeconds { get; set; }
        public string KioskMainScheduleCron { get; set; }
        public string KioskDisableTimingForLevel { get; set; }
        public bool DisableKioskOnCardDispenserEmpty { get; set; }
    }
}
