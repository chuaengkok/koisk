﻿namespace Kiosk.Model.API.Response
{
    public class GallagherResponse
    {
        public string Message { get; set; }
        public int Code { get; set; }
    }
}
