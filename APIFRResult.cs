﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model.API.Response
{
    public class APIFRResult
    {
        public decimal score { get; set; }
    }

    public class APIFREnrollFaceResult
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class APIFRQualityResult
    {
        public bool IsSuccess { get; set; }
    }

    public class APIFRCropResult
    {
        public string base64 { get; set; }
    }
}
