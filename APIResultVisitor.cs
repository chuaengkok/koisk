﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model.API.Response
{
    public class APIResultVisitor
    {
        public string IdentityNumber { get; set; }
        public string IdentityNumberLast4 { get; set; }
        public string VisitorName { get; set; }
        public string VisitorContactNumber { get; set; }
        public string CompanyName { get; set; }
        public bool Blacklisted { get; set; } = false;
        public long? LocationID { get; set; }
        public string LocationName { get; set; }
        public string CardNumber { get; set; }
        public int VisitorTypeID { get; set; }
        public string UnitName { get; set; }
        public string UnitNumber { get; set; }
        public string PTWNumber { get; set; }
        public bool? IncludeSaturday { get; set; } = true;
        public bool? IncludeSunday { get; set; } = true;
        public bool? IncludeHoliday { get; set; } = true;
        public DateTime? EntryStartDatetime { get; set; }
        public DateTime? EntryEndDatetime { get; set; }
    }
}
