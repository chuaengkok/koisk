﻿using System.Collections.Generic;
using System.Linq;

namespace Kiosk.Model
{
    public class NavigationPage
    {
        public string Page { get; set; }
        public List<string> Titles { get; set; }
        public List<string> Messages { get; set; }
        public bool SkipIfHasValue { get; set; } = false;
        public bool AllowsBlankValue { get; set; } = false;
        public bool PerformVerification { get; set; } = true;

        public List<string> FeaturesEnabled { get; set; }

        public string GetTitle(int index)
        {
            if (Titles != null && Titles.Count > index)
                return Titles[index];
            return "";
        }
        public string GetMessages(int index)
        {
            if (Messages != null && Messages.Count > index && !string.IsNullOrWhiteSpace(Messages[index]))
                return Messages[index].Replace("/n", System.Environment.NewLine);
            return "";
        }

        public bool CheckIfFeatureEnabled(string featureName)
        {
            if (FeaturesEnabled != null)
                return FeaturesEnabled.FirstOrDefault(i => i.Equals(featureName, System.StringComparison.OrdinalIgnoreCase)) != null;
            else
                return false;
        }
    }
}
