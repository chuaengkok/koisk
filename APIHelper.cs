﻿using Kiosk.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Kiosk.Utility.API
{
    public class APIHelper
    {
        //protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected Utility.Log.Ilog ilog;

        private string url = "";
        private int apiCallTimeout = 5;
        private List<HttpStatusCode> unableToConnectToServerErrorList = new List<HttpStatusCode>()
                        {
                            HttpStatusCode.BadGateway, HttpStatusCode.Forbidden, HttpStatusCode.GatewayTimeout,
                            HttpStatusCode.NotFound, HttpStatusCode.RequestTimeout, HttpStatusCode.ServiceUnavailable
                        };

        public APIHelper(Utility.Log.Ilog ilog, string url, int apiCallTimeout)
        {
            this.ilog = ilog;
            this.url = url;
            this.apiCallTimeout = apiCallTimeout;
        }

        protected IRestResponse GenericRestAPICall(string apiPath, Method postOrGet, object requestBody = null, Dictionary<string, string> headerValues = null,
                                            bool bypassSSL = false)
        {
            var client = new RestClient(url);
            var request = new RestRequest(apiPath, postOrGet);
            request.Timeout = apiCallTimeout * 1000;
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-Type", "application/json");

            if (headerValues != null)
            {
                foreach (KeyValuePair<string, string> headerValue in headerValues)
                    request.AddHeader(headerValue.Key, headerValue.Value);
            }

            if (requestBody != null)
                request.AddJsonBody(requestBody);

            if (bypassSSL)
                System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;


            IRestResponse response = client.Execute(request);
            

            //if (!response.IsSuccessful)
            //{
            //    ilog.LogError(response.Content);
            //    if (unableToConnectToServerErrorList.Contains(response.StatusCode) ||
            //        (response.ErrorException is WebException &&
            //        ((WebException)response.ErrorException).Status == WebExceptionStatus.Timeout))
            //        throw new TimeoutException("Unable to communicate with server, please contact security officer for assistance");
            //    else
            //        throw new Exception(response.ErrorException.Message);
            //}



            if (!response.IsSuccessful)
            {
                ilog.LogError($"Response (failed) - content = {response.Content}");

                if (response.ErrorException != null)
                    ilog.LogError($"response ErrorException - {response.ErrorException}");
                else if (!string.IsNullOrWhiteSpace(response.ErrorMessage))
                {
                    ilog.LogError($"response ErrorMessage - {response.ErrorMessage}");
                }
                else
                    ilog.LogError($"response error StatusDescription - {response.StatusDescription ?? response.ErrorMessage ?? "Unknown error"}");
            }
            else
                ilog.LogDebug($"Response (success) {response.Content}");

            return response;
        }
    }
}
