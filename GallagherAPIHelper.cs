﻿using Kiosk.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kiosk.Model.API.Response;

namespace Kiosk.Utility.API
{
    public class GallagherAPIHelper : APIHelper
    {
        Dictionary<string, string> header = new Dictionary<string, string>();

        public GallagherAPIHelper(Utility.Log.Ilog ilog, string url, int apiCallTimeout, string apiKey) : base (ilog, url, apiCallTimeout)
        {
            header.Add("Authorization", apiKey);
        }
        public async Task<GallagherResponse> CreateCardHolder(string visitorName, string visitorType, string cardNumber,
                    DateTime entryStartDatetime, DateTime entryEndDatetime,
                    string gallagherDivisionUrl, string gallagherAccessGroupUrl, string gallagherCardTypeUrl)
        {
            GallagherResponse apiResult = new GallagherResponse() ;
            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        firstName = string.IsNullOrWhiteSpace(visitorName) ? $"{visitorType} {DateTime.Now.ToString("ddMMMyyyyHHmmss")}" : visitorName,
                        description = visitorType,
                        authorised = true,
                        division = new
                        {
                            href = gallagherDivisionUrl
                        },
                        accessGroups = new object[]
                        {
                            new
                            {
                                accessGroup = new
                                {
                                    href = gallagherAccessGroupUrl
                                }
                            }
                        },
                        useExtendedAccessTime = false,
                        notifications = new
                        {
                            enabled = false
                        },
                        cards = new object[]
                        {
                            new
                            {
                                number = cardNumber,
                                issueLevel = 1,
                                type = new
                                {
                                    href = gallagherCardTypeUrl
                                },
                                from = entryStartDatetime.ToString("yyyy-MM-ddTHH:mm:00+8:00"),
                                until = entryStartDatetime.ToString("yyyy-MM-ddTHH:mm:00+8:00"),
                                credentialClass = "card"
                            }
                        }
                    };

                    // for debugging only
                    //string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(requestBody);


                    IRestResponse restResponse = GenericRestAPICall("api/cardholders", Method.POST, requestBody, header, true);

                    if (restResponse.IsSuccessful)
                    {
                        // no need to do anything
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(restResponse.Content))
                        {
                            apiResult = JsonConvert.DeserializeObject<GallagherResponse>(restResponse.Content);
                            if (!string.IsNullOrWhiteSpace(apiResult.Message))
                                ilog.LogDebug($"Gallagher API Result error message - {apiResult.Message}");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult;
        }

    }

    // working json (from postman)
    /*
     
    {
    "firstName": "Test From API",
    "description": "Visitor Type",
    "authorised": true,
    "division": {
        "href": "https://172.31.12.221:8904/api/divisions/587"
    },
    "accessGroups": [
        {
            "accessGroup": {
                "href": "https://172.31.12.221:8904/api/access_groups/1090"
            }
        }
    ],
    "useExtendedAccessTime": false,
    "notifications": {
        "enabled": false
    },

    "cards": [
        {
            "number": "10101011",
            "issueLevel": 1,
            "type": {
                "href": "https://172.31.12.221:8904/api/card_types/1077"
            },
            "from": "2019-12-01T13:00:00+8:00",
            "until": "2019-12-01T15:00:00+8:00",
            "credentialClass": "card"
        }
    ]
}

    */
}
