﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Kiosk.Model
{
    public class StaffAttendanceModel
    {
        public long StaffId { get; set; }
        public BitmapImage Photo { get; set; }
        public string StaffName { get; set; }
        public string ClockedTime { get; set; }
    }
}
