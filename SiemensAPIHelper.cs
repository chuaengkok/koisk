﻿using Kiosk.Model.Siemens;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public class SiemensAPIHelper
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string clientID = "";
        private string baseURL = "";

        private Utility.Log.Ilog ilog;

        public SiemensAPIHelper(Utility.Log.Ilog ilog, string clientID, string baseURL)
        {
            this.ilog = ilog;
            this.clientID = clientID;
            this.baseURL = baseURL;
        }

        // Login
        public async Task<string> GetToken(string loginID, string password)
        {
            string token = "";
            await Task.Run(() =>
            {
                object requestBody = new
                {
                    userName = loginID,
                    password = password
                };

                try
                {
                    IRestResponse restResponse = GenericRestAPICall("api/siemens/login", "", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                    {
                        LoginResponse loginResponse = JsonConvert.DeserializeObject<LoginResponse>(restResponse.Content);
                        token = loginResponse.Token;
                    }
                }
                catch (Exception ex)
                {
                    ilog.LogError($"Exception in SiemensAPIHelper.GetToken", ex);
                }
            });

            return token;
        }

        // log out
        public async Task<bool> Logout(string token)
        {
            bool successful = false;

            await Task.Run(() =>
            {
                IRestResponse restResponse = GenericRestAPICall("api/siemens/logout", token, Method.POST);
                successful = restResponse.IsSuccessful;
            });

            return successful;
        }

        // get access group
        public async Task<List<AccessGroup>> GetAccessGroups(string token)
        {
            List<AccessGroup> accessGroups = new List<AccessGroup>();
            await Task.Run(() =>
            {
                AccessGroupReturn accessGroupReturn = null;
                try
                {
                    IRestResponse restResponse = GenericRestAPICall("api/siemens/accessgroups",
                            token, Method.GET);

                    if (restResponse.IsSuccessful)
                        accessGroupReturn = JsonConvert.DeserializeObject<AccessGroupReturn>(restResponse.Content);

                    if (accessGroups != null)
                        accessGroups = accessGroupReturn.Records;
                }
                catch (Exception ex)
                {
                    ilog.LogError($"Exception in SiemensAPIHelper.GetAccessGroups", ex);
                }
            });

            return accessGroups;
        }
        
        public async Task<List<WorkGroup>> GetWorkGroups(string token)
        {
            List<WorkGroup> workGroups = new List<WorkGroup>();
            await Task.Run(() =>
            {
                WorkGroupReturn workGroupReturn = null;
                try
                {
                    IRestResponse restResponse = GenericRestAPICall("api/siemens/workgroups",
                            token, Method.GET);

                    if (restResponse.IsSuccessful)
                        workGroupReturn = JsonConvert.DeserializeObject<WorkGroupReturn>(restResponse.Content);

                    if (workGroups != null)
                        workGroups = workGroupReturn.Records;
                }
                catch (Exception ex)
                {
                    ilog.LogError($"Exception in SiemensAPIHelper.GetWorkGroups", ex);
                }
            });

            return workGroups;
        }


        // get card holder
        public async Task<CardHolder> GetCardHolderByCardNumber(string token, string cardNumber)
        {
            CardHolder cardHolder = null;
            await Task.Run(() =>
            {
                CardHolderReturn cardHolderReturn = null;
                try
                {
                    IRestResponse restResponse = GenericRestAPICall($"api/siemens/cardholder/{cardNumber}",
                            token, Method.GET);

                    if (restResponse.IsSuccessful)
                        cardHolderReturn = JsonConvert.DeserializeObject<CardHolderReturn>(restResponse.Content);

                    if (cardHolderReturn != null && cardHolderReturn.Records != null)
                        cardHolder = cardHolderReturn.Records.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    ilog.LogError($"Exception in SiemensAPIHelper.GetCardHolderByCardNumber", ex);
                }
            });

            return cardHolder;
        }

        // delete card holder
        public async Task<bool> DeleteCardHolderbyID(string token, string cardHolderID)
        {
            bool successful = false;
            await Task.Run(() =>
            {
                IRestResponse restResponse = GenericRestAPICall($"api/siemens/cardholder/{cardHolderID}",
                    token, Method.DELETE);
                successful = restResponse.IsSuccessful;
            });

            return successful;

        }


        // create card holder
        public async Task<bool> CreateCardHolder(string token, string name,
                        string cardnumber, string accessGroupToken, string accessGroupName) 
                       // string workGroupToken, string workGroupName)
        {
            bool successful = false;
            await Task.Run(() =>
            {
                object request = new
                {
                    name = name,
                    cardnumber = cardnumber,
                    accessGroupToken = accessGroupToken,
                    accessGroupName = accessGroupName
                    //workGroupToken = workGroupToken,
                    //workGroupName = workGroupName
                };

                IRestResponse restResponse = GenericRestAPICall("api/siemens/cardholder", token, Method.POST, request);
                successful = restResponse.IsSuccessful;
            });

            return successful;
        }


        private IRestResponse GenericRestAPICall(string method, string token, Method postOrGet, object requestBody = null)
        {
            var client = new RestClient(baseURL);
            var request = new RestRequest(method, postOrGet);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("clientUniqueId", clientID);

            if (!string.IsNullOrWhiteSpace(token))
                request.AddHeader("Authorization", token);

            if (requestBody != null)
                request.AddJsonBody(requestBody);

            IRestResponse response = client.Execute(request);

            if (!response.IsSuccessful)
            {
                // log?
                ilog.LogInfo($"Response (failed) - {response.Content}");
            }
            else
            {
                ilog.LogInfo($"Response (success) - {response.Content}");
            }

            return response;
        }

        // login 

        // logout

        // get access group

        //get cardholder

        // delete cardholder

        // create cardholder


    }
}
