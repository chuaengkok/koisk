﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public class PassportScannerTimerOld
    {
        private static readonly Lazy<PassportScannerTimerOld> lazy = new Lazy<PassportScannerTimerOld>(() => new PassportScannerTimerOld());
        public static PassportScannerTimerOld Instance { get { return lazy.Value; } }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /*
        passport scanner information card type
        2005 - Singapore NRIC 2005 - 新加坡身份证2005版
        2004 - Singapore NRIC - 新加坡身份证
                IDENTITY CARD NO.
        2025 - Work permit 2017 - 新加坡工作证2017版-指纹页
        2031 - Singapore Driving License - 新加坡驾驶证
                Driver's license number
        13 - Passport

            Field name removed
                - "Passport number" 
        */

        private static List<int> CardTypeIDList;
        private static List<string> WantedFieldForIDList;
        private static List<string> WantedFieldForNameList;


        private static List<int> CardTypeIDListDefault = new List<int>() { 2004, 2031, 2005, 2023, 2025 };
        private static List<string> WantedFieldForIDListDefault = new List<string> { "IDENTITY CARD NO.", "FIN", "Driver's license number"};
        private static List<string> WantedFieldForNameListDefault = new List<string>{ "Name"};

        private static int PassportCardID = 13;
        private static string PassportIDFieldName = "Passport number";
        private static string PassportNameFieldName = "English name";

        // doesnt work, scanner only scan back of work permit and Spass
        //private static string WorkPermitIDFieldName = "Work Permit No.";
        //private static string SPassIDFieldName = "S Pass No.";

        public string Identity { get; set; }
        public string Name { get; set; }

        private string savedImageFolderPath = "";
        private string savedImageFileName = "";

        //indicates the idcard.dll is loaded or not
        private bool m_bIsIDCardLoaded = false;

        private static class MyDll
        {
            [DllImport("kernel32")]
            public static extern int LoadLibrary(string strDllName);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int InitIDCard(char[] cArrUserID, int nType, char[] cArrDirectory);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetRecogResult(int nIndex, char[] cArrBuffer, ref int nBufferLen);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogIDCard();

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetFieldName(int nIndex, char[] cArrBuffer, ref int nBufferLen);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int AcquireImage(int nCardType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SaveImage(char[] cArrFileName);
            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SaveHeadImage(char[] cArrFileName);


            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetCurrentDevice(char[] cArrDeviceName, int nLength);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern void GetVersionInfo(char[] cArrVersion, int nLength);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern bool CheckDeviceOnline();

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern bool SetAcquireImageType(int nLightType, int nImageType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern bool SetUserDefinedImageSize(int nWidth, int nHeight);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern bool SetAcquireImageResolution(int nResolutionX, int nResolutionY);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SetIDCardID(int nMainID, int[] nSubID, int nSubIdCount);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int AddIDCardID(int nMainID, int[] nSubID, int nSubIdCount);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogIDCardEX(int nMainID, int nSubID);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetButtonDownType();

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetGrabSignalType();

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SetSpecialAttribute(int nType, int nSet);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern void FreeIDCard();
            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetDeviceSN(char[] cArrSn, int nLength);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetBusinessCardResult(int nID, int nIndex, char[] cArrBuffer, ref int nBufferLen);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogBusinessCard(int nType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetBusinessCardFieldName(int nID, char[] cArrBuffer, ref int nBufferLen);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetBusinessCardResultCount(int nID);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int LoadImageToMemory(string path, int nType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int ClassifyIDCard(ref int nCardType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogChipCard(int nDGGroup, bool bRecogVIZ, int nSaveImageType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogGeneralMRZCard(bool bRecogVIZ, int nSaveImageType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int RecogCommonCard(int nSaveImageType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int SaveImageEx(char[] lpFileName, int nType);

            [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
            public static extern int GetDataGroupContent(int nDGIndex, bool bRawData, byte[] lpBuffer, ref int len);
        }

        public bool IsIDCardLoaded
        {
            get { return m_bIsIDCardLoaded; }
        }

        // passport scanner related
        public bool LoadScanner(string passportScannerUserID, string savedImageFolderPath, string savedImageFileName)
        {
            this.savedImageFolderPath = savedImageFolderPath;
            this.savedImageFileName = savedImageFileName;
            //check if the kernel is loaded or not
            if (m_bIsIDCardLoaded)
            {
                //textBoxDisplayResult.Text = "The recognition engine is loaded successfully.";
                return true;
            }
            
            int nRet;
            nRet = MyDll.LoadLibrary("IDCard");
            if (nRet == 0)
            {
                throw new Exception("Failed to load IDCard.dll");
            }

            //Load engine

            char[] arr = passportScannerUserID.ToCharArray();

            nRet = MyDll.InitIDCard(arr, 1, null);
            if (nRet != 0)
            {
                throw new Exception("Failed to initialize the recognition engine.\r\n" + nRet.ToString());
            }
            MyDll.SetSpecialAttribute(1, 1);
            m_bIsIDCardLoaded = true;

            return true;
        }

        public void StopTimer()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Elapsed -= timer_Tick;
            }
        }

        public void StopScanner()
        {
            //timer.Enabled = false;
            // Scanning = false;
            StopTimer();

            if (m_bIsIDCardLoaded)
            {
                MyDll.FreeIDCard();
                m_bIsIDCardLoaded = false;
            }
        }

        private System.Timers.Timer timer;

        public void StartScanning(bool includesPassport = false)//, bool includesWorkPermitNumber = false)
        {
            CardTypeIDList = new List<int>(CardTypeIDListDefault);
            WantedFieldForIDList = new List<string>(WantedFieldForIDListDefault);
            WantedFieldForNameList = new List<string>(WantedFieldForNameListDefault);

            if (includesPassport)
            {
                CardTypeIDList.Add(PassportCardID);
                WantedFieldForIDList.Add(PassportIDFieldName);
                WantedFieldForNameList.Add(PassportNameFieldName);
            }

            //if (includesWorkPermitNumber)
            //{
            //    WantedFieldForIDList.Add(WorkPermitIDFieldName);
            //    WantedFieldForIDList.Add(SPassIDFieldName);
            //}


            Identity = "";
            timer = new System.Timers.Timer();
            timer.Interval = 500;
            timer.Elapsed += timer_Tick;
            timer.AutoReset = true;
            timer.Start();

            counter = 0;
        }

        private static readonly object _locker = new object();
        private void timer_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Identity))
            {
                if (!Monitor.TryEnter(_locker)) { return; }
                
                try
                {
                    Identity = AutoClassAndRecognize();
                }
                catch (Exception ex )
                {
                    log.Error("Do Passport Scanning timer exception", ex);
                }
                finally
                {
                    Monitor.Exit(_locker);
                }
            }
            else
                StopTimer();
        }


        private int counter = 0;
        private void doScanningFake()
        {
            System.Diagnostics.Debug.WriteLine("inside fake scanning");
            if (counter >= 10)
            {
                System.Diagnostics.Debug.WriteLine("return 111111");
                Identity = "111111";
            }
            counter++;
        }


        //private static readonly object _locker = new object();


        //private CancellationTokenSource cancellationTokenSource { get; set; }  // set to false when stop scanning;
        //public async Task<string> StartScanning()
        //{
        //    if (!m_bIsIDCardLoaded)
        //        throw new Exception("Recognition engine not loaded");

        //    Identity = "";
        //    //Scanning = true;

        //    while (true)
        //    {
        //        // do the work in the loop
        //        Identity = AutoClassAndRecognize();
        //        if (!string.IsNullOrWhiteSpace(Identity))
        //            break;

        //        // don't run again for at least 200 milliseconds
        //        await Task.Delay(1000);
        //    }

        //    return Identity;
        //}

        ///// <summary>
        ///// for testing Async function without passport scanner, will return "111111" after 10 seconds 
        ///// </summary>
        ///// <returns></returns>
        //public async Task<string> StartScanningFake()
        //{
        //    //if (!m_bIsIDCardLoaded)
        //    //    throw new Exception("Recognition engine not loaded");

        //    //Scanning = true;
        //    cancellationTokenSource = new CancellationTokenSource();
        //    int counter = 0;

        //    try
        //    {
        //        while (true)
        //        {
        //            Console.WriteLine("inside while scanning");
        //            if (cancellationTokenSource.IsCancellationRequested)
        //                cancellationTokenSource.Token.ThrowIfCancellationRequested();

        //            if (counter >= 10)
        //            {
        //                Console.WriteLine("return 111111");
        //                return "111111";
        //            }

        //            counter++;

        //            // don't run again for at least 200 milliseconds
        //            await Task.Delay(1000);
        //        }
        //    }
        //    catch (OperationCanceledException ex)
        //    {
        //        Console.WriteLine("Ops Cancelled");
        //    }

        //    return "";
        //}

        private string GetStringWithoutNull(char[] charList)
        {
            if (charList != null && charList.Length > 0)
            {
                string temp = new string(charList);
                string[] tempResultList = temp.Split(new[] { '\0' }, 2);
                if (tempResultList.Length > 0)
                {
                    tempResultList[0] = tempResultList[0].Replace(Environment.NewLine, " ");
                    return tempResultList[0];
                }
            }
            return "";
        }

        public bool CheckScannerOnline()
        {
            if (!m_bIsIDCardLoaded)
                throw new Exception("Recognition engine not loaded");

            bool bRet = MyDll.CheckDeviceOnline();
            if (bRet)
                return true;
            else
                return false;
        }

        private string AutoClassAndRecognize()
        {
            string identityValue = "";
            int nRet = MyDll.GetGrabSignalType();
            if (nRet == 1)
            {
                int[] nSubID = new int[1];
                nSubID[0] = 0;

                for (int i = 0; i < CardTypeIDList.Count; i++)
                {
                    if (i == 0)
                        MyDll.SetIDCardID(CardTypeIDList[i], nSubID, 1);
                    else
                        MyDll.AddIDCardID(CardTypeIDList[i], nSubID, 1);
                }


                int nCardType = 0;
                nRet = MyDll.ClassifyIDCard(ref nCardType);
                if (nRet <= 0)
                {
                    return "";
                }



                int nDG = 15;
                int nSaveImage = 3;
                bool bVIZ = true;

                if (nCardType == 1)
                {
                    nRet = MyDll.RecogChipCard(nDG, bVIZ, nSaveImage);
                }

                if (nCardType == 2)
                {
                    nRet = MyDll.RecogGeneralMRZCard(bVIZ, nSaveImage);
                }

                if (nCardType == 3)
                {
                    nRet = MyDll.RecogCommonCard(nSaveImage);
                }

                if (nRet < 0)
                {
                    return "";
                }

                int MAX_CH_NUM = 128;
                char[] cArrFieldValue = new char[MAX_CH_NUM];
                char[] cArrFieldName = new char[MAX_CH_NUM];

                for (int i = 1; ; i++)
                {

                    nRet = MyDll.GetRecogResult(i, cArrFieldValue, ref MAX_CH_NUM);
                    if (nRet == 3)
                    {
                        break;
                    }
                    MyDll.GetFieldName(i, cArrFieldName, ref MAX_CH_NUM);

                    //textBoxDisplayResult.Text += new String(cArrFieldName);
                    //textBoxDisplayResult.Text += ":";
                    //textBoxDisplayResult.Text += new String(cArrFieldValue);
                    //textBoxDisplayResult.Text += "\r\n";


                    string tempFieldName = GetStringWithoutNull(cArrFieldName);

                     // get Identity Number
                    if (WantedFieldForIDList.Contains(tempFieldName))
                    {
                        identityValue = GetStringWithoutNull(cArrFieldValue);
                        //break; // remove to add checking for name
                    }
                    
                    // get name
                    if (WantedFieldForNameList.Contains(tempFieldName))
                    {
                        Name = GetStringWithoutNull(cArrFieldValue);
                        Name = Name.Replace("\r\n", Environment.NewLine);
                        //break; // remove to add checking for name
                    }
                }


                if (!string.IsNullOrWhiteSpace(identityValue))
                {
                    //SaveImage
                    char[] carrImgPath = (savedImageFolderPath + savedImageFileName ).ToCharArray();
                    nRet = MyDll.SaveImageEx(carrImgPath, nSaveImage);
                    if (nRet != 0)
                    {
                        log.Info($"after save image to file nRet != 0  nRet={nRet}");
                    }
                }
            }
            return identityValue;
        }

    }
}
