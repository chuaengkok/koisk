﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model.Quartz
{
    public class CronSchedule
    {
        public string StartSchedule { get; set; }
        public string StopSchedule { get; set; }
    }
}
