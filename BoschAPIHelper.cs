﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Threading.Tasks;
using Kiosk.Model.API.Response;
using Kiosk.Model.API;

namespace Kiosk.Utility.API
{
    public class BoschAPIHelper : APIHelper
    {
        public BoschAPIHelper(Utility.Log.Ilog ilog, string url, int apiCallTimeout) : base (ilog, url, apiCallTimeout)
        {
        }

        public async Task<string> IssueCode(int visitorType, int cardNumber, string name)
        {
            BoschResponse boschResponse = new BoschResponse();
            string returnMessage = "";

            string startDate = "";
            string startTime = "";
            string endDate = "";
            string endTime = "";

            if (ConfigurationStore.Instance.IsDebug)
            {
                startDate = DateTime.Now.ToString("yyyy/MM/dd");
                startTime = DateTime.Now.ToString("HH:00");
                endDate = DateTime.Now.AddHours(2).ToString("yyyy/MM/dd");
                endTime = DateTime.Now.AddHours(2).ToString("HH:00");
            }
            else
            {
                switch (visitorType)
                {
                    case 2: //contractor
                    case 4: //adhoc
                        if (DateTime.Now.TimeOfDay >= new TimeSpan(23, 0, 0))
                        {
                            startDate = DateTime.Now.ToString("yyyy/MM/dd");
                            startTime = "23:00";
                            endDate = DateTime.Now.AddDays(1).ToString("yyyy/MM/dd");
                            endTime = "07:00";
                        }
                        else
                        {
                            // assuming it is before 7am so it will create based on yesterday night till this morning
                            startDate = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                            startTime = "23:00";
                            endDate = DateTime.Now.ToString("yyyy/MM/dd");
                            endTime = "07:00";
                        }
                        break;

                    case 3: //delivery fixed to 2 hours 
                        startDate = DateTime.Now.ToString("yyyy/MM/dd");
                        startTime = DateTime.Now.ToString("HH:mm") ;
                        endDate = DateTime.Now.AddHours(2).ToString("yyyy/MM/dd");
                        endTime = DateTime.Now.AddHours(2).ToString("HH:mm");
                        break;
                }
            }

            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        CardNumber = cardNumber.ToString().PadLeft(6, '0'),
                        CardHolderName = string.IsNullOrWhiteSpace(name) ? $"Kiosk {cardNumber}" : name,
                        CardStartValidDate = startDate,
                        StartTime = startTime,
                        CardExpiryDate = endDate,
                        EndTime = endTime,
                        IsVisitor = true
                    };

                    IRestResponse restResponse = GenericRestAPICall("/pvcardaccess/addcard", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                    {
                        boschResponse = JsonConvert.DeserializeObject<BoschResponse>(restResponse.Content);
                        if (!boschResponse.IsSucceed)
                            returnMessage = boschResponse.Remarks;
                    }
                }
                catch (Exception ex)
                {
                    APIMessage apiMessage = ErrorMessageHelper.Instance.GetLocalServerError();
                    returnMessage = apiMessage.Message;
                    ilog.LogError("Exception", ex);
                }
            });
            return returnMessage;
        }
    }
}
