﻿using Kiosk.Model;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Windows;
using System.Windows.Media;

namespace Kiosk.Utility
{
    public class NavigationHelper
    {
        private static readonly Lazy<NavigationHelper> lazy = new Lazy<NavigationHelper>(() => new NavigationHelper());
        public static NavigationHelper Instance { get { return lazy.Value; } }

        private NavigationData NavData;
        private MainWindow mainWindow;
        public int SelectedGroup { get; set; }
        private int CurrentPageCounter = 0;

        public void LoadPages(MainWindow mainWindow, string jsonString)
        {
            this.mainWindow = mainWindow;
            NavData = JsonConvert.DeserializeObject<NavigationData>(jsonString);

            if (string.IsNullOrWhiteSpace(NavData.MainPage) || NavData.NavigationGroups == null || NavData.NavigationGroups.Count == 0)
                throw new Exception("Invalid Navigation.json file");
        }

        public Navigate GetMainPageEnum()
        {
            return GetPageEnum(NavData.MainPage);
        }
        public NavigationPage GetSpecialNavigationPage(string pageName)
        {
            return NavData.GetSpecialPage(pageName);
        }

        public bool NavigateToNext()
        {
            NavigationPage navigationPage = NavData.NavigateToNext(SelectedGroup, CurrentPageCounter);
            if (navigationPage != null)
            {
                CurrentPageCounter++;
                Navigate navigateTo = GetPageEnum(navigationPage.Page);
                mainWindow.NavigateContentPage(navigateTo, navigationPage);
                return true;
            }

            return false;
        }
        //public bool NavigateToBack()
        //{
        //    NavigationPage navigationPage = NavData.NavigateToBack(SelectedGroup, CurrentPageCounter);
        //    if (navigationPage != null)
        //    {
        //        CurrentPageCounter--;
        //        Navigate navigateTo = GetPageEnum(navigationPage.Page);
        //        mainWindow.NavigateContentPage(navigateTo, navigationPage);
        //        return true;
        //    }

        //    return false;
        //}
        public bool NavigateToMain()
        {
            return NavigateToSelectedMain(0); // change to hardcoded
        }
        public bool NavigateToMessageBeforeMain()
        {
            NavigationPage navigationPage = GetSpecialNavigationPage(Navigate.TimeoutMessage.ToString()); ;
            if (navigationPage != null)
            {
                mainWindow.NavigateContentPage(Navigate.TimeoutMessage, navigationPage);
                return true;
            }
            else
                return NavigateToMain();
        }
        public bool NavigateToSelectedMain(int groupIndex)
        {
            SelectedGroup = groupIndex;
            NavigationPage navigationPage = NavData.NavigateToMain(SelectedGroup);
            if (navigationPage != null)
            {
                CurrentPageCounter = 0;
                Navigate navigateTo = GetPageEnum(navigationPage.Page);
                mainWindow.NavigateContentPage(navigateTo, navigationPage);
                return true;
            }
            return false;
        }

        public bool NavigateToPage(Navigate navigate)
        {
            mainWindow.NavigateContentPage(navigate, null);
            return true;
        }

        public bool NavigateToError(string errorMessage, string errorTitle = "", bool hideButton = false)
        {
            NavigationPage navigationPage = new NavigationPage();
            navigationPage.Titles = new List<string>() { errorTitle };
            navigationPage.Messages = new List<string>() { errorMessage };
            mainWindow.NavigateContentPage(Navigate.ErrorMessage, navigationPage, hideButton);
            return true;
        }

        public enum Navigate
        {
            MainPage,   //visitor, delivery
            MainPage2,  //contractor, delivery
            MainPage3,
            MainPage4,//visitor,delivery,contractor
            StaffAttendance,
            PDPAAcknowledgement,
            EnterCompanyName,
            EnterUnitNumber,
            EnterName,
            EnterContactNumber,
            EnterIdentityManual,
            SelectLocation,
            VisitSummary,
            ErrorMessage,
            ScanIdentityCard,
            TakePhoto,
            EnterPin,
            Back,
            NA,
            BackToMain,

            // added for fraser
            AdminMain,
            ScanIdentityCardBarcode, 
            ScanIdentityCardBarcodeNoRescan,

            EnterLevel,
            EnterUnit,

            ScanPTWBarcodeNoRescan,
            ShowPTWInfo,

            ExistingWorkerInfo,
            WorkerListMain,
            WorkerListVisitSummary,
            AddScanIdentityCardBarcodeNoRescan,
            AddEnterName,
            AddEnterContactNumber,
            AddTakePhoto,

            WorkerListMainAdhoc,
            FraserVisitSummary,

            // added for jtc
            ScanIdentityCardJTC,
            VisitSummaryJTC,
            GenericMessage,
            TimeoutMessage,
            WorkerListVisitSummaryJTC,

            // added for concierge
            MainPageConcierge,
            SignInConcierge,
            SignOutConcierge,
            SignInHistory
        }

        public Navigate GetPageEnum(string pageString)
        {
            Navigate navigation;
            System.Enum.TryParse(pageString, out navigation);

            return navigation;
        }

        //public MainWindow GetParentObject(DependencyObject control)
        //{
        //    return mainWindow;
        //}

        public void HideLoadingPanel()
        {
            mainWindow.HidePanelLoading();
        }

        public void ShowLoadingPanel(string loadingText = "")
        {
            mainWindow.ShowPanelLoading(loadingText);
        }

        public string GetGenericMessage(string key)
        {
            return NavData.GetGenericMessage(key);
        }
        public void CloseApplication()
        {
            mainWindow.CloseApplication();
        }
    }
}
