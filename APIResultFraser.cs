﻿
namespace Kiosk.Model.API.Response
{
    public class APIResultFraser
    {
        public bool IsSuccess { get; set; }
        public string ServerErrorMessage { get; set; }
        public string ServerErrorCode { get; set; }
        public APIResultVisitor Data { get; set; }
    }
}
