﻿using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class AdminMain : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        private System.Windows.Threading.DispatcherTimer initialTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;
        private int CloseScreenTimerCounter = 5;


        private int indexStart = 0;
        public AdminMain(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle1.Text = navigationPage.GetTitle(0);
            lblTitle2.Text = navigationPage.GetTitle(1);

            initialTimer = new System.Windows.Threading.DispatcherTimer();
            initialTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            CloseScreenTimerCounter = ConfigurationStore.Instance.CloseScreenTimer * 2;

            lblMessage.Text = "";

            if (ConfigurationStore.Instance.KioskLocation == "Fraser")
                AdhocPTWStackPanelLabel.Content = "Adhoc GWP";
            else
                AdhocPTWStackPanelLabel.Content = "Adhoc PTW";

            if (!navigationPage.CheckIfFeatureEnabled(btnAddWorkerByPTW.Tag.ToString()))
                btnAddWorkerByPTW.Visibility = Visibility.Collapsed;
            else
                indexStart++;

            if (!navigationPage.CheckIfFeatureEnabled(btnAdhoc.Tag.ToString()))
                btnAdhoc.Visibility = Visibility.Collapsed;

            if (!navigationPage.CheckIfFeatureEnabled(btnShutdown.Tag.ToString()))
                btnShutdown.Visibility = Visibility.Collapsed;


            StartInitialTimer();


            NavigationHelper.Instance.HideLoadingPanel();
        }

        #region Buttons
        private void AddWorkerStackPanel_Click(object sender, RoutedEventArgs e)
        {
            kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Contractor;
            NavigationHelper.Instance.SelectedGroup = ConfigurationStore.Instance.AdminGroupStartIndex;
            NavigationHelper.Instance.NavigateToNext();

            // scan existing ID
            // scan New ID
            // enter contact
            // visit summary
                // same as first ID
        }

        private void AdhocPTWStackPanel_Click(object sender, RoutedEventArgs e)
        {
            kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Adhoc;
            NavigationHelper.Instance.SelectedGroup = ConfigurationStore.Instance.AdminGroupStartIndex + indexStart;
            NavigationHelper.Instance.NavigateToNext();

            // enter ptw contact
            // enter ptw name 
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopInitialTimer();
            StopCloseScreenTimer();
        }

        private int initialTimerCounter = 1;
        private void initialTimer_Tick(object sender, EventArgs args)
        {
            if (initialTimerCounter > CloseScreenTimerCounter)
            {
                StopInitialTimer();
                StartCloseScreenTimer();
            }
            initialTimerCounter++;
            CommandManager.InvalidateRequerySuggested();
        }

        private int countDownCounter = 0;
        private object _lock = new object();
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();

            if (countDownCounter < 0)
            {
                //ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigationHelper.Instance.NavigateToMessageBeforeMain();
            }
        }

        private void StartInitialTimer()
        {
            initialTimerCounter = 1;
            //lblMessage.Text = navigationPage.GetMessages(0);
            lblCountdown.Content = "";
            initialTimer.Tick -= initialTimer_Tick;
            initialTimer.Tick += initialTimer_Tick;
            initialTimer.Start();
        }
        private void StartCloseScreenTimer()
        {
            countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
            lblMessage.Text = navigationPage.GetMessages(0);
            lblCountdown.Content = countDownCounter.ToString();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
            closeScreenTimer.Tick += closeScreenTimer_Tick;
            closeScreenTimer.Start();
        }
        private void StopInitialTimer()
        {
            lock (_lock)
            {
                initialTimer.Stop();
                initialTimer.Tick -= initialTimer_Tick;
            }
        }
        private void StopCloseScreenTimer()
        {
            lock (_lock)
            {
                closeScreenTimer.Stop();
                closeScreenTimer.Tick -= closeScreenTimer_Tick;
            }
        }

        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void RefreshNoActivityTimer()
        {
            lock (_lock)
            {
                if (initialTimer.IsEnabled)
                    initialTimerCounter = 1;
                else
                {
                    if (closeScreenTimer.IsEnabled)
                    {
                        StopCloseScreenTimer();
                        if (!initialTimer.IsEnabled)
                            StartInitialTimer();
                    }
                }
            }
        }


        private void ShutdownStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.CloseApplication();
        }

        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }
        #endregion
    }
}
