﻿using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Kiosk.Utility.API;

namespace Kiosk.ContentPage
{
    public partial class TakePhoto : UserControl
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        private System.Windows.Threading.DispatcherTimer showphotoTimer;
        private System.Windows.Threading.DispatcherTimer countdownTimer;
        private System.Windows.Threading.DispatcherTimer confirmTimer;
        private int countDownCounter;

        private BitmapImage imageFromWebcam;

        private WebcamHelper webcamHelper;
        private FRApiHelper _frApiHelper;

        public TakePhoto(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
            _frApiHelper = new FRApiHelper(ilog, ConfigurationStore.Instance.GMSUrl, ConfigurationStore.Instance.APICallTimeout);
        }

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblMessage.Text = "";

            lblCountdown.Content = "";

            showphotoTimer = new System.Windows.Threading.DispatcherTimer();
            showphotoTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);

            countdownTimer = new System.Windows.Threading.DispatcherTimer();
            countdownTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000);

            confirmTimer = new System.Windows.Threading.DispatcherTimer();
            confirmTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000);

            //imgCamera.Visibility = Visibility.Collapsed;

            //webcamHelper = new WebcamHelper(ConfigurationStore.Instance.KioskFilePath, ConfigurationStore.Instance.CameraImageRotateDegree);
            //webcamHelper.StartCamera();

            //showphotoTimer = new System.Windows.Threading.DispatcherTimer();
            //showphotoTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);

            //countdownTimer = new System.Windows.Threading.DispatcherTimer();
            //countdownTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000);

            //confirmTimer = new System.Windows.Threading.DispatcherTimer();
            //confirmTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000);

            //StartCapturingPhoto();

            //NavigationHelper.Instance.HideLoadingPanel();


            string errorMessage = "";
            try
            {
                webcamHelper = new WebcamHelper(ConfigurationStore.Instance.KioskFilePath, ConfigurationStore.Instance.CameraImageRotateDegree);
                webcamHelper.StartCamera();

                // delay here so camera is loaded before user press buttons
                for (int i = 0; i < 10; i++)
                {
                    if (webcamHelper.HasInitialImage)
                        break;

                    await System.Threading.Tasks.Task.Delay(500);
                }
                if (webcamHelper.HasInitialImage)
                    SetPhotoFromWebCam();
            }
            catch (Exception ex)
            {
                // camera dead
                ilog.LogError("AddTakePhoto error", ex);
                errorMessage = NavigationHelper.Instance.GetGenericMessage("WebCamError");
            }

            if (string.IsNullOrWhiteSpace(errorMessage))
            {
                StartCapturingPhoto();

                NavigationHelper.Instance.HideLoadingPanel();
            }
            else
                NavigationHelper.Instance.NavigateToError(errorMessage);
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            webcamHelper.StopCamera();
            StopShowphotoTimer();
            StopCountdownTimer();
            StopConfirmTimer();
        }

        private void StartCapturingPhoto()
        {
            countDownCounter = ConfigurationStore.Instance.CountdownTimer;
            lblCountdown.Content = countDownCounter;
            lblMessage.Text = navigationPage.GetMessages(0);

            grdCancel.Visibility = Visibility.Visible;
            grdRetake.Visibility = Visibility.Collapsed;

            showphotoTimer.Tick += new EventHandler(showphotoTimer_Tick);
            showphotoTimer.Start();

            countdownTimer.Tick += new EventHandler(countdownTimer_Tick);
            countdownTimer.Start();

            confirmTimer.Tick += new EventHandler(confirmTimer_Tick);
        }

        private async Task StopCapturingPhoto()
        {
            StopShowphotoTimer();
            StopCountdownTimer();

            countDownCounter = ConfigurationStore.Instance.CountdownTimer;
            lblCountdown.Content = countDownCounter;

            lblMessage.Text = navigationPage.GetMessages(1);
            grdCancel.Visibility = Visibility.Collapsed;
            grdRetake.Visibility = Visibility.Visible;

            confirmTimer.Start();



            var p = BitmapToBytes(kioskVisitor.Photo);
            var p64 = Convert.ToBase64String(p);
            var ret1 = await _frApiHelper.FaceCrop(p64);
            if (!string.IsNullOrWhiteSpace(ret1.base64))
            {
                var bytes = Convert.FromBase64String(ret1.base64);

                //imageFromWebcam = ToImage(bytes);
                webcamHelper.Image = ToImage(bytes);
                kioskVisitor.Photo = ToImage(bytes);
                SetPhotoFromWebCam();
            }
            else
            {
                ShowMessage("Failed to detect face on photo.");
            }
            
        }

        private void showphotoTimer_Tick(object sender, EventArgs e)
        {
            SetPhotoFromWebCam();
        }

        private void SetPhotoFromWebCam()
        {
            imageFromWebcam = webcamHelper.Image;
            imgCamera.Source = imageFromWebcam;
        }

        private void countdownTimer_Tick(object sender, EventArgs e)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter;

            if (countDownCounter < 0)
                StopCapturingPhoto();

            CommandManager.InvalidateRequerySuggested();
        }

        private void confirmTimer_Tick(object sender, EventArgs e)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter;

            if (countDownCounter < 0)
            {
                StopConfirmTimer();
                NavigateToNextScreen();
            }

            CommandManager.InvalidateRequerySuggested();




        }

        private void StopShowphotoTimer()
        {
            showphotoTimer.Stop();
            showphotoTimer.Tick -= showphotoTimer_Tick;
        }
        private void StopCountdownTimer()
        {
            countdownTimer.Stop();
            countdownTimer.Tick -= countdownTimer_Tick;
        }
        private void StopConfirmTimer()
        {
            confirmTimer.Stop();
            confirmTimer.Tick -= confirmTimer_Tick;
        }

        private async Task NavigateToNextScreen()
        {
            // save photo to local disk (required for printing sticker)
            try
            {
                //SaveImage(imageFromWebcam);       // no need to save here, save before print
                kioskVisitor.Photo = imageFromWebcam;
            }
            catch (Exception ex)
            {
                ilog.LogError("Error Save image", ex);
            }

            if (ConfigurationStore.Instance.FR_CheckQuality)
            {
                NavigationHelper.Instance.ShowLoadingPanel("Checking photo quality...");
                try
                {
                    var photo1 = BitmapToBytes(kioskVisitor.Photo);
                    var photo64 = Convert.ToBase64String(photo1);

                    var ret = await _frApiHelper.FaceQuality(photo64);
                    if (!ret.IsSuccess)
                    {
                        ShowMessage("Photo quality is low. please try again.");
                        NavigationHelper.Instance.HideLoadingPanel();
                        return;
                    }
                }
                finally
                {
                    // NavigationHelper.Instance.HideLoadingPanel();
                }

            }

            if (ConfigurationStore.Instance.FR_OneToOneEnabled &&
                ConfigurationStore.Instance.FR_OneToOneThredshold > 0 &&
                kioskVisitor.IDPhoto != null && kioskVisitor.IDPhoto.Length > 0)
            {
                NavigationHelper.Instance.ShowLoadingPanel("Verifying ID and photo...");
                try
                {
                    var photo1 = BitmapToBytes(kioskVisitor.Photo);
                    var photo64_1 = Convert.ToBase64String(photo1);
                    var photo64_2 = Convert.ToBase64String(kioskVisitor.IDPhoto);

                    var ret = await _frApiHelper.FaceOneToOne(photo64_1, photo64_2);
                    var score = ret.score;
                    if (score < ConfigurationStore.Instance.FR_OneToOneThredshold)
                    {
                        NavigationHelper.Instance.HideLoadingPanel();
                        ShowMessage("ID and Face mismatch. please try again.");
                        return;
                    }
                    else
                    {
                        // ilog.LogInfo($"1:1 passed. move next page 1");
                        NavigationHelper.Instance.NavigateToNext();
                        // ilog.LogInfo($"1:1 passed. move next page 2");
                    }
                }
                finally
                {
                    //NavigationHelper.Instance.HideLoadingPanel();
                }

            }



            NavigationHelper.Instance.NavigateToNext();


        }

        public BitmapImage ToImage(byte[] array)
        {
            using (var ms = new System.IO.MemoryStream(array))
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad; // here
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
        }
        private void ShowMessage(string message)
        {
            this.Dispatcher.Invoke(() =>
            {
                lblMessage.Text = message;
            });
        }

        private byte[] BitmapToBytes(BitmapImage bitmapImage)
        {
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }

        //private void SaveImage(BitmapImage image)
        //{
        //    BitmapEncoder encoder = new PngBitmapEncoder();
        //    encoder.Frames.Add(BitmapFrame.Create(image));

        //    using (var fileStream = new System.IO.FileStream(ConfigurationStore.Instance.KioskFilePath +
        //                                           ConfigurationStore.Instance.TempVisitorPhotoFileName, System.IO.FileMode.Create))
        //    {
        //        try
        //        {
        //            encoder.Save(fileStream);
        //        }
        //        catch (Exception ex)
        //        {
        //            ilog.LogError("Exception", ex);
        //        }
        //    }
        //}

        #region Buttons
        private void RetakeStackPanel_Click(object sender, RoutedEventArgs e)
        {
            StopConfirmTimer();

            StartCapturingPhoto();
        }
        private void NextStackPanel_Click(object sender, RoutedEventArgs e)
        {
            StopConfirmTimer();
            NavigateToNextScreen();
        }
        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }
        private void OKStackPanel_Click(object sender, RoutedEventArgs e)
        {
            StopCapturingPhoto();




            //if (string.IsNullOrWhiteSpace(ret1.base64))
            //{
            //    ShowMessage("No face detected in camera. please try again.");
            //    return;
            //}
            //else
            //{
            //    var bytes = Convert.FromBase64String(ret1.base64);
            //    //imageFromWebcam = ToImage(bytes);
            //    webcamHelper.Image = ToImage(bytes);
            //    kioskVisitor.Photo = ToImage(bytes);
            //    SetPhotoFromWebCam();
            //    // StopCapturingPhoto();
            //}

        }
        #endregion
    }
}
