﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Kiosk.Utility
{
    public static class ColourChanger
    {
        public static void ChangeLabelColourDefault(Label label)
        {
            BrushConverter bc = new BrushConverter();
            label.Foreground = (Brush)bc.ConvertFrom($"#{ConfigurationStore.Instance.ButtonTextColour}");
        }
        public static void ChangeLabelColourOnEnter(Label label)
        {
            BrushConverter bc = new BrushConverter();
            label.Foreground = (Brush)bc.ConvertFrom($"#{ConfigurationStore.Instance.ButtonTextColourOnEnter}");
        }
        public static void ChangeImage(Image image, string path)
        {
            Uri uriSource = new Uri("/Kiosk;component" + path, UriKind.Relative);
            image.Source = new BitmapImage(uriSource);
        }


    }
}
