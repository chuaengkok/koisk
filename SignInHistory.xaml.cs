﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using Kiosk.Utility;
using Kiosk.Utility.API;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class SignInHistory : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;

        private List<int> hourList = new List<int>();
        private List<int> minuteList = new List<int>();

        public SignInHistory(Utility.Log.Ilog ilog, NavigationPage navigationPage)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;

            for (int i = 0; i < 24; i++)
                hourList.Add(i);

            for (int i = 0; i < 60; i = i + 5)
                minuteList.Add(i);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblMessage.Text = navigationPage.GetMessages(0);

            dtpDateFrom.DisplayDateStart = DateTime.Now.AddDays(-35);
            dtpDateFrom.DisplayDateEnd = DateTime.Today;

            dtpDateTo.DisplayDateStart = DateTime.Now.AddDays(-35);
            dtpDateTo.DisplayDateEnd = DateTime.Today;

            cbbDateFromHour.ItemsSource = hourList;
            cbbDateFromMinutes.ItemsSource = minuteList;

            cbbDateToHour.ItemsSource = hourList;
            cbbDateToMinutes.ItemsSource = minuteList;

            ResetPage();


            NavigationHelper.Instance.HideLoadingPanel();
        }

        private async void SearchVisitor()
        {
            dtgResult.ItemsSource = null;
            dtgResult.Visibility = Visibility.Hidden;

            DateTime dateFrom = dtpDateFrom.SelectedDate.HasValue ? dtpDateFrom.SelectedDate.Value : DateTime.MinValue;
            DateTime dateTo = dtpDateTo.SelectedDate.HasValue ? dtpDateTo.SelectedDate.Value : DateTime.MinValue;

            if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue )
            {
                dateFrom = dateFrom.AddHours((int)cbbDateFromHour.SelectedItem).AddMinutes((int)cbbDateFromMinutes.SelectedItem);
                dateTo = dateTo.AddHours((int)cbbDateToHour.SelectedItem).AddMinutes((int)cbbDateToMinutes.SelectedItem);

                if (dateTo > dateFrom)
                {

                    JTCAPIHelper jtcAPIHelper = new JTCAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                    APIResultSignInHistory apiResult = await jtcAPIHelper.GetSignInHistory(ConfigurationStore.Instance.LocationID, dateFrom, dateTo);

                    if (apiResult.IsSuccess)
                    {
                        if (apiResult.Data.Count > 0)
                        {
                            dtgResult.ItemsSource = apiResult.Data;
                            dtgResult.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            lblMessage.Text = navigationPage.GetMessages(2);
                        }
                    }
                    else
                    {
                        NavigationHelper.Instance.NavigateToError(apiResult.ServerErrorMessage);
                    }
                }
                else
                    lblMessage.Text = navigationPage.GetMessages(3);
            }
            else
                lblMessage.Text = navigationPage.GetMessages(1);

        }
        private void ResetPage()
        {

            //set default
            dtpDateFrom.SelectedDate = DateTime.Today;
            dtpDateTo.SelectedDate = DateTime.Today;

            cbbDateFromHour.SelectedItem = 7;
            cbbDateFromMinutes.SelectedItem = 0;

            cbbDateToHour.SelectedItem = 22;
            cbbDateToMinutes.SelectedItem = 0;

            dtgResult.ItemsSource = null;
            dtgResult.Visibility = Visibility.Hidden;
        }


        #region Buttons
        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }
        private void SearchStackPanel_Click(object sender, RoutedEventArgs e)
        {
            SearchVisitor();
        }
        private void ResetStackPanel_Click(object sender, RoutedEventArgs e)
        {
            ResetPage();
        }

        #endregion

    }
}
