﻿using Kiosk.Model;
using Kiosk.Utility;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class MainPage2 : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        public MainPage2(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle1.Text = navigationPage.GetTitle(0);
            lblTitle2.Text = navigationPage.GetTitle(1);

            lblMessage.Text = navigationPage.GetMessages(0);

            NavigationHelper.Instance.HideLoadingPanel();
        }

        #region Buttons
        private void ContractorStackPanel_Click(object sender, RoutedEventArgs e)
        {
            kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Contractor;
            NavigationHelper.Instance.SelectedGroup = 0;
            NavigationHelper.Instance.NavigateToNext();
        }

        private void DeliveryStackPanel_Click(object sender, RoutedEventArgs e)
        {
            kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Delivery;
            NavigationHelper.Instance.SelectedGroup = 1;
            NavigationHelper.Instance.NavigateToNext();
        }
        #endregion
    }
}
