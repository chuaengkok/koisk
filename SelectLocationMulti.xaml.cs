﻿using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class SelectLocationMulti : UserControl
    {
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;
        private List<Model.API.Response.Location> locationList;
        private List<Model.Siemens.AccessGroup> accessGroups;

        private System.Windows.Threading.DispatcherTimer initialTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;

        private int CloseScreenTimerCounter = 5;
        public SelectLocationMulti(NavigationPage navigationPage, KioskVisitor kioskVisitor, 
            List<Model.API.Response.Location> locationList, List<Model.Siemens.AccessGroup> accessGroups)
        {
            InitializeComponent();

            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
            this.locationList = locationList;
            this.accessGroups = accessGroups;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Content = navigationPage.GetTitle(0);
            lblMessage.Content = "";
            
            initialTimer = new System.Windows.Threading.DispatcherTimer();
            initialTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            CloseScreenTimerCounter = ConfigurationStore.Instance.CloseScreenTimer;

            StartInitialTimer();

            NavigationHelper.Instance.HideLoadingPanel();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopInitialTimer();
            StopCloseScreenTimer();
        }

        private int initialTimerCounter = 1;
        private void initialTimer_Tick(object sender, EventArgs args)
        {
            if (initialTimerCounter > CloseScreenTimerCounter)
            {
                StopInitialTimer();
                StartCloseScreenTimer();
            }
            initialTimerCounter++;
            CommandManager.InvalidateRequerySuggested();
        }

        private int countDownCounter = 0;
        private object _lock = new object();
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();
            
            if (countDownCounter < 0)
            {
                //log.Info("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigateToMainScreen();
            }
        }

        private void StartInitialTimer()
        {
            initialTimerCounter = 1;
            lblMessage.Content = "";
            lblCountdown.Content = "";
            initialTimer.Tick -= initialTimer_Tick;
            initialTimer.Tick += initialTimer_Tick;
            initialTimer.Start();
        }
        private void StartCloseScreenTimer()
        {
            countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
            lblMessage.Content = navigationPage.GetMessages(2);
            lblCountdown.Content = countDownCounter.ToString();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
            closeScreenTimer.Tick += closeScreenTimer_Tick;
            closeScreenTimer.Start();
        }
        private void StopInitialTimer()
        {
            lock (_lock)
            {
                initialTimer.Stop();
                initialTimer.Tick -= initialTimer_Tick;
            }
        }
        private void StopCloseScreenTimer()
        {
            lock (_lock)
            {
                closeScreenTimer.Stop();
                closeScreenTimer.Tick -= closeScreenTimer_Tick;
            }
        }

        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void RefreshNoActivityTimer()
        {
            lock (_lock)
            {
                if (initialTimer.IsEnabled)
                    initialTimerCounter = 1;
                else
                {
                    if (closeScreenTimer.IsEnabled)
                    {
                        StopCloseScreenTimer();
                        if (!initialTimer.IsEnabled)
                            StartInitialTimer();
                    }
                }
            }
        }

        #region Buttons
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();
            lblMessage.Content = "";

            Button button = (Button)sender;
            string value = button.CommandParameter.ToString();
            switch (value.ToUpper())
            {
                case "BACK":
                    if (!string.IsNullOrWhiteSpace(txtInput.Text))
                        txtInput.Text = txtInput.Text.Remove(txtInput.Text.Length - 1);
                    break;
                case "C":
                    txtInput.Text = "";
                    lblMessage.Content = "";
                    break;
                default:
                    if (txtInput.Text.Length < 2)
                        txtInput.Text = txtInput.Text + value;
                    break;
            }
        }
        
        private void BackStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void NextStackPanel_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();
            if (!string.IsNullOrEmpty(txtInput.Text))
            {
                int selectedLevelInt = 0;
                int.TryParse(txtInput.Text, out selectedLevelInt);

                if (ConfigurationStore.Instance.KioskLocation == "OGS")
                {
                    string siemensLocationName = $"{ConfigurationStore.Instance.KioskSubLocation} Visitor {txtInput.Text.PadLeft(2, '0')}";

                    Model.Siemens.AccessGroup accessGroup = accessGroups.FirstOrDefault(i => i.Name.Equals(siemensLocationName, StringComparison.InvariantCultureIgnoreCase));
                    switch (ConfigurationStore.Instance.KioskSubLocation)
                    {
                        case "A": // lobby A
                                  // A Visitor 15
                                  // A Visitor 23
                            if (accessGroup == null)
                            {
                                txtInput.Text = "";
                                lblMessage.Content = navigationPage.GetMessages(5);
                                return;
                            }
                            break;
                        case "B": // lobby B
                                  // B Visitor 07
                                  // B Visitor 15
                                  // Level 8, 9 and 11 are restricted
                            if (accessGroup == null)
                            {
                                txtInput.Text = "";
                                lblMessage.Content = navigationPage.GetMessages(4);
                                return;
                            }
                            else if (selectedLevelInt == 8 || selectedLevelInt == 9 || selectedLevelInt == 11)
                            {
                                NavigationHelper.Instance.NavigateToError(navigationPage.GetMessages(3));
                                return;
                            }

                            break;
                    }

                    kioskVisitor.LocationName = siemensLocationName;
                    NavigationHelper.Instance.NavigateToNext();
                }
                else
                {
                    Model.API.Response.Location location = locationList.Find(l => l.GetLevel() == selectedLevelInt);
                    if (location != null)
                    {
                        kioskVisitor.LocationID = location.ID.ToString();
                        kioskVisitor.LocationName = location.Name;

                        NavigationHelper.Instance.NavigateToNext();
                    }
                    else
                    {
                        txtInput.Text = "";
                        lblMessage.Content = navigationPage.GetMessages(1);

                    }
                }
            }
            else
                lblMessage.Content = navigationPage.GetMessages(0);

        }
        #endregion
    }
}
