﻿using System;
using System.Windows.Media.Imaging;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Kiosk.Utility
{
    public class WebcamHelper
    {
        //private static readonly Lazy<WebcamHelper> lazy = new Lazy<WebcamHelper>(() => new WebcamHelper());
        //public static WebcamHelper Instance { get { return lazy.Value; } }
        
        public BitmapImage Image { get; set; }
        public FilterInfo CurrentDevice { get; set; }

        public bool HasInitialImage { get; set; }

        private IVideoSource _videoSource;

        private string folderPath;
        private string photoRotateDegree;

        //private System.Windows.Threading.DispatcherTimer initialTimer;
        //private int initialTimerCounter;

        //private WebcamHelper()
        //{
        //}

        public WebcamHelper(string folderPath, string photoRotateDegree)
        {
            this.folderPath = folderPath;
            this.photoRotateDegree = photoRotateDegree;
        }

        private void GetVideoDevices()
        {
            FilterInfoCollection devices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (devices.Count > 0)
                CurrentDevice = devices[0];
        }

        public void StartCamera()
        {
            GetVideoDevices();
            if (CurrentDevice != null)
            {
                _videoSource = new VideoCaptureDevice(CurrentDevice.MonikerString);
                _videoSource.NewFrame += video_NewFrame;
                _videoSource.Start();

                HasInitialImage = false;

                //initialTimer = new System.Windows.Threading.DispatcherTimer();
                //initialTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
                //initialTimerCounter = 0;
                //initialTimer.Tick -= initialTimer_Tick;
                //initialTimer.Tick += initialTimer_Tick;
                //initialTimer.Start();
            }
            else
            {
                throw new Exception("Camera not found");
            }
        }

        //private void initialTimer_Tick(object sender, EventArgs args)
        //{
        //    initialTimerCounter++;
        //}


        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
                //if (initialTimerCounter % 2 == 0) // limit the webcam to do 1 video process per second
                //{
                    //Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss.fff")} {initialTimerCounter}");
                    using (var bitmap = (Bitmap)eventArgs.Frame.Clone())
                    {
                        //RotateImage(bitmap, this.photoRotateDegree);

                        //using (Bitmap newBitmap = DrawBox(bitmap))
                        //{
                            BitmapImage bi = ToBitmapImageOld(bitmap, ImageFormat.Bmp, this.photoRotateDegree);

                            //BitmapImage bi = await ToBitmapImage(bitmap, ImageFormat.Bmp); // hardcode this part first

                            bi.Freeze();
                            Image = bi;

                    HasInitialImage = true;
                       // }
                    }
               // }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void StopCamera()
        {
            if (_videoSource != null && _videoSource.IsRunning)
            {
                _videoSource.SignalToStop();
                _videoSource.NewFrame -= video_NewFrame;

            }


            //initialTimer.Stop();
            //initialTimer.Tick -= initialTimer_Tick;

            //Image = null; ?? may not require

            Dispose();
        }

        private void Dispose()
        {
            if (_videoSource != null && _videoSource.IsRunning)
            {
                _videoSource.SignalToStop();
            }
        }

        private BitmapImage ToBitmapImageOld(Bitmap bitmap, ImageFormat imageformat, string rotateDegree) //, bool rotateImage, string rotateDegree)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, imageformat);
            ms.Seek(0, SeekOrigin.Begin);
            bi.StreamSource = ms;


            switch (rotateDegree)
            {
                case "90":
                    bi.Rotation = Rotation.Rotate90;
                    break;
                case "180":
                    bi.Rotation = Rotation.Rotate180;
                    break;
                case "270":
                    bi.Rotation = Rotation.Rotate270;
                    break;
            }


            bi.EndInit();
            return bi;
        }

        /*
        private void RotateImage(Bitmap bitmap, string rotateDegree)
        {
            switch (rotateDegree)
            {
                case "90":
                    bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    break;
                case "180":
                    bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                case "270":
                    bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    break;
            }
            
        }

        private Bitmap DrawBox(Bitmap bitmap)
        {


            using (MemoryStream ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Bmp);
                byte[] array = ms.ToArray();

                var data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                                                ImageLockMode.ReadOnly, bitmap.PixelFormat);

                using (var detector = Dlib.GetFrontalFaceDetector())
                {
                    using (var cimg = Dlib.LoadImageData<RgbPixel>(array, (uint)data.Height, (uint)data.Width, (uint)data.Stride))
                    {
                        // Detect faces 
                        var faces = detector.Operator(cimg);
                        foreach (var face in faces)
                        {
                            // draw a rectangle for each face
                            Dlib.DrawRectangle(cimg, face, color: new RgbPixel(0, 255, 255), thickness: 4);

                        }
                        return cimg.ToBitmap();
                    }
                }


                //DlibDotNet.Rectangle[] faces = detector.Operator(cimg);

                //List<FullObjectDetection> shapes = new List<FullObjectDetection>();

                //foreach (DlibDotNet.Rectangle r in faces)
                //{
                //    FullObjectDetection det = poseModel.Detect(cimg, r);
                //    shapes.Add(det);

                //    Dlib.DrawRectangle(cimg, r, new RgbPixel { Green = 255 });
                //}

                //Bitmap result = cimg.ToBitmap(); // The Place Error Occured.
                //pictureBox.Invoke(new Action(() =>
                //{
                //    pictureBox.Image?.Dispose();
                //    pictureBox.Image = result;
                //}));
            }

            return bitmap;
        }

    

        private async Task<BitmapImage> ToBitmapImage(Bitmap bitmap, ImageFormat imageformat) //, bool rotateImage, string rotateDegree)
        {
            //BitmapImage bi = new BitmapImage();
            //bi.BeginInit();
            //MemoryStream ms = new MemoryStream();
            //bitmap.Save(ms, imageformat);
            //ms.Seek(0, SeekOrigin.Begin);
            //bi.StreamSource = ms;

            //if (rotateImage)
            //{
            //    switch (rotateDegree)
            //    {
            //        case "90":
            //            bi.Rotation = Rotation.Rotate90;
            //            break;
            //        case "180":
            //            bi.Rotation = Rotation.Rotate180;
            //            break;
            //        case "270":
            //            bi.Rotation = Rotation.Rotate270;
            //            break;
            //    }
            //}
            WriteableBitmap result = null;

            await Task.Run(() =>
            {
                using (var faceDetector = Dlib.GetFrontalFaceDetector())
                using (MemoryStream data = new MemoryStream())
                {
                    bitmap.Save(data, imageformat);
                    data.Seek(0, SeekOrigin.Begin);
                // DlibDotNet can create Array2D from file but this sample demonstrate
                // converting managed image class to dlib class and vice versa.
                WriteableBitmap writeableBitmap = new WriteableBitmap(BitmapFrame.Create(data));
                    using (Array2D<RgbPixel> image = writeableBitmap.ToArray2D<RgbPixel>())
                    {
                        var dets = faceDetector.Operator(image);
                        foreach (var r in dets)
                            Dlib.DrawRectangle(image, r, new RgbPixel { Green = 255 });

                        result = image.ToWriteableBitmap();
                        //if (result.CanFreeze)
                        //    result.Freeze();
                        
                    }
                }
        });

            return ConvertWriteableBitmapToBitmapImage(result);

            //bi.EndInit();
            //return bi;
        }

        public BitmapImage ConvertWriteableBitmapToBitmapImage(WriteableBitmap wbm)
        {
            BitmapImage bmImage = new BitmapImage();
            using (MemoryStream stream = new MemoryStream())
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(wbm));
                encoder.Save(stream);
                bmImage.BeginInit();
                bmImage.CacheOption = BitmapCacheOption.OnLoad;
                bmImage.StreamSource = stream;
                bmImage.EndInit();
                bmImage.Freeze();
            }
            return bmImage;
        }
        */


    }
}
