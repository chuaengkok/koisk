﻿using Kiosk.Model;
using Kiosk.Model.Mom;
using Kiosk.Utility;
using Kiosk.Utility.API;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Kiosk.ContentPage
{
    public partial class ScanIdentityCard : UserControl
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;
        private List<Model.API.Response.Location> locationList;

        private System.Windows.Threading.DispatcherTimer identityScannerTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;

        private string identityNumber;
        private string scanningText = "Scanning..";
        private int countDownCounter;

        public ScanIdentityCard(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor, List<Model.API.Response.Location> locationList)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
            this.locationList = locationList;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblMessage.Text = navigationPage.GetMessages(0);

            identityScannerTimer = new System.Windows.Threading.DispatcherTimer();
            identityScannerTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            StartScanning();
            
            NavigationHelper.Instance.HideLoadingPanel();
        }

        private void StartScanning()
        {
            identityScannerTimer.Tick += new EventHandler(identityScannerTimer_Tick);
            closeScreenTimer.Tick += new EventHandler(closeScreenTimer_Tick);

            identityNumber = "";
            countDownCounter = ConfigurationStore.Instance.CountdownTimer;
            lblCountdown.Content = scanningText;

            string errorMessage = "";
            lblMessage2.Text = "";
            
            try
            {
                PassportScanner.Instance.StartScanning(ilog);
            }
            catch (Exception ex)
            {
                ilog.LogError("PassportScanner error", ex);
                errorMessage = ex.Message;
            }

            //Console.WriteLine($"loadSuccessful {loadSuccessful}");
            //Console.WriteLine($"errorMessage {errorMessage}");

            if (string.IsNullOrWhiteSpace(errorMessage))
                identityScannerTimer.Start();
            else
                StartCloseScreenCountdown(errorMessage);
            //IdentityNumber = await PassportScanner.Instance.StartScanningFake(); // use fake, no passport scanner now 
        }

        private void StartCloseScreenCountdown(string message)
        {
            StopIdentityScannerTimer();
            countDownCounter = ConfigurationStore.Instance.CountdownTimer;
            lblCountdown.Content = countDownCounter;

            ShowMessage(message);
            closeScreenTimer.Start();
        }

        private void identityScannerTimer_Tick(object sender, EventArgs args)
        {
            try
            {
                int secondPassed = ConfigurationStore.Instance.CountdownTimer - countDownCounter + 1;
                countDownCounter--;
                lblCountdown.Content = scanningText + new string('.', secondPassed >= 0 ? secondPassed : 0);

                if (!string.IsNullOrWhiteSpace(PassportScanner.Instance.Identity))
                {
                    StopIdentityScannerTimer();
                    identityNumber = PassportScanner.Instance.Identity;
                    string identityNumberFirstMasked = "";
                    string identityNumberLastFour = "";

                    if (identityNumber.Length > 3)
                    {
                        identityNumberFirstMasked = new string('*', identityNumber.Length - 4);
                        identityNumberLastFour = identityNumber.Substring(identityNumber.Length - 4);
                    }

                    lblImageTemplate.Visibility = Visibility.Collapsed;
                    //lblImageIdentity.Visibility = Visibility.Visible;

                    if (System.IO.File.Exists(ConfigurationStore.Instance.KioskFilePath +
                                              ConfigurationStore.Instance.TempVisitorIdentityFileName))
                    {
                        var idPhotoPath = ConfigurationStore.Instance.KioskFilePath +
                                          ConfigurationStore.Instance.TempVisitorIdentityFileName;
                        lblImageIdentity.Source = new BitmapImage(new Uri(idPhotoPath));
                        kioskVisitor.IDPhoto = File.ReadAllBytes(idPhotoPath);
                    }
                        
                    
                    ShowMessage("Identity : " + identityNumberFirstMasked + identityNumberLastFour);

                    kioskVisitor.VisitorName = PassportScanner.Instance.Name.Replace(";", "");



                    if (!string.IsNullOrWhiteSpace(kioskVisitor.VisitorName))
                        lblMessage2.Text = $"Name : {kioskVisitor.VisitorName}";

                    countDownCounter = ConfigurationStore.Instance.CountdownTimer;
                    lblCountdown.Content = countDownCounter.ToString();

                    CancelStackPanel.Visibility = Visibility.Collapsed;
                    //ManualRescanStackPanel.Visibility = Visibility.;
                    OKStackPanel.Visibility = Visibility.Visible;
                    //RescanStackPanel.Visibility = Visibility.Collapsed;

                    closeScreenTimer.Start();
                }
                else
                {
                    if (countDownCounter < 0)
                        StartCloseScreenCountdown(navigationPage.GetMessages(1));
                }
            }
            catch (Exception ex)
            {
                ilog.LogError("identityScannerTimer_Tick ", ex);
            }

            // Forcing the CommandManager to raise the RequerySuggested event
            CommandManager.InvalidateRequerySuggested();
        }
        
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();
            
            if (countDownCounter < 0)
            {
                StopCloseScreenTimer();
                if (!string.IsNullOrWhiteSpace(identityNumber))
                    NavigateToNextScreen();
                else
                    NavigationHelper.Instance.NavigateToMessageBeforeMain();
            }
        }
        
        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private async void NavigateToNextScreen()
        {
            StopIdentityScannerTimer();
            StopCloseScreenTimer();

            string identityNumberLastFour = "";
            if (identityNumber.Length > 3)
                identityNumberLastFour = identityNumber.Substring(identityNumber.Length - 4);
            
            kioskVisitor.IdentityNumber = identityNumberLastFour;
            if (kioskVisitor.VistorVisitType == KioskVisitor.VisitType.Contractor)
            {
                NavigationHelper.Instance.ShowLoadingPanel("Verifying...");

                CapitalandAPIHelper capitalandAPIHelper = new CapitalandAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                string errorMessage = await capitalandAPIHelper.CheckValidSignIn(kioskVisitor, (int)this.kioskVisitor.VistorVisitType, kioskVisitor.IdentityNumber);
                if (string.IsNullOrWhiteSpace(errorMessage))
                {
                    // only foreign contractor need to verify MOM
                    if (ConfigurationStore.Instance.CheckMOMWorkPermit && !identityNumber.ToUpper().StartsWith("T") && !identityNumber.ToUpper().StartsWith("S"))
                    {

                        MomAPIHelper momAPIHelper = new MomAPIHelper(ilog, ConfigurationStore.Instance.InternalMomAPIUrl, ConfigurationStore.Instance.APICallTimeout);
                        WorkerStatus workerStatus = await momAPIHelper.GetWorkerStatus(identityNumber);

                        NavigationHelper.Instance.HideLoadingPanel();
                        if (workerStatus != null && workerStatus.valid)
                        {
                            Model.API.Response.Location location = locationList.Find(l => l.ID == kioskVisitor.LocationID);
                            if (location != null)
                                kioskVisitor.LocationName = location.Name;

                            NavigationHelper.Instance.NavigateToNext();
                        }
                        else
                        {
                            if (workerStatus == null)
                                NavigationHelper.Instance.NavigateToError("Unable to verify worker status");
                            else
                                NavigationHelper.Instance.NavigateToError(workerStatus.message);
                        }
                    }
                    else
                        NavigationHelper.Instance.NavigateToNext();
                }
                else
                    NavigationHelper.Instance.NavigateToError(errorMessage);
            }
            else
                NavigationHelper.Instance.NavigateToNext();
        }
        
        private void ShowMessage(string message)
        {
            this.Dispatcher.Invoke(() =>
            {
                lblMessage.Text = message;

                grdCancel.Visibility = Visibility.Collapsed;
                grdRescan.Visibility = Visibility.Visible;
            });
        }

        private void StopIdentityScannerTimer()
        {
            PassportScanner.Instance.StopScanning();
            identityScannerTimer.Stop();
            identityScannerTimer.Tick -= identityScannerTimer_Tick;
        }
        private void StopCloseScreenTimer()
        {
            closeScreenTimer.Stop();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopIdentityScannerTimer();
            StopCloseScreenTimer();
        }

        #region Buttons
        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigateToMainScreen();
        }

        private void ManualEntryStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToPage(NavigationHelper.Navigate.EnterIdentityManual);
        }

        private void OKStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigateToNextScreen();
        }

        private void RescanStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.ShowLoadingPanel();

            StopCloseScreenTimer();

            grdCancel.Visibility = Visibility.Visible;
            grdRescan.Visibility = Visibility.Collapsed;

            CancelStackPanel.Visibility = Visibility.Collapsed;
            //ManualRescanStackPanel.Visibility = Visibility.Visible;
            OKStackPanel.Visibility = Visibility.Collapsed;
           // RescanStackPanel.Visibility = Visibility.Visible;

            lblMessage.Text = navigationPage.GetMessages(0);

            lblImageTemplate.Visibility = Visibility.Visible;
            lblImageIdentity.Visibility = Visibility.Collapsed;

            StartScanning();
            
            NavigationHelper.Instance.HideLoadingPanel();
        }
        #endregion
    }
}
