﻿
namespace Kiosk.Model.API.Response
{
    public class APIResult
    {
        public bool IsSuccess { get; set; }
        public string ServerError { get; set; }

        public string LocationID { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
    }
}
