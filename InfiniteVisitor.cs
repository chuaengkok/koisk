﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model.API.Response
{
    public class InfiniteVisitor
    {
        public int ID { get; set; }
        public string Contact { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Access_card { get; set; }
        public string Access_group { get; set; }
        public DateTime? Check_in { get; set; }
        public DateTime? Check_out { get; set; }
        public string Tenant { get; set; }
        public string Purpose { get; set; }
        public bool? Blacklist { get; set; }
        public DateTime? LastVisit { get; set; }
        public bool? IsPreRegister { get; set; }
        public DateTime? Registration_Date { get; set; }
    }
}
