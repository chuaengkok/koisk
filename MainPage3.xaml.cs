﻿using Kiosk.Model;
using Kiosk.Utility;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class MainPage3 : UserControl
    {
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        public MainPage3(NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle1.Content = navigationPage.GetTitle(0);
           // lblTitle2.Content = navigationPage.GetTitle(1);

            lblMessage.Text = navigationPage.GetMessages(0);

            NavigationHelper.Instance.HideLoadingPanel();
        }

        #region Buttons
        private void VisitorStackPanel_Click(object sender, RoutedEventArgs e)
        {
            kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Visitor;
            NavigationHelper.Instance.SelectedGroup = 0;
            NavigationHelper.Instance.NavigateToNext();
        }

        private void DeliveryStackPanel_Click(object sender, RoutedEventArgs e)
        {
            kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Delivery;
            NavigationHelper.Instance.SelectedGroup = 1;
            NavigationHelper.Instance.NavigateToNext();
        }
        #endregion
    }
}
