﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Model
{
    public class NavigationGroup
    {
        public List<NavigationPage> NavigationPages { get; set; }

        public NavigationPage NavigateToNext(int currentPageCounter)
        {
            if (NavigationPages.Count > currentPageCounter + 1)
            {
                currentPageCounter++;
                return NavigationPages[currentPageCounter];
            }
            return null;
        }

        public NavigationPage NavigateToBack(int currentPageCounter)
        {
            if (currentPageCounter > 0 && NavigationPages.Count > currentPageCounter)
            {
                currentPageCounter--;
                return NavigationPages[currentPageCounter];
            }
            return null;
        }

        public NavigationPage NavigateToMain()
        {
            if (NavigationPages.Count > 0)
                return NavigationPages[0];
            
            return null;
        }
    }
}
