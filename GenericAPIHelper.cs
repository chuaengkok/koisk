﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Kiosk.Utility.API
{
    public class GenericAPIHelper : APIHelper
    {
        public GenericAPIHelper(Utility.Log.Ilog ilog, string url, int apiCallTimeout) : base (ilog, url, apiCallTimeout)
        {
        }

        public async Task<List<Location>> GetLocationList(int levelID)
        {
            ilog.LogDebug($"GetLocationList=> levelID=>{levelID} ");

            List<Location> locationList = new List<Location>();
            APIResult apiResult = new APIResult();
            await Task.Run(() =>
            {
                try
                {
                    IRestResponse restResponse = GenericRestAPICall($"/api/position/{levelID}", Method.GET, null);

                    if (restResponse.IsSuccessful)
                        locationList = JsonConvert.DeserializeObject<List<Location>>(restResponse.Content);
                }
                catch (TimeoutException timeoutEx)
                {
                    apiResult.ServerError = timeoutEx.Message;
                    ilog.LogError("Timeout exception", timeoutEx);
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return locationList;
        }


        public async Task<List<Location>> GetLocationListByParentID(int parentID)
        {
            List<Location> locationList = new List<Location>();
            APIResult apiResult = new APIResult();
            await Task.Run(() =>
            {
                try
                {
                    IRestResponse restResponse = GenericRestAPICall($"/api/PositionByParentID/{parentID}", Method.GET, null);

                    if (restResponse.IsSuccessful)
                        locationList = JsonConvert.DeserializeObject<List<Location>>(restResponse.Content);
                }
                catch (TimeoutException timeoutEx)
                {
                    apiResult.ServerError = timeoutEx.Message;
                    ilog.LogError("Timeout exception", timeoutEx);
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return locationList;
        }


        public async Task<List<Location>> GetLocationListByKioskSubLocation(string kioskSubLocation)
        {
            List<Location> locationList = new List<Location>();
            APIResult apiResult = new APIResult();
            await Task.Run(() =>
            {
                try
                {
                    IRestResponse restResponse = GenericRestAPICall($"/api/PositionByParentName/{Uri.EscapeUriString(kioskSubLocation)}", Method.GET, null);

                    if (restResponse.IsSuccessful)
                        locationList = JsonConvert.DeserializeObject<List<Location>>(restResponse.Content);
                }
                catch (TimeoutException timeoutEx)
                {
                    apiResult.ServerError = timeoutEx.Message;
                    ilog.LogError("Timeout exception", timeoutEx);
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return locationList;
        }





        public async Task<string> SaveVisitInfo(int visitorTypeID, string identityNumber, string visitorName, 
                    string visitorContactNumber, string locationID, string cardNumber)
        {
            APIResult apiResult = new APIResult() ;
            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        visitorTypeID = visitorTypeID,
                        identityNumber = identityNumber,
                        visitorName = visitorName,
                        visitorContactNumber = visitorContactNumber,
                        locationID = locationID,
                        cardNumber = cardNumber
                    };

                    IRestResponse restResponse = GenericRestAPICall("/api/SaveVisitRecord", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                        apiResult = JsonConvert.DeserializeObject<APIResult>(restResponse.Content);
                }
                catch (TimeoutException timeoutEx)
                {
                    apiResult.ServerError = timeoutEx.Message;
                    ilog.LogError("Timeout exception", timeoutEx);
                }
                catch (Exception ex)
                {
                    apiResult.ServerError = ex.Message;
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult.ServerError;
        }

        public async Task<string> CheckValidSignIn(KioskVisitor kioskVisitor, int visitorTypeID, string identityNumber)
        {
            APIResult apiResult = new APIResult();
            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        visitorTypeID = visitorTypeID,
                        identityNumber = identityNumber
                    };

                    IRestResponse restResponse = GenericRestAPICall("/api/CheckValidSignIn", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                    {
                        apiResult = JsonConvert.DeserializeObject<APIResult>(restResponse.Content);
                        kioskVisitor.LocationID = apiResult.LocationID;
                        kioskVisitor.CompanyName = apiResult.Company;
                        kioskVisitor.VisitorName = apiResult.Name;
                    }
                }
                catch (Exception ex)
                {
                    apiResult.ServerError = ex.Message;
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult.ServerError;
        }
        
        public async Task<int> GetNumberofCardSignOutByCompany(string companyName)
        {
            int signOutCardCount = 0;
            await Task.Run(() =>
            {
                try
                {
                    IRestResponse restResponse = GenericRestAPICall($"api/GetNumberofCardSignedoutByCompany/{companyName}", Method.GET, null);

                    if (restResponse.IsSuccessful)
                    {
                        JObject jsonObject = JObject.Parse(restResponse.Content);
                        signOutCardCount = (int)jsonObject["Count"];
                    }
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return signOutCardCount;
        }


        

        public async Task<KioskConfig> GetKioskConfig(string kioskID)
        {
            KioskConfig kioskConfig = null;
            await Task.Run(() =>
            {
                try
                {
                    IRestResponse restResponse = GenericRestAPICall($"api/kioskconfig/{kioskID}", Method.GET, null);

                    if (restResponse.IsSuccessful)
                        kioskConfig = JsonConvert.DeserializeObject<KioskConfig>(restResponse.Content);
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return kioskConfig;
        }
    }
}
