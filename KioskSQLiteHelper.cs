﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Utility.Sqlite
{
    public class KioskSQLiteHelper
    {
        private static readonly Lazy<KioskSQLiteHelper> lazy = new Lazy<KioskSQLiteHelper>(() => new KioskSQLiteHelper());
        public static KioskSQLiteHelper Instance { get { return lazy.Value; } }
        
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly string ConnectionString = "Data Source=Kiosk.db; Version=3; New=True;";
        private static readonly string GeneralDataTableName = "GeneralData";
        private static readonly string VisitHistoryTableName = "VisitHistory";

        private KioskSQLiteHelper()
        {
            CreateTableGeneralData();
            //CreateTableVisitHistory();
        }

        private SQLiteConnection CreateConnection()
        {
            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(ConnectionString);
           // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                throw new SQLiteException("SQLite error in CreateConnection", ex);
            }
            return sqlite_conn;
        }

        private void CloseConnection(SQLiteConnection sqlite_conn)
        {
            try
            {
                sqlite_conn.Close();
                sqlite_conn.Dispose();
            }
            catch (Exception ex)
            {
                throw new SQLiteException("SQLite error in CloseConnection", ex);
            }
        }

        private void CreateTableGeneralData()
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"SELECT name FROM sqlite_master WHERE name='{GeneralDataTableName}'";
            var name = sqlite_cmd.ExecuteScalar();

            // check account table exist or not 
            if (name == null || name.ToString() != GeneralDataTableName)
            {
                string Createsql = $"CREATE TABLE {GeneralDataTableName} (EquipmentName VARCHAR(100), Data VARCHAR(100), LastModifiedDatetime DATETIME)";
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = Createsql;
                sqlite_cmd.ExecuteNonQuery();
            }

            CloseConnection(conn);
        }

        public void InsertOrUpdateRecordGeneralData(string equipmentName, string data)
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();

            sqlite_cmd.CommandText = $"UPDATE {GeneralDataTableName} SET Data=@data,LastModifiedDatetime=@datetime WHERE EquipmentName=@equipmentname; ";
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@data", data));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@equipmentname", equipmentName));
            sqlite_cmd.ExecuteNonQuery();

            sqlite_cmd.CommandText = "SELECT changes();";
            var count = sqlite_cmd.ExecuteScalar();
            int countInt = 0;
            if (count != null)
                int.TryParse(count.ToString(), out countInt);

            if (countInt == 0)
            {
                sqlite_cmd.CommandText = $"INSERT INTO {GeneralDataTableName} VALUES(@equipmentname, @data, @datetime);";
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@equipmentname", equipmentName));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@data", data));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                sqlite_cmd.ExecuteNonQuery();
            }

            CloseConnection(conn);
        }

        public string GetDataGeneralData(string equipmentName)
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"SELECT Data FROM {GeneralDataTableName} WHERE EquipmentName = @equipmentname";
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@equipmentname", equipmentName));

            var data = sqlite_cmd.ExecuteScalar();
            CloseConnection(conn);

            if (data != null)
                return data.ToString();
            else
                return "";
        }




        private void CreateTableVisitHistory()
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"SELECT name FROM sqlite_master WHERE name='{VisitHistoryTableName}'";
            var name = sqlite_cmd.ExecuteScalar();

            // check account table exist or not 
            if (name == null || name.ToString() != VisitHistoryTableName)
            {
                string Createsql = $"CREATE TABLE {VisitHistoryTableName} (ContactNumber VARCHAR(50), LastModifiedDatetime DATETIME)";
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = Createsql;
                sqlite_cmd.ExecuteNonQuery();
            }

            CloseConnection(conn);
        }

        public void InsertOrUpdateRecordVisitHistory(string contactNumber)
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();

            sqlite_cmd.CommandText = $"UPDATE {VisitHistoryTableName} SET LastModifiedDatetime=@datetime WHERE ContactNumber=@contactNumber; ";
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@contactNumber", contactNumber));
            sqlite_cmd.ExecuteNonQuery();

            sqlite_cmd.CommandText = "SELECT changes();";
            var count = sqlite_cmd.ExecuteScalar();
            int countInt = 0;
            if (count != null)
                int.TryParse(count.ToString(), out countInt);

            if (countInt == 0)
            {
                sqlite_cmd.CommandText = $"INSERT INTO {VisitHistoryTableName} VALUES(@contactNumber, @datetime);";
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@contactNumber", contactNumber));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                sqlite_cmd.ExecuteNonQuery();
            }

            CloseConnection(conn);
        }

        public string GetDatetimeVisitHistory(string contactNumber)
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"SELECT LastModifiedDatetime FROM {VisitHistoryTableName} WHERE ContactNumber=@contactNumber; ";
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@contactNumber", contactNumber));

            var data = sqlite_cmd.ExecuteScalar();
            CloseConnection(conn);

            if (data != null)
                return data.ToString();
            else
                return "";
        }


    }
}