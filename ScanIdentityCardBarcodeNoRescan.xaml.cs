﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using Kiosk.Model.Mom;
using Kiosk.Utility;
using Kiosk.Utility.API;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Kiosk.ContentPage
{
    public partial class ScanIdentityCardBarcodeNoRescan : UserControl
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        private System.Windows.Threading.DispatcherTimer identityScannerTimer;

        private string scanningText = "Scanning..";
        private int countDownCounter;

        private char[] charToTrim = { '*', '\n', '\r' };

        public ScanIdentityCardBarcodeNoRescan(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblMessage.Text = navigationPage.GetMessages(0);

            identityScannerTimer = new System.Windows.Threading.DispatcherTimer();
            identityScannerTimer.Interval = new TimeSpan(0, 0, 1);
            
            kioskVisitor.ResetStringFromInput();
            kioskVisitor.IdentityNumber = string.Empty;

            StartScanning();
            
            NavigationHelper.Instance.HideLoadingPanel();
        }

        private void StartScanning()
        {
            identityScannerTimer.Tick += new EventHandler(identityScannerTimer_Tick);

            countDownCounter = ConfigurationStore.Instance.CountdownTimer;
            lblCountdown.Content = scanningText;

            lblMessage2.Text = "";
            
            identityScannerTimer.Start();
        }
        
        private void identityScannerTimer_Tick(object sender, EventArgs args)
        {
            try
            {
                int secondPassed = ConfigurationStore.Instance.CountdownTimer - countDownCounter + 1;
                countDownCounter--;

                if (countDownCounter > 5)
                    lblCountdown.Content = scanningText + new string('.', secondPassed >= 0 ? secondPassed : 0);
                else
                {
                    lblMessage.Text = navigationPage.GetMessages(1);
                    lblCountdown.Content = countDownCounter.ToString();
                }

                if (kioskVisitor.StringFromInputEnd)
                {
                    kioskVisitor.IdentityNumber = kioskVisitor.StringFromInput.Trim(charToTrim);
                    if (!string.IsNullOrWhiteSpace(kioskVisitor.IdentityNumber))
                    {
                        //trim for extra characters at end for FIN
                        if (kioskVisitor.IdentityNumber.Length > 9)
                            kioskVisitor.IdentityNumber = kioskVisitor.IdentityNumber.Substring(0, 9);


                        StopIdentityScannerTimer();

                        ShowMessage($"ID No. : {kioskVisitor.IdentityNumberMasked}");

                        if (!string.IsNullOrWhiteSpace(kioskVisitor.VisitorName))
                            lblMessage2.Text = $"Name : {kioskVisitor.VisitorName}";

                        countDownCounter = ConfigurationStore.Instance.CountdownTimer;
                        lblCountdown.Content = countDownCounter.ToString();

                        NavigateToNextScreen();
                    }
                    else
                        NavigationHelper.Instance.NavigateToError(NavigationHelper.Instance.GetGenericMessage("EmptyBarcodeError"));
                }
                else
                {
                    if (countDownCounter <= 0)
                        NavigateToMainScreen();
                }
            }
            catch (Exception ex)
            {
                ilog.LogError("identityScannerTimer_Tick ", ex);
            }

            // Forcing the CommandManager to raise the RequerySuggested event
            CommandManager.InvalidateRequerySuggested();
        }
        
        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private async void NavigateToNextScreen()
        {
            NavigationHelper.Instance.ShowLoadingPanel("Verifying...");

            if (navigationPage.PerformVerification)
            {
                FraserAPIHelper fraserAPIHelper = new FraserAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                APIResultFraser apiResult = await fraserAPIHelper.CheckValidSignIn((int)this.kioskVisitor.VistorVisitType, kioskVisitor.IdentityNumber, kioskVisitor.IdentityNumberLast4, ConfigurationStore.Instance.LocationID);
                if (apiResult.IsSuccess)
                {
                    KioskVisitorUtility.CopyDataToKioskVisitor(kioskVisitor, apiResult.Data);
                    NavigationHelper.Instance.NavigateToNext();
                }
                else
                {
                    ErrorMessageHelper.ServerErrorCode errorCode;
                    if (Enum.TryParse<ErrorMessageHelper.ServerErrorCode>(apiResult.ServerErrorCode, out errorCode))
                    {
                        string localErrorMessage = NavigationHelper.Instance.GetGenericMessage("MOMValidationError");
                        if (!string.IsNullOrWhiteSpace(localErrorMessage))
                            NavigationHelper.Instance.NavigateToError(localErrorMessage.Replace("[[errormessage]]", apiResult.ServerErrorMessage));
                        else
                            NavigationHelper.Instance.NavigateToError(apiResult.ServerErrorMessage);
                    }
                    else
                        NavigationHelper.Instance.NavigateToError(apiResult.ServerErrorMessage);
                }
            }
            else
                NavigationHelper.Instance.NavigateToNext();
        }
        
        private void ShowMessage(string message)
        {
            this.Dispatcher.Invoke(() =>
            {
                lblMessage.Text = message;
            });
        }

        private void StopIdentityScannerTimer()
        {
            //PassportScannerTimerOld.Instance.StopTimer();
            identityScannerTimer.Stop();
            identityScannerTimer.Tick -= identityScannerTimer_Tick;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopIdentityScannerTimer();
        }

        #region Buttons
        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigateToMainScreen();
        }

        private void ManualEntryStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToPage(NavigationHelper.Navigate.EnterIdentityManual);
        }

        private void OKStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigateToNextScreen();
        }
        #endregion
    }
}
