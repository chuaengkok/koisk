﻿
namespace Kiosk.Utility
{
    public interface IDevice
    {
        void StartDevice();
        void StopDevice();

        void GetStatus();
    }
}
