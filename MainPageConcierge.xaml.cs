﻿using Kiosk.Model;
using Kiosk.Utility;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class MainPageConcierge : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        public MainPageConcierge(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle1.Text = navigationPage.GetTitle(0);
            lblTitle2.Text = navigationPage.GetTitle(1);

            lblMessage.Text = navigationPage.GetMessages(0);

            NavigationHelper.Instance.HideLoadingPanel();
        }

        #region Buttons
        private void VisitorStackPanel_Click(object sender, RoutedEventArgs e)
        {
            kioskVisitor.VistorVisitType = KioskVisitor.VisitType.Visitor;
            NavigationHelper.Instance.SelectedGroup = 0;
            NavigationHelper.Instance.NavigateToNext();
        }

        private void SignOutStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.SelectedGroup = 1;
            NavigationHelper.Instance.NavigateToNext();
        }

        private void HistoryStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.SelectedGroup = 2;
            NavigationHelper.Instance.NavigateToNext();
        }
        #endregion
    }
}
