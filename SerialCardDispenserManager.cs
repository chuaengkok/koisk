﻿using Kiosk.Model;
using Kiosk.Utility.Sqlite;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public class SerialCardDispenserManager
    {
        private static readonly Lazy<SerialCardDispenserManager> lazy = new Lazy<SerialCardDispenserManager>(() => new SerialCardDispenserManager());
        public static SerialCardDispenserManager Instance { get { return lazy.Value; } }


        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        private List<SerialCardDispenser> serialCardDispenserList = new List<SerialCardDispenser>();

        private static CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        //public void AddSerialCardDispenser(string portName, int baudRate)
        //{
        //    SerialCardDispenser serialCardDispenser = new SerialCardDispenser(portName, baudRate,);
        //    serialCardDispenserList.Add(serialCardDispenser);
        //}

        public void AddSerialCardDispenser(string jsonString)
        {

            List<CardDispenserInfo> cardDispenserInfos = JsonConvert.DeserializeObject<List<CardDispenserInfo>>(jsonString);

            foreach (CardDispenserInfo cardDispenserInfo in cardDispenserInfos)
            {
                SerialCardDispenser serialCardDispenser = new SerialCardDispenser(cardDispenserInfo.ComPortNumber,
                                    cardDispenserInfo.BaudRate, cardDispenserInfo.LocationIDMin, cardDispenserInfo.LocationIDMax);
                serialCardDispenserList.Add(serialCardDispenser);
            }
        }

        //public SerialCardDispenser GetSerialCardDispenser(int index)
        //{
        //    if (serialCardDispenserList.Count > index)
        //    {
        //        SerialCardDispenser temp = serialCardDispenserList[index];
        //        if (temp.Connect())
        //            return temp;
        //        else
        //            throw new Exception("Unable to connect to the card dispenser");

        //    }
        //    else
        //        throw new Exception($"Serial Card Dispenser not found for index - {index}");
        //}

        public SerialCardDispenser GetSerialCardDispenserByLevel(string locationID)
        {
            if (serialCardDispenserList != null)
            {
                SerialCardDispenser temp;
                if (serialCardDispenserList.Count == 1)
                    temp = serialCardDispenserList[0];
                else
                {
                    int locationIDInt = 0;
                    int.TryParse(locationID, out locationIDInt);
                    temp = serialCardDispenserList.Find(e => locationIDInt >= e.locationIDMin 
                                                                && locationIDInt <= e.locationIDMax);
                    if (temp == null)
                        temp = serialCardDispenserList[0];
                }

                if (temp != null && temp.Connect())
                    return temp;
                else
                    throw new Exception("Unable to connect to the card dispenser");
            }
            else
                throw new Exception($"Serial Card Dispenser not configured");
        }

        public void CloseAllSerialCardDispenser()
        {
            cancellationTokenSource.Cancel();

            foreach (SerialCardDispenser serialCardDispenser in serialCardDispenserList)
            {
                if (serialCardDispenser != null)
                    serialCardDispenser.Disconnect();
            }
        }

        public void StartSendStatus(int milliSecondsDelay)
        {
            foreach (SerialCardDispenser serialCardDispenser in serialCardDispenserList)
                UpdateStatusToServer(serialCardDispenser, milliSecondsDelay);
        }

        private void UpdateStatusToServer(SerialCardDispenser serialCardDispenser, int milliSecondsDelay)
        {
            Task.Run(async () =>  // <- marked async
            {
                SerialCardDispenserStatus cardDispenserStatus;
                while (!cancellationTokenSource.IsCancellationRequested)
                {
                    // do 
                    try
                    {
                        if (serialCardDispenser.Connect())
                        {
                            cardDispenserStatus = await serialCardDispenser.RequestStatusAsync();

                            System.Diagnostics.Debug.WriteLine(cardDispenserStatus);

                            KioskSQLiteHelper.Instance.InsertOrUpdateRecordGeneralData("CardDispenser", Enum.GetName(typeof(SerialCardDispenserStatus), cardDispenserStatus));
                            if (cardDispenserStatus == SerialCardDispenserStatus.CardDispenserEmpty )
                                ConfigurationStore.Instance.KioskEnabledHasCard = false;
                            else
                                ConfigurationStore.Instance.KioskEnabledHasCard = true;

                            // fixed to 2 seconds for now 
                            await Task.Delay(2000, cancellationTokenSource.Token); // <- await with cancellation
                        }
                        else
                        {
                            ConfigurationStore.Instance.KioskEnabledHasCard = false;
                            log.Debug("Card Dispenser offline");
                            System.Diagnostics.Debug.WriteLine("Card Dispenser offline");
                        }
                    }
                    catch( Exception ex)
                    {
                        log.Error("Error saving status of card dispenser", ex);
                    }
                }
            });
        }
    }
}
