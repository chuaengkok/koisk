﻿using Kiosk.Model;
using Kiosk.Utility;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kiosk.ContentPage
{
    public partial class ShowPTWInfo : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;
        private KioskVisitor kioskVisitor;

        private System.Windows.Threading.DispatcherTimer initialTimer;
        private System.Windows.Threading.DispatcherTimer closeScreenTimer;

        private int CloseScreenTimerCounter = 5;
        public ShowPTWInfo(Utility.Log.Ilog ilog, NavigationPage navigationPage, KioskVisitor kioskVisitor)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
            this.kioskVisitor = kioskVisitor;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            lblTitle2.Text = navigationPage.GetTitle(1);
            lblMessage.Text = navigationPage.GetMessages(0);

            initialTimer = new System.Windows.Threading.DispatcherTimer();
            initialTimer.Interval = new TimeSpan(0, 0, 1);

            closeScreenTimer = new System.Windows.Threading.DispatcherTimer();
            closeScreenTimer.Interval = new TimeSpan(0, 0, 1);

            CloseScreenTimerCounter = ConfigurationStore.Instance.CloseScreenTimer;

            lblCompanyNameValue.Text = kioskVisitor.CompanyName;
            lblTanentLocationValue.Text = kioskVisitor.UnitName;
            lblTenantUnitNumberValue.Text = kioskVisitor.UnitNumber;

            if (kioskVisitor.EntryStartDatetime.HasValue && kioskVisitor.EntryEndDatetime.HasValue)
            {
                lblWorkingDatesValue.Text = $"{kioskVisitor.EntryStartDatetime.Value.ToString("dd-MMM-yyyy")} to {kioskVisitor.EntryEndDatetime.Value.ToString("dd-MMM-yyyy")}";
                if (kioskVisitor.EntryStartDatetime.Value.TimeOfDay == kioskVisitor.EntryEndDatetime.Value.TimeOfDay ||
                        kioskVisitor.EntryStartDatetime.Value.TimeOfDay < kioskVisitor.EntryEndDatetime.Value.TimeOfDay)
                    lblWorkingHoursValue.Text = $"{kioskVisitor.EntryStartDatetime.Value.ToString("HH:mm")} to {kioskVisitor.EntryEndDatetime.Value.ToString("HH:mm")}";
                else
                    lblWorkingHoursValue.Text = $"{kioskVisitor.EntryStartDatetime.Value.ToString("HH:mm")} to next day {kioskVisitor.EntryEndDatetime.Value.ToString("HH:mm")}";
            }
            else if (kioskVisitor.EntryStartDatetime.HasValue)
            {
                lblWorkingDatesValue.Text = $"{kioskVisitor.EntryStartDatetime.Value.ToString("dd-MMM-yyyy")} onwards";
                lblWorkingHoursValue.Text = $"{kioskVisitor.EntryStartDatetime.Value.ToString("HH:mm")} onwards";
            }

            StartInitialTimer();

            NavigationHelper.Instance.HideLoadingPanel();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            StopInitialTimer();
            StopCloseScreenTimer();
        }

        private int initialTimerCounter = 1;
        private void initialTimer_Tick(object sender, EventArgs args)
        {
            if (initialTimerCounter > CloseScreenTimerCounter)
            {
                StopInitialTimer();
                StartCloseScreenTimer();
            }
            initialTimerCounter++;
            CommandManager.InvalidateRequerySuggested();
        }

        private int countDownCounter = 0;
        private object _lock = new object();
        private void closeScreenTimer_Tick(object sender, EventArgs args)
        {
            countDownCounter--;
            lblCountdown.Content = countDownCounter.ToString();
            CommandManager.InvalidateRequerySuggested();

            if (countDownCounter < 0)
            {
                //ilog.LogInfo("inside  closeScreenTimer_Tick  if (countDownCounter < 0)");
                StopCloseScreenTimer();
                NavigationHelper.Instance.NavigateToMessageBeforeMain();
            }
        }

        private void StartInitialTimer()
        {
            initialTimerCounter = 1;
            lblMessage.Text = navigationPage.GetMessages(0);
            lblCountdown.Content = "";
            initialTimer.Tick -= initialTimer_Tick;
            initialTimer.Tick += initialTimer_Tick;
            initialTimer.Start();
        }
        private void StartCloseScreenTimer()
        {
            countDownCounter = ConfigurationStore.Instance.CloseScreenTimer;
            lblMessage.Text = navigationPage.GetMessages(1);
            lblCountdown.Content = countDownCounter.ToString();
            closeScreenTimer.Tick -= closeScreenTimer_Tick;
            closeScreenTimer.Tick += closeScreenTimer_Tick;
            closeScreenTimer.Start();
        }
        private void StopInitialTimer()
        {
            lock (_lock)
            {
                initialTimer.Stop();
                initialTimer.Tick -= initialTimer_Tick;
            }
        }
        private void StopCloseScreenTimer()
        {
            lock (_lock)
            {
                closeScreenTimer.Stop();
                closeScreenTimer.Tick -= closeScreenTimer_Tick;
            }
        }

        private void NavigateToMainScreen()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void RefreshNoActivityTimer()
        {
            lock (_lock)
            {
                if (initialTimer.IsEnabled)
                    initialTimerCounter = 1;
                else
                {
                    if (closeScreenTimer.IsEnabled)
                    {
                        StopCloseScreenTimer();
                        if (!initialTimer.IsEnabled)
                            StartInitialTimer();
                    }
                }
            }
        }

        #region Buttons
        private void BackStackPanel_Click(object sender, RoutedEventArgs e)
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        private void NextStackPanel_Click(object sender, RoutedEventArgs e)
        {
            RefreshNoActivityTimer();

            NavigationHelper.Instance.NavigateToNext();
        }

        #endregion

    }
}
