﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kiosk.Model.API;
using Kiosk.Model.API.Response;
using Kiosk.Utility.Log;
using Newtonsoft.Json;
using RestSharp;

namespace Kiosk.Utility.API
{
    public class FRApiHelper : APIHelper
    {
        public FRApiHelper(Ilog ilog, string baseUrl, int apiCallTimeout) : base(ilog, baseUrl, apiCallTimeout)
        {
        }

        public async Task<APIFRCropResult> FaceCrop(string base64)
        {
            var apiResult = new APIFRCropResult();
            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        base64 = base64
                    };

                    IRestResponse restResponse = GenericRestAPICall("/PortalFR/CropFace", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                    {
                        apiResult = JsonConvert.DeserializeObject<APIFRCropResult>(restResponse.Content);

                        if (string.IsNullOrWhiteSpace(apiResult.base64))
                            ilog.LogDebug($"photo quality failed");
                    }
                    else
                    {
                        if (!restResponse.IsSuccessful)
                            ilog.LogDebug($"restResponse error message : {restResponse.ErrorMessage}");

                    }
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult;
        }

        public async Task<APIFRQualityResult> FaceQuality(string base64_1)
        {
            var apiResult = new APIFRQualityResult();
            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        base64=base64_1
                    };

                    IRestResponse restResponse = GenericRestAPICall("/PortalFR/CheckQuality", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                    {
                        apiResult = JsonConvert.DeserializeObject<APIFRQualityResult>(restResponse.Content);

                        if (!apiResult.IsSuccess)
                            ilog.LogDebug($"photo quality failed");
                    }
                    else
                    {
                        if (!restResponse.IsSuccessful)
                            ilog.LogDebug($"restResponse error message : {restResponse.ErrorMessage}");
                        
                    }
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult;
        }

        public async Task<APIFRResult> FaceOneToOne(string base64_1, string base64_2)
        {
            var apiResult = new APIFRResult();
            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        base64_1,
                        base64_2
                    };

                    IRestResponse restResponse = GenericRestAPICall("/PortalFR/OneToOneBase64", Method.POST, requestBody);

                    if (restResponse.IsSuccessful)
                    {
                        apiResult = JsonConvert.DeserializeObject<APIFRResult>(restResponse.Content);

                        if (apiResult.score <= 0)
                            ilog.LogDebug($"API Result score is <= 0");
                    }
                    else
                    {
                        if (!restResponse.IsSuccessful)
                            ilog.LogDebug($"restResponse error message : {restResponse.ErrorMessage}");

                    }
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return apiResult;
        }
    }
}
