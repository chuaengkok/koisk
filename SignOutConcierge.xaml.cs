﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using Kiosk.Utility;
using Kiosk.Utility.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Kiosk.ContentPage
{
    public partial class SignOutConcierge : UserControl
    {
        private Utility.Log.Ilog ilog;
        private NavigationPage navigationPage;

        public SignOutConcierge(Utility.Log.Ilog ilog, NavigationPage navigationPage)
        {
            InitializeComponent();

            this.ilog = ilog;
            this.navigationPage = navigationPage;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Text = navigationPage.GetTitle(0);
            ResetPage();

            NavigationHelper.Instance.HideLoadingPanel();
        }
        private async void txtCardNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                await SignOut();
            }
        }

        private void ResetPage()
        {
            lblMessage.Foreground = System.Windows.Media.Brushes.Red;
            lblMessage.Text = navigationPage.GetMessages(0);

            txtCardNumber.Text = "";

            txtCardNumber.Focus();
        }

        private async System.Threading.Tasks.Task<bool> SignOut()
        {
            NavigationHelper.Instance.ShowLoadingPanel();

            lblMessage.Foreground = System.Windows.Media.Brushes.Red;

            bool success = false;

            // do sign out;
            if (!string.IsNullOrWhiteSpace(txtCardNumber.Text))
            {
                string cardNumber = txtCardNumber.Text.Trim();
                JTCAPIHelper jtcAPIHelper = new JTCAPIHelper(ilog, ConfigurationStore.Instance.KioskApiURL, ConfigurationStore.Instance.APICallTimeout);
                success = await jtcAPIHelper.SignOut(ConfigurationStore.Instance.KioskID, cardNumber);

                if (success)
                {
                    //clear all data;
                    ResetPage();
                    lblMessage.Text = navigationPage.GetMessages(2).Replace("[[cardnumber]]", cardNumber);
                    lblMessage.Foreground = System.Windows.Media.Brushes.DarkBlue;
                    success = true;
                }
                else
                {
                    lblMessage.Text = navigationPage.GetMessages(1).Replace("[[cardnumber]]", txtCardNumber.Text.Trim());
                    txtCardNumber.SelectAll();
                    txtCardNumber.Focus();
                }
            }
            else
            {
                lblMessage.Text = navigationPage.GetMessages(0);
                txtCardNumber.Focus();
            }

            NavigationHelper.Instance.HideLoadingPanel();
            return success;
        }

        private void BackToMain()
        {
            NavigationHelper.Instance.NavigateToMain();
        }

        #region Buttons
        private void CancelStackPanel_Click(object sender, RoutedEventArgs e)
        {
            BackToMain();
        }
        private async void SignOutStackPanel_Click(object sender, RoutedEventArgs e)
        {
            await SignOut();
        }
        
        private void ResetStackPanel_Click(object sender, RoutedEventArgs e)
        {
            ResetPage();
        }


        #endregion

    }
}
