﻿using Kiosk.Model;
using Kiosk.Model.API.Response;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Kiosk.Utility.API
{
    public class InfiniteAPIHelper : APIHelper
    {
        public InfiniteAPIHelper(Utility.Log.Ilog ilog, string url, int apiCallTimeout) : base(ilog, url, apiCallTimeout)
        {
        }

        public async Task<InfiniteVisitor> GetInfiniteVisitor(string contactNumber)
        {
            InfiniteVisitor infiniteVisitor = null;
            await Task.Run(() =>
            {
                try
                {
                    IRestResponse restResponse = GenericRestAPICall($"api/Infinite/GetVisitor/{contactNumber}", Method.GET, null);

                    if (restResponse.IsSuccessful)
                        infiniteVisitor = JsonConvert.DeserializeObject<InfiniteVisitor>(restResponse.Content);
                }
                catch (TimeoutException timeoutEx)
                {
                    ilog.LogError("Timeout exception", timeoutEx);
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return infiniteVisitor;
        }


        public async Task<bool> SaveInfiniteVisitorLog(string contactNumber, string visitorName, string accessGroupName, string cardNumber)
        {
            bool success = false;
            await Task.Run(() =>
            {
                try
                {
                    object requestBody = new
                    {
                        contact = contactNumber,
                        visitorName = visitorName,
                        accessGroup = accessGroupName,
                        cardNumber = cardNumber,
                        issueTime = DateTime.Now
                    };

                    IRestResponse restResponse = GenericRestAPICall("api/Infinite/SaveVisitorLog", Method.POST, requestBody);
                    success = restResponse.IsSuccessful;

                }
                catch (TimeoutException timeoutEx)
                {
                    ilog.LogError("Timeout exception", timeoutEx);
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }
            });
            return success;
        }
    }
}
