﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.Utility
{
    public enum SerialCardDispenserStatus
    {
        DipensingCard = 383030,
        RetractingCard = 343030,
        ErrorOccured = 323030,
        RetractedCardHolderFull = 313030,
        Unknown1 = 303830,
        Unknown2 = 303430,
        CardDispenserJammed = 303230,
        LowOnCard = 303130,
        CardDispenserEmpty = 303038,
        CardReadyForRetrieval = 303034,
        CardAtReaderLocation = 303032,
        CardInProcessed = 303031,
        CardDispenserOnStandby = 303030,
        NoStatus = 000000,
        Offline = 999999
    }

    public static class ErrorLevelExtensions
    {
        public static string ToString(this SerialCardDispenserStatus me)
        {
            switch (me)
            {
                case SerialCardDispenserStatus.DipensingCard: return "Currently dispensing card";
                case SerialCardDispenserStatus.RetractingCard: return "Currently retracting card";
                case SerialCardDispenserStatus.ErrorOccured: return "An error has occured";
                case SerialCardDispenserStatus.RetractedCardHolderFull: return "Retracted card holder is full";
                case SerialCardDispenserStatus.Unknown1: return "Unknown";
                case SerialCardDispenserStatus.Unknown2: return "Unknown";
                case SerialCardDispenserStatus.CardDispenserJammed: return "Card Dispenser is jammed";
                case SerialCardDispenserStatus.LowOnCard: return "Low on cards";
                case SerialCardDispenserStatus.CardDispenserEmpty: return "No more cards";
                case SerialCardDispenserStatus.CardReadyForRetrieval: return "Card is ready for retrieval";
                case SerialCardDispenserStatus.CardAtReaderLocation: return "Card is currently at the reader";
                case SerialCardDispenserStatus.CardInProcessed: return "Card is still being processed";
                case SerialCardDispenserStatus.CardDispenserOnStandby: return "Card Dispenser is ready on standby for instructions";
                case SerialCardDispenserStatus.NoStatus: return "No Status";
                case SerialCardDispenserStatus.Offline: return "Offline";
                default: return "Unknown Status";
            }
        }
    }
}
