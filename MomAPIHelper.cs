﻿using Kiosk.Model.Mom;
using Kiosk.Model.Siemens;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kiosk.Utility.API
{
    public class MomAPIHelper : APIHelper
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        

        public MomAPIHelper(Utility.Log.Ilog ilog, string url, int apiCallTimeout) : base(ilog, url, apiCallTimeout)
        {
        }

        public async Task<WorkerStatus> GetWorkerStatus(string FIN)
        {
            WorkerStatus workerStatus = null;
            await Task.Run(() =>
            {
                try
                {
                    IRestResponse restResponse = GenericRestAPICall($"/api/mom/workerstatus/{FIN}", Method.GET);
                    workerStatus = JsonConvert.DeserializeObject<WorkerStatus>(restResponse.Content);
                }
                catch (Exception ex)
                {
                    ilog.LogError("Exception", ex);
                }

            });

            return workerStatus;
        }
    }
}
